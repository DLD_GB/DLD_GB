﻿using Fr.EQL.AI110.DLD_GB.Entities;

namespace Fr.EQL.AI110.DLD_GB.Web.Models
{
    public class StorageViewModel
    {
        #region Critères
        public StorageArea StorageArea { get; set; }
        public StorageAreaDetails StorageAreaDetails { get; set; }
        //public PlageHoraire PlageHoraireLundi { get; set; }
        //public PlageHoraire PlageHoraireMardi { get; set; }
        //public PlageHoraire PlageHoraireMercredi { get; set; }
        //public PlageHoraire PlageHoraireJeudi { get; set; }
        //public PlageHoraire PlageHoraireVendredi { get; set; }
        //public PlageHoraire PlageHoraireSamedi { get; set; }
        //public PlageHoraire PlageHoraireDimanche { get; set; }
        public int IdPartner { get; set; }
        #endregion

        #region Listes de données
        public List<Taille> Tailles { get; set; }
        public List<TemperatureStockage> Temperatures { get; set; }
        public List<VilleCP> Villes { get; set; }
        public List<Partner> Partenaires { get; set; }
        public List<StorageAreaDetails> StorageAreasDetails { get; set; }
        //public List<PlageHoraire> PlagesHoraires { get; set; }
        #endregion
    }
}
