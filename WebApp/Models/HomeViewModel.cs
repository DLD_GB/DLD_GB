﻿using Fr.EQL.AI110.DLD_GB.Entities;

namespace Fr.EQL.AI110.DLD_GB.Web.Models;

public class HomeViewModel
{


    public List<Partner> Partners { get; set; }
    public List<Adherent> Adherents { get; set; }

    public int IdAdherent { get; set; }
    public int IdPartner { get; set; }

    public HomeViewModel()
    {
    }

}
