﻿using Fr.EQL.AI110.DLD_GB.Entities;
using Fr.EQL.AI110.DLD_GB.Business;
namespace Fr.EQL.AI110.DLD_GB.Web.Models;

public class DonationSearchViewModel
{
    #region critères de recherche
    public string NomRayon { get; set; }
    public string NomCategorie { get; set; }
    public string NomProduit { get; set; }
    public string UniteMesure { get; set; }
    public int QuantiteProduit { get; set; }
    public string TemperatureStockage { get; set; }
    public string Ville { get; set; }
    public string TypeStockage { get; set; }
    public bool Box { get; set; }
    public int Distance { get; set; }

    public List<Rayon> Rayons { get; set; }
    public List<Taille> Tailles { get; set; }
    public List<Categorie> Categories { get; set; }
    public List<UniteMesure> UnitesMesure { get; set; }
    public List<TemperatureStockage> TemperaturesStockage { get; set; }
    public List<VilleCP> Villes { get; set; }
    public List<Produit> Produits { get; set; }

    public List<PlageHoraire> Plages { get; set; }

    public string ListeVillesProches { get; set; }
    public string ListeCategories { get; set; }
    public string ListeRayons { get; set; }
    public string ListeProduits { get; set; }

    public int RayonId { get; set; }
    public int CategorieId { get; set; }
    public int ProduitId { get; set; }

    public Produit ProduitActuel { get; set; }
    public Categorie CategorieActuelle { get; set; }
    public Rayon RayonActuel { get; set; }

    public Categorie CategorieParent { get; set; }
    public Rayon RayonParent { get; set; }

    public bool reset { get; set; }
    public int IdToReserve { get; set; }
    public int IdToCancelReservation { get; set; }
    public int IdToCancel { get; set; }
    public int IdPartner { get; set; }

    #endregion

    public List<DonationDetails> DonationsDetails { get; set; }
    public List<DonationDetails> Donationreserved { get; set; }
    public Adherent Adherent { get; set; }

    public DonationBusiness donationBusiness { get; set; }

    public DonationSearchViewModel(string nomRayon, string nomCategorie, string nomProduit, string uniteMesure, int quantiteProduit, 
        string temperatureStockage, string ville, string typeStockage, bool box, int distance, List<Rayon> rayons, List<Taille> tailles, 
        List<Categorie> categories, List<UniteMesure> unitesMesure, List<TemperatureStockage> temperaturesStockage, List<VilleCP> villes, 
        List<Produit> produits, string listeVillesProches, string listeCategories, string listeRayons, string listeProduits, List<DonationDetails> donationsDetails)
    {
        NomRayon = nomRayon;
        NomCategorie = nomCategorie;
        NomProduit = nomProduit;
        UniteMesure = uniteMesure;
        QuantiteProduit = quantiteProduit;
        TemperatureStockage = temperatureStockage;
        Ville = ville;
        TypeStockage = typeStockage;
        Box = box;
        Distance = distance;
        Rayons = rayons;
        Tailles = tailles;
        Categories = categories;
        UnitesMesure = unitesMesure;
        TemperaturesStockage = temperaturesStockage;
        Villes = villes;
        Produits = produits;
        ListeVillesProches = listeVillesProches;
        ListeCategories = listeCategories;
        ListeRayons = listeRayons;
        ListeProduits = listeProduits;
        DonationsDetails = donationsDetails;
    }

    public DonationSearchViewModel()
    {
    }
}

