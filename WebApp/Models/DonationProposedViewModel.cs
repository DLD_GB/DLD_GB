﻿
using Fr.EQL.AI110.DLD_GB.Entities;


namespace Fr.EQL.AI110.DLD_GB.Web.Models
{
    public class DonationProposedViewModel
    {
        #region Listes des données pour affichage
        public List<DonationDetails>? Donations { get; set; }
        #endregion
    }
}