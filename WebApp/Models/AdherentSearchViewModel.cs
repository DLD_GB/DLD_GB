﻿using Fr.EQL.AI110.DLD_GB.Entities;

namespace Fr.EQL.AI110.DLD_GB.Web.Models
{
    public class AdherentSearchViewModel
    {

            #region Critères de recherche
            public string Nom { get; set; }
            public string Prenom { get; set; }
            #endregion

            #region Listes des données pour affichage
            public List<Adherent> Adherents { get; set; }
            #endregion
    }
}
