﻿using Fr.EQL.AI110.DLD_GB.Entities;

namespace Fr.EQL.AI110.DLD_GB.Web.Models
{
    public class AddCartViewModel
    {
        public List<CartDetails> CartsDetails { get; set; }

        public int TotalArticleAuPanier { get; set; }

        public int IdAdherent { get; set; }
    }
}
