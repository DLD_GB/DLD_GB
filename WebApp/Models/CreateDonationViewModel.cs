using Fr.EQL.AI110.DLD_GB.Entities;

namespace Fr.EQL.AI110.DLD_GB.Web.Models;

public class CreateDonationViewModel
{
    #region donn�es r�cup�r�es pour cr�ation du don 
    public int AdherentId { get; set; }
    public float QuantiteProduit { get; set; }
    public int UniteDeMesureId { get; set; }
    public int EmballageId { get; set; }
    public DateTime DatePeremption { get; set; }
    public int TemperatureStockageId { get; set; }
    public int ProduitId { get; set; }
    public int VilleId { get; set; }
    public string NomEtNDeVoie { get; set; }
    public int PartnerId { get; set; }

    #endregion

    #region donn�es pour l'affichage
    public List<Rayon> Rayons { get; set; }
    public List<Categorie> Categories { get; set; }
    public List<Produit> Produits { get; set; }
    public List<Adherent> Adherents { get; set; }
    public List<UniteMesure> UnitesMesure { get; set; }
    public List<TemperatureStockage> TemperaturesStockage { get; set; }
    public List<VilleCP> Villes { get; set; }
    public List<Emballage> Emballages { get; set; }
    public List<Partner> Partners { get; set; }
    public List<PartnerDetails> PartnerDetails { get; set; }
    public List<StorageArea> StorageArea { get; set; }
    public List<VilleCP> VillesProche { get; set; }
    public VilleCP Ville { get; set; }

    public string AdherentNom { get; set; }
    public string ProduitNom { get; set; }
    public string RayonNom { get; set; }
    public string CategorieNom { get; set; }
    public string VilleNom { get; set; }
    public string Adherent { get; set; }
    public string UniteDeMesure { get; set; }
    public string Emballage { get; set; }
    public int RayonId { get; set; }
    public int CategorieId { get; set; }

    #endregion

    
    public DonationDetails DonationDetails { get; set; }
    public List<DonationDetails> DonationsDetails { get; set; }


 
}