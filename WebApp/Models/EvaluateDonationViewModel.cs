﻿using Fr.EQL.AI110.DLD_GB.Entities;

namespace Fr.EQL.AI110.DLD_GB.Web.Models;

public class EvaluateDonationViewModel
{
    public int QualiteEchange { get; set; }
    public int QualiteEchangeId { get; set; }
    public int QualiteProduit { get; set; }
    public int QualiteProduitId { get; set; }
    public int Echange { get; set; }
    public int EchangeId { get; set; }
    public int NoteEvaluation { get; set; }
    public string? CommentaireEvaluation { get; set; }
    public DateTime DateEvaluation { get; set; }
    public List<QualiteEchange>? QualitesEchange { get; set; }
    public List<QualiteProduit>? QualitesProduit { get; set; }
    public List<Echange>? Echanges { get; set; }

    public EvaluateDonationViewModel(int qualiteEchange, int qualiteEchangeId, int qualiteProduit, int qualiteProduitId, int echange, int echangeId, int noteEvaluation, string commentaireEvaluation,
        DateTime dateEvaluation, List<QualiteEchange> qualitesEchange, List<QualiteProduit> qualitesProduit, List<Echange> echanges)

    {
        QualiteEchange = qualiteEchange;
        QualiteEchangeId = qualiteEchangeId;
        QualiteProduit = qualiteProduit;
        QualiteProduitId = qualiteProduitId;
        Echange = echange;
        EchangeId = echangeId;
        NoteEvaluation = noteEvaluation;
        CommentaireEvaluation = commentaireEvaluation;
        DateEvaluation = dateEvaluation;
    }

    public EvaluateDonationViewModel()
    {
    }
}

