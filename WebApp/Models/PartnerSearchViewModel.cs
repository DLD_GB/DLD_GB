﻿using Fr.EQL.AI110.DLD_GB.Entities;

namespace Fr.EQL.AI110.DLD_GB.Web.Models
{
	public class PartnerSearchViewModel
	{

        #region Critères de recherche
        public string Name { get; set; }
        public double Siret { get; set; }
        public int VilleId { get; set; }
		#endregion

		#region Listes des données pour affichage
		public List<Partner> Partners { get; set; }

		public List<VilleCP> VilleCPs { get; set;}

		#endregion
	}
}
