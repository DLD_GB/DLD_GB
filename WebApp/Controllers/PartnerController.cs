﻿using Microsoft.AspNetCore.Mvc;
using Fr.EQL.AI110.DLD_GB.Business;
using Fr.EQL.AI110.DLD_GB.Entities;
using Fr.EQL.AI110.DLD_GB.Web.Models;

namespace Fr.EQL.AI110.DLD_GB.Web.Controllers
{
    public class PartnerController : Controller
    {

        [HttpGet]
        public IActionResult Index()
        {   
            PartnerSearchViewModel model = new PartnerSearchViewModel();

            PartnerBusiness PartnerBusiness = new PartnerBusiness();
            model.Partners = PartnerBusiness.GetAllPartner();

            VilleCPBusiness villeCPBusiness = new VilleCPBusiness();
            model.VilleCPs = villeCPBusiness.GetAllVilleCP(); 

            return View(model);
        }

        [HttpPost]
        public IActionResult Index(PartnerSearchViewModel model)
		{
            PartnerBusiness partnerBusiness = new PartnerBusiness();
            model.Partners = partnerBusiness.SearchPartner(model.Name, model.Siret, model.VilleId);

            VilleCPBusiness villeCPBusiness = new VilleCPBusiness();
            model.VilleCPs = villeCPBusiness.GetAllVilleCP();
            return View(model);
		}

        [HttpGet]
        public IActionResult DonationToStock(int IdPartner)
        {
            PartnerBusiness DonationBusiness = new PartnerBusiness();
            DonationBusiness donationBusiness = new DonationBusiness();

            DonationSearchViewModel model = new DonationSearchViewModel();

            model.DonationsDetails = donationBusiness.GetDonationByIdPartner(IdPartner);
            model.IdPartner = IdPartner;
            ViewBag.IdPartner = IdPartner;

            return View(model);

        }

    }
}
