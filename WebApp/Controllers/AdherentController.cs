﻿using Fr.EQL.AI110.DLD_GB.Business;
using Fr.EQL.AI110.DLD_GB.Entities;
using Fr.EQL.AI110.DLD_GB.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fr.EQL.AI110.DLD_GB.Web.Controllers
{
    public class AdherentController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            AdherentSearchViewModel model = new AdherentSearchViewModel();

            AdherentBusiness adherentBu = new AdherentBusiness();
            model.Adherents = adherentBu.GetAllAdherents();
            
            return View(model);
        }

        [HttpPost]
        public IActionResult Index(AdherentSearchViewModel model)
        {
            AdherentBusiness adherentBu = new AdherentBusiness();
            model.Adherents = adherentBu.SearchAdherent(model.Nom, model.Prenom);

            return View(model);
        }
    }
}
