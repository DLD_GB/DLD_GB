﻿using Microsoft.AspNetCore.Mvc;
using Fr.EQL.AI110.DLD_GB.Business;
using Fr.EQL.AI110.DLD_GB.Entities;
using Fr.EQL.AI110.DLD_GB.Web.Models;

namespace Fr.EQL.AI110.ChristmasFactory.Web.Controllers
{
    public class PanierViewComponent : ViewComponent
    {

        public IViewComponentResult Invoke(int IdAdherent)
        {
           CartBusiness cartBusiness = new CartBusiness();
           
            AddCartViewModel cartViewModel = new AddCartViewModel();
            cartViewModel.CartsDetails = cartBusiness.GetAllCartDetailsByIdAdherent(IdAdherent);
            cartViewModel.TotalArticleAuPanier = cartBusiness.TotalPanierByIdAdherent(IdAdherent);
            cartViewModel.IdAdherent = IdAdherent;
            return View(cartViewModel);
        }
    }
}
