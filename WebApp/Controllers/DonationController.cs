﻿using Fr.EQL.AI110.DLD_GB.Business;
using Fr.EQL.AI110.DLD_GB.Entities;
using Fr.EQL.AI110.DLD_GB.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;





namespace Fr.EQL.AI110.DLD_GB.Web.Controllers
{


    public class DonationController : Controller
    {
        public static int RayonIdStatic = 0;
        public static int CategorieIdStatic = 0;
        public static int ProduitIdStatic = 0;
        public static int IdAdherentStatic = 0;


        [HttpGet]
        public IActionResult Index(int IdAdherent)
        {
            
            AdherentBusiness adherentBusiness = new AdherentBusiness();
            DonationBusiness DonationBusiness = new DonationBusiness();
            CategorieBusiness CategorieBusiness = new CategorieBusiness();
            TailleBusiness tailleBusiness = new TailleBusiness();
            RayonBusiness rayonBusiness = new RayonBusiness();
            TemperatureStockageBusiness temperatureStockageBusiness = new TemperatureStockageBusiness();
            UniteMesureBusiness uniteMesureBusiness = new UniteMesureBusiness();
            VilleCPBusiness villeCPBusiness = new VilleCPBusiness();
            ProduitBusiness produitBusiness = new ProduitBusiness();
            ViewBag.IdAdherent = IdAdherent;
            IdAdherentStatic = IdAdherent;

            DonationSearchViewModel model = new DonationSearchViewModel();

            model.Produits = produitBusiness.GetAllProduit();
            model.DonationsDetails = DonationBusiness.GetDonationByMultiCriteria(0, 0, 0, "", "", "", "", 0, "", "", "", null, true, null, null, null, null);
            model.Categories = CategorieBusiness.GetAllCategorie();
            model.Tailles = tailleBusiness.GetAllTaille();
            model.Rayons = rayonBusiness.GetAllRayon();
            model.TemperaturesStockage = temperatureStockageBusiness.GetAllTemperatureStockage();
            model.UnitesMesure = uniteMesureBusiness.GetAllUniteMesure();
            model.Villes = villeCPBusiness.GetAllVilleCP();
            model.Adherent = adherentBusiness.GetAdherentById(IdAdherent);

            return View(model);
        }

        [HttpPost]
        public IActionResult Index(DonationSearchViewModel model)
        {
            bool? box = null;

            if (model.IdToReserve > 0) 

            if (model.RayonId != 0)
            {
                RayonIdStatic = model.RayonId;
                CategorieIdStatic = 0;
                ProduitIdStatic = 0;
            }
            else if (RayonIdStatic != 0 && model.CategorieId == 0 && model.RayonId == 0 && model.ProduitId == 0 && CategorieIdStatic == 0) model.RayonId = RayonIdStatic;

            if (model.CategorieId != 0)
            {
                CategorieIdStatic = model.CategorieId;
                ProduitIdStatic = 0;
            }
            else if (CategorieIdStatic != 0 && model.CategorieId == 0 && model.RayonId == 0 && model.ProduitId == 0 && ProduitIdStatic == 0) model.CategorieId = CategorieIdStatic;

            if (model.ProduitId != 0) ProduitIdStatic = model.ProduitId;
            else if (ProduitIdStatic != 0 && model.CategorieId == 0 && model.RayonId == 0 && model.ProduitId == 0) model.ProduitId = ProduitIdStatic;


            if (model.reset == true)
            {
                model.CategorieId = 0;
                model.RayonId = 0;
                model.ProduitId = 0;
                RayonIdStatic = 0;
                CategorieIdStatic = 0;
                ProduitIdStatic = 0;
    }

            AdherentBusiness adherentBusiness = new AdherentBusiness();
            DonationBusiness DonationBusiness = new DonationBusiness();
            VilleCPBusiness VilleCPBusiness = new VilleCPBusiness();
            CategorieBusiness CategorieBusiness = new CategorieBusiness();
            TailleBusiness tailleBusiness = new TailleBusiness();
            RayonBusiness rayonBusiness = new RayonBusiness();
            TemperatureStockageBusiness temperatureStockageBusiness = new TemperatureStockageBusiness();
            UniteMesureBusiness uniteMesureBusiness = new UniteMesureBusiness();
            VilleCPBusiness villeCPBusiness = new VilleCPBusiness();
            ProduitBusiness produitBusiness = new ProduitBusiness();



                if (!string.IsNullOrEmpty(model.Ville)) model.ListeVillesProches = VilleCPBusiness.listeVillesProches(VilleCPBusiness.GetVilleCPByNomDeVille(model.Ville), model.Distance);
            if (model.Box == true) box = true;

            model.DonationsDetails = DonationBusiness.GetDonationByMultiCriteria(model.RayonId, model.CategorieId, model.ProduitId, model.NomRayon, model.NomCategorie, model.NomProduit,
                                     model.UniteMesure, model.QuantiteProduit, model.TemperatureStockage, model.Ville, model.TypeStockage,
                                     box, true, model.ListeVillesProches, model.ListeCategories, model.ListeRayons, model.ListeProduits);


            if (model.RayonId == 0 && model.CategorieId == 0 && model.ProduitId == 0)
            {
                model.Categories = CategorieBusiness.GetAllCategorie();
                model.Produits = produitBusiness.GetAllProduit();
            }

            else if (model.RayonId != 0 && model.CategorieId == 0 && model.ProduitId == 0)
            {
                model.Categories = CategorieBusiness.GetCategorieByRayonId(model.RayonId);
                model.RayonActuel = rayonBusiness.GetRayonById(model.RayonId);
            }

            else if (model.RayonId == 0 && model.CategorieId != 0 && model.ProduitId == 0)
            {
                model.Produits = produitBusiness.GetProduitByIdCategorieId(model.CategorieId);
                model.RayonActuel = CategorieBusiness.GetRayonParentByIdCategorie(model.CategorieId);
                model.CategorieActuelle = CategorieBusiness.GetCategorieById(model.CategorieId);
            }

            else if (model.RayonId == 0 && model.CategorieId == 0 && model.ProduitId != 0)
            {
                model.RayonActuel = CategorieBusiness.GetRayonParentByIdCategorie(produitBusiness.GetCategorieParentByIdProduit(model.ProduitId).Id);
                model.CategorieActuelle = produitBusiness.GetCategorieParentByIdProduit(model.ProduitId);
                model.ProduitActuel = produitBusiness.GetProduitById(model.ProduitId);
                model.NomProduit = produitBusiness.GetProduitById(model.ProduitId).Nom;
            }

            model.Tailles = tailleBusiness.GetAllTaille();
            model.Rayons = rayonBusiness.GetAllRayon();
            model.TemperaturesStockage = temperatureStockageBusiness.GetAllTemperatureStockage();
            model.UnitesMesure = uniteMesureBusiness.GetAllUniteMesure();
            model.Villes = villeCPBusiness.GetAllVilleCP();
            model.Adherent = adherentBusiness.GetAdherentById(IdAdherentStatic);

            return View(model);
        }

        public IActionResult ReserverUnDon(int IdToReserve, int idAdherent)
        {
            DonationBusiness donationBusiness = new DonationBusiness();
            donationBusiness.ReserveDonation(IdToReserve, idAdherent);
            return RedirectToAction("Index", new {idAdherent });
        }

        public IActionResult ConfirmerRecuperationDeDon(int IdToConfirm, int idAdherent)
        {
            DonationBusiness donationBusiness = new DonationBusiness();
            donationBusiness.ConfirmerReceptionDeDon(IdToConfirm);

            return RedirectToAction("Proposed", new {idAdherent});
        }

        public IActionResult ConfirmerDepot(int IdDon, int idAdherent)
        {
            DonationBusiness donationBusiness = new DonationBusiness();
            donationBusiness.ConfirmerDepotDon(IdDon);
            return RedirectToAction("Proposed", new { idAdherent = idAdherent });
        }

        public IActionResult CancelReservation(int IdDon, int idAdherent)
        {
            DonationBusiness donationBusiness = new DonationBusiness();
            donationBusiness.CancelReservation(IdDon);
            return RedirectToAction("Proposed", new { idAdherent = idAdherent });
        }

        private IActionResult EditerDon(string rubrique, Donation donation)
        {
            ViewBag.Rubrique = rubrique;
            return View("Create", donation);
        }


        [HttpGet]
        public IActionResult Create1(int IdAdherent)
        {
            AdherentBusiness adherentBu = new AdherentBusiness();
            ViewBag.IdAdherent = IdAdherent;
            RayonBusiness rayonBu = new RayonBusiness();
            CreateDonationViewModel model = new CreateDonationViewModel();
            model.AdherentId = IdAdherent;
            model.AdherentNom = adherentBu.GetAdherentByAdherentId(IdAdherent).Nom;
            model.Rayons = rayonBu.GetAllRayon();

            return View(model);
        }

        [HttpPost]
        public IActionResult Create2(CreateDonationViewModel model)
        {
            AdherentBusiness adherentBu = new AdherentBusiness();
            RayonBusiness rayonBu = new RayonBusiness();
            CategorieBusiness categorieBu = new CategorieBusiness();

            model.AdherentNom = adherentBu.GetAdherentByAdherentId(model.AdherentId).Nom;
            model.Categories = categorieBu.GetAllCategorieByIdRayon(model.RayonId);
            model.RayonNom = rayonBu.GetRayonByIdRayon(model.RayonId).Nom;
            ViewBag.IdAdherent = model.Adherent;
            return View(model);
        }

        [HttpPost]
        public IActionResult Create3(CreateDonationViewModel model)
        {
            AdherentBusiness adherentBu = new AdherentBusiness();
            RayonBusiness rayonBu = new RayonBusiness();
            CategorieBusiness categorieBu = new CategorieBusiness();
            ProduitBusiness produitBu = new ProduitBusiness();

            model.AdherentNom = adherentBu.GetAdherentByAdherentId(model.AdherentId).Nom;
            model.RayonNom = rayonBu.GetRayonByIdRayon(model.RayonId).Nom;
            model.CategorieNom = categorieBu.GetCategorieByIdCategorie(model.CategorieId).Nom;
            model.Produits = produitBu.GetAllProduitsByIdCategorie(model.CategorieId);
            ViewBag.IdAdherent = model.Adherent;

            return View(model);
        }

        [HttpPost]
        public IActionResult Create4(CreateDonationViewModel model)
        {
            UniteMesureBusiness uniteMesureBusiness = new UniteMesureBusiness();
            EmballageBusiness emballageBu = new EmballageBusiness();
            TemperatureStockageBusiness tempBu = new TemperatureStockageBusiness();

            model.UnitesMesure = uniteMesureBusiness.GetAllUniteMesure();
            model.Emballages = emballageBu.GetAllEmballage();
            model.TemperaturesStockage = tempBu.GetAllTemperatureStockage();
            ViewBag.IdAdherent = model.Adherent;

            return View(model);
        }

        [HttpPost]
        public IActionResult Create5(CreateDonationViewModel model)
        {
            VilleCPBusiness villeCpBu = new VilleCPBusiness();
            PartnerBusiness partnerBu = new PartnerBusiness();
            AdherentBusiness adherentBu = new AdherentBusiness();
            ViewBag.IdAdherent = model.Adherent;

            model.Villes = villeCpBu.GetAllVilleCP();

            // recherche des partenaires en fonction de la ville de l'adhérent, de la température de stockage du produit,
            // et de la disponibilité des emplacements de stockage
            int adherentVilleId = adherentBu.GetAdherentByAdherentId(model.AdherentId).VilleCpId;
            model.Ville = villeCpBu.GetVilleCPByIdVille(adherentVilleId);
            model.VillesProche = villeCpBu.listeVillesProximite(model.Ville, 30);
            List<Partner> result = new List<Partner>();
            foreach (VilleCP ville in model.VillesProche)
            {
                List<Partner>Partner = partnerBu.SearchPartnerDispo(ville.Id, model.TemperatureStockageId);
                result.AddRange(Partner);
            }
            model.Partners = result;

            return View(model);
        }

        [HttpPost]
        public IActionResult Create6(CreateDonationViewModel model)
        {
            DonationDetails donationDetails = new DonationDetails();
            donationDetails.AdherentId = model.AdherentId;
            donationDetails.ProduitId = model.ProduitId;
            donationDetails.UniteDeMesureId = model.UniteDeMesureId;
            donationDetails.QuantiteProduit = model.QuantiteProduit;
            donationDetails.DatePeremption = model.DatePeremption;
            donationDetails.EmballageId = model.EmballageId;
            ViewBag.IdAdherent = model.Adherent;

            if (model.NomEtNDeVoie is not null)
            {
                donationDetails.NomEtNDeVoie = model.NomEtNDeVoie;
            }
            
            if (model.VilleId != 0)
            {
                donationDetails.VilleCpId = model.VilleId;

            }

            if (model.PartnerId != 0)
            {   //Récupération d'un emplacment de stockage en fonction du partenaire choisi, et du type de stockage
                donationDetails.PartnerId = model.PartnerId;
                StorageAreaBusiness storageAreaBu = new StorageAreaBusiness();
                List<StorageArea> storageAreaDispo = storageAreaBu.GetStorageAreaDispoByMulticritera(model.PartnerId, model.TemperatureStockageId);
                StorageArea UnStorageAreaDispo = storageAreaDispo.First();
                donationDetails.EmplacementDeStockageId = UnStorageAreaDispo.Id;
            }
            else donationDetails.EmplacementDeStockageId = null;

            DonationBusiness donationBusiness = new DonationBusiness();
            donationBusiness.CreateDonation(donationDetails);

            return RedirectToAction("Index", "Home");
        }

        public IActionResult Cancel(int idDon, int idAdherent)
        {
            DonationBusiness donationBu = new DonationBusiness();
            donationBu.CancelDonation(idDon);
            return RedirectToAction("Proposed", new { id = idAdherent });
        }

      
        public IActionResult AddToCart(int idDonation, int IdAdherent)
        {

            CartBusiness cartBu = new CartBusiness();
            List<Cart>ListCarts= cartBu.GetByIdAdherent(IdAdherent);
            Boolean AlreadyInCart = false;


            foreach(Cart cart in ListCarts)
            {
                if(idDonation == cart.DonId)
                {
                    AlreadyInCart = true;
                }
            }
            if(AlreadyInCart == false)
            {
                cartBu.addDonation(idDonation, IdAdherent);
                return RedirectToAction("Index", new {IdAdherent} );
            }
            else
            {
                return RedirectToAction("Index", new { IdAdherent });
            }

        }


        public IActionResult Proposed(int IdAdherent)

      {
           
            AdherentBusiness adherentBusiness = new AdherentBusiness();
            DonationBusiness DonationBusiness = new DonationBusiness();
            CategorieBusiness CategorieBusiness = new CategorieBusiness();
            TailleBusiness tailleBusiness = new TailleBusiness();
            RayonBusiness rayonBusiness = new RayonBusiness();
            TemperatureStockageBusiness temperatureStockageBusiness = new TemperatureStockageBusiness();
            UniteMesureBusiness uniteMesureBusiness = new UniteMesureBusiness();
            VilleCPBusiness villeCPBusiness = new VilleCPBusiness();
            ProduitBusiness produitBusiness = new ProduitBusiness();


            DonationSearchViewModel model = new DonationSearchViewModel();

            model.Produits = produitBusiness.GetAllProduit();
            model.Categories = CategorieBusiness.GetAllCategorie();
            model.Tailles = tailleBusiness.GetAllTaille();
            model.Rayons = rayonBusiness.GetAllRayon();
            model.TemperaturesStockage = temperatureStockageBusiness.GetAllTemperatureStockage();
            model.UnitesMesure = uniteMesureBusiness.GetAllUniteMesure();
            model.Villes = villeCPBusiness.GetAllVilleCP();
            model.Adherent = adherentBusiness.GetAdherentById(IdAdherent);
       
            model.Adherent = adherentBusiness.GetAdherentById(IdAdherent);

            model.DonationsDetails = DonationBusiness.GetDonationByIdAdherent(IdAdherent);
            model.Donationreserved = DonationBusiness.GetReservedDonationsByIdAdherent(IdAdherent);

            ViewBag.IdAdherent = IdAdherent;
            return View(model);
        }



        [HttpGet]
        public IActionResult ModificationAndCancel(int IdDon, int idAdherent)
        {
            DonationBusiness donationBusiness = new DonationBusiness();
            PlageHoraireBusiness plageHoraireBusiness = new PlageHoraireBusiness();
            VilleCPBusiness villeCPBusiness = new VilleCPBusiness();
            PartnerBusiness partnerBusiness = new PartnerBusiness();
            StorageAreaBusiness storageAreaBusiness = new StorageAreaBusiness();

            DonationDetails donationDetails = donationBusiness.GetDonationById(IdDon);
            donationDetails.Villes = villeCPBusiness.GetAllVilleCP();
            donationDetails.AdherentId = idAdherent;

            donationDetails.VilleCP = villeCPBusiness.GetVilleCPByNomDeVille(donationDetails.Ville);
            List<VilleCP> VillesProche = villeCPBusiness.listeVillesProximite(donationDetails.VilleCP, 30);


            List<Partner> result = new List<Partner>();
            foreach (VilleCP ville in VillesProche)
            {
                List<Partner> Partner = partnerBusiness.SearchPartnerDispo(ville.Id, donationDetails.TemperatureStockageId);
                result.AddRange(Partner);
            }
            donationDetails.Partners = result;

            ViewBag.IdAdherent = idAdherent;
            return View(donationDetails);

        }

        [HttpPost]
        public IActionResult ModificationAndCancel(DonationDetails model)
        {
            PlageHoraireBusiness plageHoraireBusiness = new PlageHoraireBusiness();
            DonationBusiness donationBusiness = new DonationBusiness();
            VilleCPBusiness villeCPBusiness = new VilleCPBusiness();
            donationBusiness.CancelDonation(model.Id);

            if (model.EmplacementDeStockageId != null && model.Ville != null) model.Ville = null;

            if (model.PartnerId != 0)
            {   //Récupération d'un emplacment de stockage en fonction du partenaire choisi, et du type de stockage
                StorageAreaBusiness storageAreaBu = new StorageAreaBusiness();
                List<StorageArea> storageAreaDispo = storageAreaBu.GetStorageAreaDispoByMulticritera(model.PartnerId, model.TemperatureStockageId);
                StorageArea UnStorageAreaDispo = storageAreaDispo.First();
                model.EmplacementDeStockageId = UnStorageAreaDispo.Id;
                model.Ville = null;
                model.VilleCpId = null;
            }

            if (model.VilleCpId == null && model.Ville != null && model.ADomicile == false)
            {
                model.EmplacementDeStockageId = null;
                model.VilleCP = villeCPBusiness.GetVilleCPByNomDeVille(model.Ville);
                model.VilleCpId = model.VilleCP.Id;
                
            }
            if (model.ADomicile) model.VilleCpId = null;

            int NewId = donationBusiness.CreateDonation(model);

            if (model.Lundi.DebutPlage1.ToString("H:mm") != "00:00" || model.Lundi.DebutPlage2.ToString("H:mm") != "00:00")
            {
                plageHoraireBusiness.InsertPlageHoraireForDon(NewId, model.Lundi,1);
            }     
            
            if (model.Mardi.DebutPlage1.ToString("H:mm") != "00:00" || model.Mardi.DebutPlage2.ToString("H:mm") != "00:00")
            {                                              
                plageHoraireBusiness.InsertPlageHoraireForDon(NewId, model.Mardi,2);
            }   
            
            if (model.Mercredi.DebutPlage1.ToString("H:mm") != "00:00" || model.Mercredi.DebutPlage2.ToString("H:mm") != "00:00")
            {                                              
                plageHoraireBusiness.InsertPlageHoraireForDon(NewId, model.Mercredi,3);
            }    
            
            if (model.Jeudi.DebutPlage1.ToString("H:mm") != "00:00" || model.Jeudi.DebutPlage2.ToString("H:mm") != "00:00")
            {                                            
                plageHoraireBusiness.InsertPlageHoraireForDon(NewId, model.Jeudi,4);
            }    
            
            if (model.Vendredi.DebutPlage1.ToString("H:mm") != "00:00" || model.Vendredi.DebutPlage2.ToString("H:mm") != "00:00")
            {                                            
                plageHoraireBusiness.InsertPlageHoraireForDon(NewId, model.Vendredi,5);
            }         
            
            if (model.Samedi.DebutPlage1.ToString("H:mm") != "00:00" || model.Samedi.DebutPlage2.ToString("H:mm") != "00:00")
            {                                           
                plageHoraireBusiness.InsertPlageHoraireForDon(NewId, model.Samedi,6);
            }

            if (model.Dimanche.DebutPlage1.ToString("H:mm") != "00:00" || model.Dimanche.DebutPlage2.ToString("H:mm") != "00:00")
            {
                plageHoraireBusiness.InsertPlageHoraireForDon(NewId, model.Dimanche,7);
            }

            ViewBag.IdAdherent = model.AdherentId;
            int IdAdherent = model.AdherentId;
            return RedirectToAction("Proposed", new {IdAdherent});

        }

    }
}
