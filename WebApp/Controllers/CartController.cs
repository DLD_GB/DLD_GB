﻿using Fr.EQL.AI110.DLD_GB.Business;
using Fr.EQL.AI110.DLD_GB.Entities;
using Fr.EQL.AI110.DLD_GB.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fr.EQL.AI110.DLD_GB.Web.Controllers
{
    public class CartController : Controller
    {
        public IActionResult Index(int IdAdherent)
        {
            ViewBag.IdAdherent = IdAdherent;
            AddCartViewModel model = new AddCartViewModel();
            CartBusiness cartBu = new CartBusiness();
            model.CartsDetails =  cartBu.GetAllCartDetailsByIdAdherent(IdAdherent);
            model.TotalArticleAuPanier = cartBu.TotalPanierByIdAdherent(IdAdherent);
            return View(model);
           
        }

        public IActionResult DeleteOneCart(int IdCart, int IdAdherent)
        {
            ViewBag.IdAdherent = IdAdherent;
            CartBusiness cartBu = new CartBusiness();
            cartBu.DeleteCartByIdCart(IdCart);
            return RedirectToAction("Index", new {IdAdherent});
         
        }

        public IActionResult ReserveOneCart(int IdToReserve, int idAdherent, int IdCart)
        {
            ViewBag.IdAdherent = idAdherent;
            DonationBusiness donationBusiness = new DonationBusiness();
            CartBusiness cartBu = new CartBusiness();
            donationBusiness.ReserveDonation(IdToReserve, idAdherent);
            cartBu.DeleteCartByIdCart(IdCart);


            return RedirectToAction("Index", new { idAdherent });

        }
    }
}
