﻿using Fr.EQL.AI110.DLD_GB.Business;
using Fr.EQL.AI110.DLD_GB.Entities;
using Fr.EQL.AI110.DLD_GB.Web.Models;
using Microsoft.AspNetCore.Mvc;


namespace Fr.EQL.AI110.DLD_GB.Web.Controllers
{
	public class StorageAreaController : Controller
	{
		[HttpGet]
		public IActionResult Update(int id)
		{
            StorageViewModel model = new StorageViewModel();

            TailleBusiness tailleBusiness = new TailleBusiness();
            VilleCPBusiness villeCPBusiness = new VilleCPBusiness();
            TemperatureStockageBusiness temperatureStockageBusiness = new TemperatureStockageBusiness();
            PartnerBusiness partenaireBusiness = new PartnerBusiness();
            StorageAreaBusiness storageAreaBu = new StorageAreaBusiness();

            model.Villes = villeCPBusiness.GetAllVilleCP();
            model.Tailles = tailleBusiness.GetAllTaille();
            model.Temperatures = temperatureStockageBusiness.GetAllTemperatureStockage();
            model.Partenaires = partenaireBusiness.GetAllPartner();         
            model.StorageArea = storageAreaBu.GetStorageAreaById(id);

			return View ("create", model);
		}

		[HttpPost]
		public IActionResult Update(StorageArea storageArea, int id)
		{
		    StorageAreaBusiness storageAreaBu = new StorageAreaBusiness();
            storageArea.Id = id;
		    storageAreaBu.UpdateStorageArea(storageArea);
            return RedirectToAction("Index", new { IdPartner = storageArea.PartenaireId});
		}

        [HttpGet]
        public IActionResult Create(int IdPartner)
        {
            StorageViewModel model = new StorageViewModel();

            PartnerBusiness partnerBusiness = new PartnerBusiness();
            TailleBusiness tailleBusiness = new TailleBusiness();
            VilleCPBusiness villeCPBusiness = new VilleCPBusiness();
            TemperatureStockageBusiness temperatureStockageBusiness = new TemperatureStockageBusiness();
            PartnerBusiness partenaireBusiness = new PartnerBusiness();

            // Alimenter le viewModel
            model.Villes = villeCPBusiness.GetAllVilleCP();
            model.Tailles = tailleBusiness.GetAllTaille();
            model.Temperatures = temperatureStockageBusiness.GetAllTemperatureStockage();
            model.Partenaires = partenaireBusiness.GetAllPartner();
            model.IdPartner = IdPartner;
            ViewBag.IdPartner = IdPartner;
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(StorageViewModel model)
        {
            StorageAreaBusiness storageAreaBusiness = new StorageAreaBusiness();
            PlageHoraireBusiness plageHoraireBusiness = new PlageHoraireBusiness();
            //List<PlageHoraire> plages = new List<PlageHoraire>();
            //plages.Add(model.PlageHoraireLundi);
            //plages.Add(model.PlageHoraireMardi);
            //plages.Add(model.PlageHoraireMercredi);
            //plages.Add(model.PlageHoraireJeudi);
            //plages.Add(model.PlageHoraireVendredi);
            //plages.Add(model.PlageHoraireSamedi);
            //plages.Add(model.PlageHoraireDimanche);

            storageAreaBusiness.SaveStorageArea(model.StorageArea);

            //foreach(PlageHoraire p in plages)
            //{
            //    plageHoraireBusiness.SavePlageHoraire(p, model.StorageArea);
            //}
            

            return RedirectToAction("Create");
        }

        public IActionResult Cancel(int id, int IdPartner)
        {
            StorageAreaBusiness storageAreaBusiness = new StorageAreaBusiness();
            storageAreaBusiness.CancelStorageArea(id);
            ViewBag.IdPartner = IdPartner;
            return RedirectToAction("Index", new {IdPartner});
        }

        public IActionResult Index(int IdPartner)
        {
            StorageViewModel model = new StorageViewModel();
            StorageAreaBusiness storageAreaBusiness = new StorageAreaBusiness();
            PlageHoraireBusiness plageHoraireBusiness = new PlageHoraireBusiness();

            ViewBag.IdPartner = IdPartner;
            model.StorageAreasDetails = storageAreaBusiness.GetStorageAreasByIdPartner(IdPartner);
           
            model.IdPartner = IdPartner;

            return View(model);
        }
    }
}
