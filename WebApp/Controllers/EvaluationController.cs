﻿using Fr.EQL.AI110.DLD_GB.Business;
using Fr.EQL.AI110.DLD_GB.Entities;
using Fr.EQL.AI110.DLD_GB.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Fr.EQL.AI110.DLD_GB.Web.Controllers
{
    public class EvaluationController : Controller
    {
        [HttpGet]
        public IActionResult Review(int idEchange)
        {
            EvaluateDonationViewModel model = new EvaluateDonationViewModel();

            EvaluationBusiness donationBusiness = new EvaluationBusiness();

            QualiteEchangeBusiness qualiteEchangeBusiness = new QualiteEchangeBusiness();
            QualiteProduitBusiness qualiteProduitBusiness = new QualiteProduitBusiness();
            EchangeBusiness echangeBusiness = new EchangeBusiness();

            model.QualitesEchange = qualiteEchangeBusiness.GetAllQualiteEchange();
            model.QualitesProduit = qualiteProduitBusiness.GetAllQualiteProduit();
            model.Echanges = echangeBusiness.GetEchange();
            model.EchangeId = idEchange;
            // charger la vue :
            return View(model);

            //return EditerDon("Création d'un don", null);

            //ViewBag.Rubrique = "Création d'un don";
            //return View("Edit");
        }

        [HttpPost]
        public IActionResult Review(EvaluateDonationViewModel model)
        {
            EvaluationBusiness evaluationBusiness = new EvaluationBusiness();

            QualiteEchangeBusiness qualiteEchangeBusiness = new QualiteEchangeBusiness();
            QualiteProduitBusiness qualiteProduitBusiness = new QualiteProduitBusiness();
            EchangeBusiness echangeBusiness = new EchangeBusiness();

            model.QualitesEchange = qualiteEchangeBusiness.GetAllQualiteEchange();
            model.QualitesProduit = qualiteProduitBusiness.GetAllQualiteProduit();
            model.Echanges = echangeBusiness.GetEchange();

            Evaluation evaluation = new Evaluation();

            evaluation.QualiteEchangeId = model.QualiteEchangeId;
            evaluation.QualiteProduitId = model.QualiteProduitId;
            evaluation.EchangeId = model.EchangeId;
            evaluation.NoteEvaluation = model.NoteEvaluation;
            evaluation.CommentaireEvaluation = model.CommentaireEvaluation;
            evaluation.DateEvaluation = model.DateEvaluation;

            // j'enregistre mon Don uniquement s'il respecte
            // les regles de validation :
            if (ModelState.IsValid)
            {
                evaluationBusiness.InsertEvaluation(evaluation);


                return View(model);
                //Console.WriteLine("ok");
                //return RedirectToAction("Index");
            }
            else
            {
                // si pas valide, j'affiche à nouveau la vue :
                //return EditerDon("Création d'un don", donation);
                return RedirectToAction("Review");
                //Console.WriteLine("ko");
            }
        }
    }
}

