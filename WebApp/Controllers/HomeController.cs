﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApp.Models;
using Fr.EQL.AI110.DLD_GB.Business;
using Fr.EQL.AI110.DLD_GB.Entities;
using Fr.EQL.AI110.DLD_GB.Web.Models;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {

        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            PartnerBusiness partnerBusiness = new PartnerBusiness();
            AdherentBusiness adherentBusiness = new AdherentBusiness(); 
            HomeViewModel homeViewModel = new HomeViewModel();


            homeViewModel.Partners = partnerBusiness.GetAllPartner();
            homeViewModel.Adherents = adherentBusiness.GetAllAdherents();

            return View(homeViewModel);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}