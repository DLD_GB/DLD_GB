﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.Entities;
using MySql.Data.MySqlClient;

namespace Fr.EQL.AI110.DLD_GB.DataAccess
{
    public class ProduitDAO : DAO
    {
        public List<Produit> GetAll()
        {

            List<Produit> result = new List<Produit>();
            DbConnection cnx = new MySqlConnection(CNX_STR);

            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = "SELECT * FROM Produit";

            try
            {
                cnx.Open();


                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Produit Produit = new Produit();

                    Produit.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    Produit.Nom = dr.GetString(dr.GetOrdinal("Nom"));
                    Produit.TemperatureStockageId = dr.GetInt32(dr.GetOrdinal("Temperature_Stockage_Id"));
                    Produit.CategorieId = dr.GetInt32(dr.GetOrdinal("Categorie_Id"));

                    result.Add(Produit);
                }
                return result;
            }
             catch (MySqlException e)
            {
                throw new Exception("Erreur lors de l'insertion d'un Produite. Details : "
               + e.Message);
            }
            finally
            {
                cnx.Close();
            }
        }

        public List<Produit> GetAllByIdCategorie(int id)
        {
            List<Produit> result = new List<Produit>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT produit.Id, produit.Categorie_Id, produit.Temperature_Stockage_Id, produit.Nom
                                FROM produit
                                WHERE categorie_id= @id";

            cmd.Parameters.Add(new MySqlParameter("id", id));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Produit produit = new Produit();

                    produit.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    produit.CategorieId = dr.GetInt32(dr.GetOrdinal("Categorie_Id"));
                    produit.TemperatureStockageId = dr.GetInt32(dr.GetOrdinal("Temperature_Stockage_Id"));
                    produit.Nom = dr.GetString(dr.GetOrdinal("Nom"));

                    result.Add(produit);
                }

                return result;
            }
            catch (MySqlException e)
            {
                throw new Exception("Erreur lors de l'insertion d'un Categoriee. Details : "
               + e.Message);
            }
            finally
            {

                cnx.Close();
            }
        }

        public Produit GetByIdProduit(int id)
        {
            Produit result = new Produit();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT *
                                FROM produit
                                WHERE id= @id";

            cmd.Parameters.Add(new MySqlParameter("id", id));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    result.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    result.CategorieId = dr.GetInt32(dr.GetOrdinal("Categorie_Id"));
                    result.TemperatureStockageId = dr.GetInt32(dr.GetOrdinal("Temperature_Stockage_Id"));
                    result.Nom = dr.GetString(dr.GetOrdinal("Nom"));
                }
                return result;
            }
            catch (MySqlException e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                cnx.Close();
            }
        }

        public List<Produit> GetByCategorieId(int categorieId)
        {

            List<Produit> result = new List<Produit>();

            DbConnection cnx = new MySqlConnection(CNX_STR);

            DbCommand cmd = cnx.CreateCommand();

            cmd.Parameters.Add(new MySqlParameter("categorieId", categorieId));

            cmd.CommandText = "SELECT * FROM Produit WHERE Categorie_Id = @CategorieId ";

            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

               

                    while (dr.Read())

                    {
                    Produit Produit = new Produit();

                    Produit.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                        Produit.Nom = dr.GetString(dr.GetOrdinal("Nom"));
                        Produit.TemperatureStockageId = dr.GetInt32(dr.GetOrdinal("Temperature_Stockage_Id"));
                        Produit.CategorieId = dr.GetInt32(dr.GetOrdinal("Categorie_Id"));

                        result.Add(Produit);
                    }

    
                cnx.Close();

                return result;
            }
            catch (MySqlException e)
            {
                throw new Exception("Erreur lors de l'insertion d'un Produite. Details : "
               + e.Message);
            }
        }

        public Produit GetById(int id)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);

            Produit Produit = new Produit();

            DbCommand cmd = cnx.CreateCommand();


            cmd.Parameters.Add(new MySqlParameter("id", id));


            cmd.CommandText = "SELECT * FROM Produit WHERE Id = @Id";
            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    Produit.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    Produit.Nom = dr.GetString(dr.GetOrdinal("Nom"));
                    Produit.CategorieId = dr.GetInt32(dr.GetOrdinal("Categorie_Id"));
                    Produit.TemperatureStockageId = dr.GetInt32(dr.GetOrdinal("Temperature_Stockage_Id"));
                }
                else
                {
                    Produit.Nom = "erreur";
                    return Produit;
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return Produit;
        }

        public Categorie GetCategorieParent(int idProduit)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);

            Categorie Categorie = new Categorie();

            DbCommand cmd = cnx.CreateCommand();


            cmd.Parameters.Add(new MySqlParameter("id", idProduit));


            cmd.CommandText = "SELECT Produit.Categorie_Id, Categorie.Nom FROM Produit LEFT JOIN  Categorie ON Produit.Categorie_Id = Categorie.Id WHERE Produit.Id = @id";

            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    Categorie.Id = dr.GetInt32(dr.GetOrdinal("Categorie_Id"));
                    Categorie.Nom = dr.GetString(dr.GetOrdinal("Nom"));

                }
                else
                {
                    Categorie.Nom = "erreur";
                    return Categorie;
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return Categorie;
        }

    }
}
