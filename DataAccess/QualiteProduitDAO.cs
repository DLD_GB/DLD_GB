﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.Entities;
using System.Data.Common;
using MySql.Data.MySqlClient;

namespace Fr.EQL.AI110.DLD_GB.DataAccess
{
    public class QualiteProduitDAO : DAO
    {
        public List<QualiteProduit> getAll()
        {
            List<QualiteProduit> result = new List<QualiteProduit>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT * from qualite_produit";

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    QualiteProduit qualiteProduit = new QualiteProduit();

                    qualiteProduit.Nom = dr.GetString(dr.GetOrdinal("libelle_quali_prod"));
                    qualiteProduit.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    result.Add(qualiteProduit);
                }


            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de l'annulation d'un don : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }



    }
}
