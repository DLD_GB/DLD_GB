﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.Entities;
using System.Data.Common;
using MySql.Data.MySqlClient;

namespace Fr.EQL.AI110.DLD_GB.DataAccess
{
    public class QualiteEchangeDAO : DAO
    {
        public List<QualiteEchange> getAll()
        {
            List<QualiteEchange> result = new List<QualiteEchange>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT * from qualite_echange";

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    QualiteEchange qualiteEchange = new QualiteEchange();

                    qualiteEchange.Nom = dr.GetString(dr.GetOrdinal("Libelle_quali_ech"));
                    qualiteEchange.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    result.Add(qualiteEchange);
                }


            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de l'annulation d'un don : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }



    }
}
