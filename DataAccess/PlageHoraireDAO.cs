﻿using Fr.EQL.AI110.DLD_GB.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.Entities;
using System.Data.Common;
using MySql.Data.MySqlClient;

namespace Fr.EQL.AI110.DLD_GB.DataAccess
{
    public class PlageHoraireDAO : DAO
    {
		//public void Insert(PlageHoraire plageHoraire, StorageArea storageArea)
  //      {
		//	DbConnection cnx = new MySqlConnection(CNX_STR);
		//	DbCommand cmd = cnx.CreateCommand();

		//	cmd.CommandText = @"INSERT INTO plage_horaire (Emplacement_de_stockage_Id, Jours_Id,
		//									Partenaire_Id, Debut_plage_1, Fin_plage_1,
		//									Debut_plage_2, Fin_plage_2)
		//							VALUES (@EmplacementDeStockageId, @JoursId, @PartenaireId, 
  //                                          @DebutPlage1, @FinPlage1, @DebutPlage2, @FinPlage2)";

		//	cmd.Parameters.Add(new MySqlParameter("EmplacementDeStockageId", storageArea.Id));
		//	cmd.Parameters.Add(new MySqlParameter("JoursId", plageHoraire.JoursId));
		//	cmd.Parameters.Add(new MySqlParameter("PartenaireId", storageArea.PartenaireId));
		//	cmd.Parameters.Add(new MySqlParameter("DebutPlage1", plageHoraire.DebutPlage1));
		//	cmd.Parameters.Add(new MySqlParameter("FinPlage1", plageHoraire.FinPlage1));
		//	cmd.Parameters.Add(new MySqlParameter("DebutPlage2", plageHoraire.DebutPlage2));
		//	cmd.Parameters.Add(new MySqlParameter("FinPlage2", plageHoraire.FinPlage2));

  //          try
  //          {
  //              cnx.Open();
		//		cmd.ExecuteNonQuery();
  //          }
		//	catch (Exception ex)
  //          {
		//		throw new Exception(ex.Message);
  //          }
		//	finally
  //          {
		//		cnx.Close();
  //          }
  //      }

   //     public List<PlageHoraire> GetAllByIdEspaceDeStockage(int idStorageArea)
   //     {
   //         List<PlageHoraire> result = new List<PlageHoraire>();

   //         DbConnection cnx = new MySqlConnection(CNX_STR);
   //         DbCommand cmd = cnx.CreateCommand();

			//cmd.CommandText = @"SELECT DISTINCT ph.Emplacement_de_stockage_Id 'espace_de_stockage', jr.jours_semaine 'jours',
			//							CASE WHEN ph.Debut_plage_1 IS NOT NULL AND ph.Fin_plage_1 IS NOT NULL THEN
			//									CONCAT (HOUR (ph.Debut_plage_1), 'h', MINUTE (ph.Debut_plage_1), ' - ', 
			//											HOUR (ph.Fin_plage_1), 'h', MINUTE (ph.Fin_plage_1))
			//								WHEN ph.Debut_plage_1 IS NOT NULL AND ph.Fin_plage_1 IS NULL THEN
			//									CONCAT (HOUR (ph.Debut_plage_1), 'h', MINUTE (ph.Debut_plage_1))
			//								END 'matin',
			//							CASE WHEN ph.Debut_plage_2 IS NOT NULL AND ph.Fin_plage_2 IS NOT NULL THEN
			//									CONCAT (HOUR (ph.Debut_plage_2), 'h', MINUTE (ph.Debut_plage_2), ' - ', 
			//											HOUR (ph.Fin_plage_2), 'h', MINUTE (ph.Fin_plage_2))
			//								WHEN ph.Debut_plage_2 IS NULL AND ph.Fin_plage_2 IS NOT NULL THEN
			//									CONCAT (HOUR (ph.Fin_plage_2), 'h', MINUTE (ph.Fin_plage_2))
			//								END 'apres-midi'
			//					FROM jours jr, plage_horaire ph
			//					WHERE ph.Emplacement_de_stockage_Id = @EmplacementDeStockageId AND ph.Jours_Id = jr.Id
			//					ORDER BY ph.Jours_Id";

			//cmd.Parameters.Add(new MySqlParameter("EmplacementDeStockageId", idStorageArea));

   //         try
   //         {
			//	cnx.Open();
			//	DbDataReader dr = cmd.ExecuteReader();

			//	while (dr.Read())
   //             {
			//		PlageHoraire plageHoraire = new PlageHoraire();

			//		plageHoraire.EmplacementDeStockageId = dr.GetInt32(dr.GetOrdinal("espace_de_stockage"));
			//		plageHoraire.NomJour = dr.GetString(dr.GetOrdinal("jours"));
			//		plageHoraire.DebutPlage1 = dr.GetDateTime(dr.GetOrdinal("matin"));
			//		plageHoraire.FinPlage1 = dr.GetDateTime(dr.GetOrdinal("matin"));
			//		plageHoraire.DebutPlage2 = dr.GetDateTime(dr.GetOrdinal("apres-midi"));
			//		plageHoraire.FinPlage2 = dr.GetDateTime(dr.GetOrdinal("apres-midi"));

			//		result.Add(plageHoraire);
   //             }
   //         }
			//catch (Exception ex)
   //         {
			//	throw new Exception("Erreur lors de la récupération " +
			//		"des plages horaires : " + ex.Message);
   //         }
			//finally
   //         {
			//	cnx.Close();
   //         }
			//return result;
   //     }

        public List<PlageHoraire> GetAllByIdDon(int IdDon)
        {

            List<PlageHoraire> result = new List<PlageHoraire>();
            DbConnection cnx = new MySqlConnection(CNX_STR);

            DbCommand cmd = cnx.CreateCommand();
            cmd.Parameters.Add(new MySqlParameter("idDon", IdDon));

            cmd.CommandText = @"select  don.Id 'don_Id', plage_Horaire.Id, jours.jours_semaine, 
                                        plage_Horaire.Debut_plage_1, plage_horaire.Fin_plage_1, 
                                        plage_horaire.Debut_plage_2, plage_horaire.Fin_plage_2, 
                                        plage_horaire.Jours_Id
                                From don
                                LEFT JOIN Emplacement_de_stockage ON don.emplacement_de_Stockage_Id = emplacement_de_stockage.Id
                                LEFT JOIN partenaire ON emplacement_de_stockage.Partenaire_Id = partenaire.Id
                                LEFT JOIN adherent ON don.Adherent_Id = Adherent.Id,
                                plage_horaire LEFT JOIN jours ON plage_horaire.Jours_Id = jours.Id

                                WHERE 
                                don.Id = @idDon AND(
                                plage_horaire.Don_Id = don.Id  OR 
                                plage_horaire.Emplacement_de_stockage_Id = emplacement_de_stockage.Id OR
                                plage_horaire.partenaire_Id = partenaire.Id  OR
                                plage_horaire.Adherent_Id = Adherent.Id AND (don.Emplacement_de_stockage_Id = 0 OR don.Emplacement_de_stockage_Id IS NULL ))
                                ";

            cnx.Open();
            DbDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                PlageHoraire PlageHoraire = new PlageHoraire();

                PlageHoraire.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                PlageHoraire.JoursId = dr.GetInt32(dr.GetOrdinal("Jours_Id"));
                PlageHoraire.DonId = dr.GetInt32(dr.GetOrdinal("Don_Id"));
                PlageHoraire.NomJour = dr.GetString(dr.GetOrdinal("jours_semaine"));
                PlageHoraire.DebutPlage1 = dr.GetDateTime(dr.GetOrdinal("Debut_plage_1"));
                PlageHoraire.FinPlage1 = dr.GetDateTime(dr.GetOrdinal("Fin_plage_1"));


                if (!dr.IsDBNull(dr.GetOrdinal("Debut_plage_2"))) PlageHoraire.DebutPlage2 = dr.GetDateTime(dr.GetOrdinal("Debut_plage_2"));
                if (!dr.IsDBNull(dr.GetOrdinal("Fin_plage_2"))) PlageHoraire.FinPlage2 = dr.GetDateTime(dr.GetOrdinal("Fin_plage_2"));

                result.Add(PlageHoraire);
            }
            cnx.Close();
            return result;

        }

        public PlageHoraire GetById(int id)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);

            PlageHoraire PlageHoraire = new PlageHoraire();

            DbCommand cmd = cnx.CreateCommand();


            cmd.Parameters.Add(new MySqlParameter("id", id));


            cmd.CommandText = "SELECT * FROM PlageHoraire WHERE Id = @Id";
            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    PlageHoraire.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    PlageHoraire.AdherentId = dr.GetInt32(dr.GetOrdinal("Adherent_Id"));
                    PlageHoraire.EmplacementDeStockageId = dr.GetInt32(dr.GetOrdinal("Emplacement_Stockage_Id"));
                    PlageHoraire.JoursId = dr.GetInt32(dr.GetOrdinal("Jours_Id"));
                    PlageHoraire.PartenaireId = dr.GetInt32(dr.GetOrdinal("Partenaire_Id"));
                    PlageHoraire.DonId = dr.GetInt32(dr.GetOrdinal("Don_Id"));
                    PlageHoraire.DebutPlage1 = dr.GetDateTime(dr.GetOrdinal("Debut_plage_1"));
                    PlageHoraire.FinPlage1 = dr.GetDateTime(dr.GetOrdinal("Fin_plage_1"));
                    PlageHoraire.DebutPlage2 = dr.GetDateTime(dr.GetOrdinal("Debut_plage_2"));
                    PlageHoraire.FinPlage2 = dr.GetDateTime(dr.GetOrdinal("Fin_plage_2"));
                }
                else
                {
                    PlageHoraire.Id = 0;
                    return PlageHoraire;
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return PlageHoraire;
        }

        public void InsertForDon(int IdDon, PlageHoraire plageHoraire, int jour)
        {
            // créer un objet connection :
            DbConnection cnx = new MySqlConnection();

            cnx.ConnectionString = CNX_STR;
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"INSERT INTO Plage_Horaire (Emplacement_de_stockage_Id, Don_Id, Debut_plage_1, 
                                            Fin_plage_1,Debut_plage_2,Fin_plage_2,Jours_Id)
                                            VALUES   (NULL,@Don_Id, @Debut_plage_1, @Fin_plage_1,@Debut_plage_2,@Fin_plage_2,@Jours_Id)";
                            

            cmd.Parameters.Add(new MySqlParameter("Don_Id", IdDon));
            cmd.Parameters.Add(new MySqlParameter("Debut_plage_1", plageHoraire.DebutPlage1));
            cmd.Parameters.Add(new MySqlParameter("Fin_plage_1", plageHoraire.FinPlage1));
            cmd.Parameters.Add(new MySqlParameter("Debut_plage_2", plageHoraire.DebutPlage2));
            cmd.Parameters.Add(new MySqlParameter("Fin_plage_2", plageHoraire.FinPlage2));
            cmd.Parameters.Add(new MySqlParameter("Jours_Id", jour));

            //try
            //{
            cnx.Open();
            cmd.ExecuteNonQuery();
            cnx.Close();
            //}
            //catch (MySqlException exc)
            //{
            //    throw new Exception("Erreur lors de l'insertion d'un don. Details : "
            //                            + exc.Message);
            //}
        }
    }
}
