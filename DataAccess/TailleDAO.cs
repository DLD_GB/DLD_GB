﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.Entities;
using System.Data.Common;
using MySql.Data.MySqlClient;

namespace Fr.EQL.AI110.DLD_GB.DataAccess
{
    public class TailleDAO : DAO
    {
        public List<Taille> GetAll()
        {

            List<Taille> result = new List<Taille>();
            DbConnection cnx = new MySqlConnection(CNX_STR);

            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = "SELECT * FROM Taille";

            try
            {
                cnx.Open();


                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Taille Taille = new Taille();

                    Taille.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    Taille.HauteurMoyenne = dr.GetInt32(dr.GetOrdinal("Hauteur_moyenne"));
                    Taille.LargeurMoyenne = dr.GetInt32(dr.GetOrdinal("Largeur_moyenne"));
                    Taille.LongueurMoyenne = dr.GetInt32(dr.GetOrdinal("Longeur_moyenne"));
                    Taille.NomTaille = dr.GetString(dr.GetOrdinal("nom_taille"));

                    result.Add(Taille);
                }

                cnx.Close();

                return result;
            }
            catch (MySqlException e)
            {
                throw new Exception("Erreur lors de l'insertion d'un Taillee. Details : "
               + e.Message);
            }

        }

    }
}
