﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Fr.EQL.AI110.DLD_GB.Entities;

namespace Fr.EQL.AI110.DLD_GB.DataAccess
{
    public class CartDAO : DAO
    {
        public void InsertDonation(int idDonation, int idAdherent)
        {
            DbConnection cnx = new MySqlConnection();
            cnx.ConnectionString = CNX_STR;
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"INSERT INTO Panier(Don_Id, Adherent_Id)
                                VALUES(@idDonation, @idAdherent)";
                                

            cmd.Parameters.Add(new MySqlParameter("idDonation", idDonation));
            cmd.Parameters.Add(new MySqlParameter("idAdherent", idAdherent));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
               
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de l'ajout d'un don dans le panier : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }

        public List<Cart> GetByIdAdherent(int id)
        {

            List<Cart> result = new List<Cart>();
            DbConnection cnx = new MySqlConnection(CNX_STR);

            DbCommand cmd = cnx.CreateCommand();


            cmd.Parameters.Add(new MySqlParameter("id", id));


            cmd.CommandText = "SELECT * FROM Panier WHERE Adherent_Id = @Id";

            try
            {
                cnx.Open();


                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Cart Cart = new Cart();

                    Cart.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    Cart.AdherentId = dr.GetInt32(dr.GetOrdinal("Adherent_Id"));
                    Cart.DonId = dr.GetInt32(dr.GetOrdinal("Don_Id"));

                    result.Add(Cart);
                }

                cnx.Close();

                return result;
            }
            catch (MySqlException e)
            {
                throw new Exception("Erreur lors de l'insertion d'un Carte. Details : "
               + e.Message);
            }

        }

        public List<CartDetails> GetAllCartDetailsByIdAdherent(int id)
        {
            List<CartDetails> result = new List<CartDetails>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT panier.*, panier.adherent_id 'adherentId', adherent.nom 'adherentNom', produit.Nom 'produit', unite_de_mesure.Nom 'unité de mesure', 
                                       don.Quantite_produit 'quantité', categorie.Nom 'categorie', produit.picture 'picture'
                                FROM panier
                                LEFT JOIN adherent ON panier.adherent_id=adherent.id
                                LEFT JOIN don on panier.Don_Id = don.ID
                                LEFT JOIN produit ON don.Produit_Id = produit.ID
                                LEFT JOIN unite_de_mesure ON don.Unite_de_mesure_Id = unite_de_mesure.id
                                LEFT JOIN categorie ON produit.Categorie_Id = categorie.id
                                WHERE panier.Adherent_Id = @id";

            cmd.Parameters.Add(new MySqlParameter("id", id));

            try
            {
                cnx.Open();


                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    CartDetails cartDetails = new CartDetails();
                    cartDetails.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    cartDetails.DonId= dr.GetInt32(dr.GetOrdinal("Don_Id"));
                    cartDetails.AdherentId = dr.GetInt32(dr.GetOrdinal("adherentId"));
                    cartDetails.AdherentNom = dr.GetString(dr.GetOrdinal("adherentNom"));
                    cartDetails.ProduitNom = dr.GetString(dr.GetOrdinal("Produit"));
                    cartDetails.UniteDeMesure = dr.GetString(dr.GetOrdinal("unité de mesure"));
                    cartDetails.Quantite = dr.GetInt32(dr.GetOrdinal("quantité"));
                    cartDetails.Categorie = dr.GetString(dr.GetOrdinal("categorie"));
                    cartDetails.Picture = dr.GetString(dr.GetOrdinal("picture"));


                    result.Add(cartDetails);
                }

                cnx.Close();

                return result;
            }
            catch (MySqlException e)
            {
                throw new Exception("Erreur lors de l'insertion d'un Carte. Details : "
               + e.Message);
            }
        }

        public void DeleteByIdCart(int idCart)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);

            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"DELETE FROM panier  
                                WHERE id = @id";

            cmd.Parameters.Add(new MySqlParameter("id", idCart));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();

            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de la suppresion d'un don au panier : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }
    }
    
}
