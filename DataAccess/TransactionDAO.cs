﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.Entities;
using System.Data.Common;
using MySql.Data.MySqlClient;

namespace Fr.EQL.AI110.DLD_GB.DataAccess
{
    public class TransactionDAO : DAO
    {
        public List<Transaction> GetAll()
        {

            List<Transaction> result = new List<Transaction>();
            DbConnection cnx = new MySqlConnection(CNX_STR);

            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = "SELECT * FROM Transaction";

            try
            {
                cnx.Open();


                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Transaction Transaction = new Transaction();

                    Transaction.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    Transaction.AdherentId = dr.GetInt32(dr.GetOrdinal("Adherent_Id"));
                    Transaction.ModePaiementId= dr.GetInt32(dr.GetOrdinal("Mode_Paiement_Id"));
                    Transaction.AugmentationCredit = dr.GetInt32(dr.GetOrdinal("Augmentation_credit"));
                    Transaction.MontantPaiement = dr.GetInt32(dr.GetOrdinal("Montant_paiement"));
                    Transaction.DiminutionCredit = dr.GetInt32(dr.GetOrdinal("Diminution_credit"));
                    Transaction.DateTransaction = dr.GetDateTime(dr.GetOrdinal("Date_transaction"));

                    result.Add(Transaction);
                }

                cnx.Close();

                return result;
            }
            catch (MySqlException e)
            {
                throw new Exception("Erreur lors de l'insertion d'un Transactione. Details : "
               + e.Message);
            }

        }


        public Transaction GetById(int id)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);

            Transaction Transaction = new Transaction();

            DbCommand cmd = cnx.CreateCommand();


            cmd.Parameters.Add(new MySqlParameter("id", id));


            cmd.CommandText = "SELECT * FROM Transaction WHERE Id = @Id";
            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    Transaction.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    Transaction.AdherentId = dr.GetInt32(dr.GetOrdinal("Adherent_Id"));
                    Transaction.ModePaiementId = dr.GetInt32(dr.GetOrdinal("Mode_Paiement_Id"));
                    Transaction.AugmentationCredit = dr.GetInt32(dr.GetOrdinal("Augmentation_credit"));
                    Transaction.MontantPaiement = dr.GetInt32(dr.GetOrdinal("Montant_paiement"));
                    Transaction.DiminutionCredit = dr.GetInt32(dr.GetOrdinal("Diminution_credit"));
                    Transaction.DateTransaction = dr.GetDateTime(dr.GetOrdinal("Date_transaction"));
                }
                else
                {
                    Transaction.Id = 0;
                    return Transaction;
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return Transaction;
        }

        public List<Transaction> GetByIdAdherent(int id)
        {

            List<Transaction> result = new List<Transaction>();
            DbConnection cnx = new MySqlConnection(CNX_STR);

            DbCommand cmd = cnx.CreateCommand();


            cmd.Parameters.Add(new MySqlParameter("id", id));


            cmd.CommandText = "SELECT * FROM Transaction WHERE Adherent_Id = @Id";

            try
            {
                cnx.Open();


                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Transaction Transaction = new Transaction();

                    Transaction.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    Transaction.AdherentId = dr.GetInt32(dr.GetOrdinal("Adherent_Id"));
                    Transaction.ModePaiementId = dr.GetInt32(dr.GetOrdinal("Mode_Paiement_Id"));
                    Transaction.AugmentationCredit = dr.GetInt32(dr.GetOrdinal("Augmentation_credit"));
                    Transaction.MontantPaiement = dr.GetInt32(dr.GetOrdinal("Montant_paiement"));
                    Transaction.DiminutionCredit = dr.GetInt32(dr.GetOrdinal("Diminution_credit"));
                    Transaction.DateTransaction = dr.GetDateTime(dr.GetOrdinal("Date_transaction"));

                    result.Add(Transaction);
                }

                cnx.Close();

                return result;
            }
            catch (MySqlException e)
            {
                throw new Exception("Erreur lors de l'insertion d'un Transactione. Details : "
               + e.Message);
            }

        }
    }
}
