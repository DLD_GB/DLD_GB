﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.Entities;
using System.Data.Common;
using MySql.Data.MySqlClient;

namespace Fr.EQL.AI110.DLD_GB.DataAccess
{
    public class PartnerDAO : DAO
    {
        public List<Partner> GetAll()
        {
            List<Partner> result = new List<Partner>();

            DbConnection cnx = new MySqlConnection(CNX_STR);

            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = "SELECT * FROM Partenaire";

            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Partner Partner = new Partner();

                    Partner.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    Partner.Siret = dr.GetDouble(dr.GetOrdinal("Siret"));
                    Partner.NomSociete = dr.GetString(dr.GetOrdinal("Nom_Societe"));
                    Partner.Telephone = dr.GetString(dr.GetOrdinal("Telephone"));
                    Partner.Email = dr.GetString(dr.GetOrdinal("Email"));
                    Partner.NomReferent = dr.GetString(dr.GetOrdinal("Nom_referent"));
                    Partner.PrenomReferent = dr.GetString(dr.GetOrdinal("Prenom_referent"));
                    Partner.MotDePasse = dr.GetString(dr.GetOrdinal("Mot_de_passe"));
                    if (!dr.IsDBNull(dr.GetOrdinal("Date_saisie_inscription"))) Partner.DateSaisieInscription = dr.GetDateTime(dr.GetOrdinal("Date_saisie_inscription"));
                    if (!dr.IsDBNull(dr.GetOrdinal("Date_validation_inscription"))) Partner.DateValidationInscription = dr.GetDateTime(dr.GetOrdinal("Date_validation_inscription"));
                    if (!dr.IsDBNull(dr.GetOrdinal("Date_desinscription"))) Partner.DateDesinscription = dr.GetDateTime(dr.GetOrdinal("Date_desinscription"));
                    if (!dr.IsDBNull(dr.GetOrdinal("Date_reinscription"))) Partner.DateReinscritpion = dr.GetDateTime(dr.GetOrdinal("Date_reinscription"));
                    Partner.NomEtNDeVoie = dr.GetString(dr.GetOrdinal("Nom_et_n_de_voie"));

                    result.Add(Partner);
                }

                cnx.Close();

                return result;
            }
            catch (MySqlException e)
            {
                throw new Exception("Erreur lors de l'insertion d'un Partner. Details : " + e.Message);
            }

        }

        public List<Partner> GetByMulticriteria(string name, double siret, int villeId)
        {
            List<Partner> result = new List<Partner>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT partenaire.id 'id', partenaire.siret, partenaire.nom_societe, partenaire.nom_referent, partenaire.prenom_referent
                                FROM partenaire
                                WHERE nom_societe LIKE @name
                                    AND (SIRET = @siret OR @siret =0)
                                    AND (Ville_CP_Id = @villeId OR @VilleId=0)
                                    ";

            name = "%" + name + "%";
            cmd.Parameters.Add(new MySqlParameter("name", name));
            cmd.Parameters.Add(new MySqlParameter("siret", siret));
            cmd.Parameters.Add(new MySqlParameter("villeId", villeId));

            try
            {
                cnx.Open();


                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Partner Partner = new Partner();

                    Partner.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    Partner.Siret = dr.GetDouble(dr.GetOrdinal("Siret"));
                    Partner.NomSociete = dr.GetString(dr.GetOrdinal("Nom_Societe"));
                    Partner.NomReferent = dr.GetString(dr.GetOrdinal("Nom_referent"));
                    Partner.PrenomReferent = dr.GetString(dr.GetOrdinal("Prenom_referent"));


                    result.Add(Partner);
                }
                return result;
            }
            catch (MySqlException e)
            {
                throw new Exception("Erreur lors de la récupération des partenaires : " + e.Message);
            }
            finally
            {
                cnx.Close();
            }
        }

        public List<Partner> GetAllPartnersDispoByMulticriteria(int villeId, int temperatureStockageId)
        {
            List<Partner> result = new List<Partner>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT DISTINCT partenaire.id 'id', partenaire.nom_societe, ville_cp.ville 'ville'
                                FROM partenaire
                                LEFT JOIN emplacement_de_stockage ON emplacement_de_stockage.partenaire_Id = partenaire.Id
                                LEFT JOIN ville_cp ON partenaire.ville_cp_id = ville_cp.Id
                                WHERE partenaire.Ville_CP_Id = @villeId 
                                    AND (emplacement_de_stockage.disponible = @disponibilite)
                                    AND (emplacement_de_stockage.temperature_stockage_id = @temperatureStockageId)
                                    ";
            cmd.Parameters.Add(new MySqlParameter("villeId", villeId));
            //cmd.Parameters.Add(new MySqlParameter("villeId1", model.VillesProche.First());

            cmd.Parameters.Add(new MySqlParameter("disponibilite", 1));
            cmd.Parameters.Add(new MySqlParameter("temperatureStockageId", temperatureStockageId));

            try
            {
                cnx.Open();


                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Partner Partner = new Partner();

                    Partner.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    Partner.NomSociete = dr.GetString(dr.GetOrdinal("Nom_Societe"));

                    result.Add(Partner);
                }
                return result;
            }
            catch (MySqlException e)
            {
                throw new Exception("Erreur lors de la récupération des partenaires : " + e.Message);
            }
            finally
            {
                cnx.Close();
            }
        }

        public List<Partner> GetDonationStockByMultiCriteria(int id)
        {

            List<Partner> result = new List<Partner>();

            DbConnection cnx = new MySqlConnection(CNX_STR);

            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = "SELECT * FROM Partenaire" +
                "WHERE partenaire.id = @Id";

            cmd.CommandText = @"SELECT * FROM Partenaire WHERE partenaire.id = @Id";

            //WHERE adherent.prenom = @Prenom";
            //WHERE adherent.nom = @Nom";

            cmd.Parameters.Add(new MySqlParameter("Id", id));
            //cmd.Parameters.Add(new MySqlParameter("Prenom", prenom));
            //cmd.Parameters.Add(new MySqlParameter("Nom", nom));

            try
            {
                cnx.Open();


                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Partner Partner = new Partner();

                    Partner.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    Partner.Siret = dr.GetInt32(dr.GetOrdinal("Id"));
                    Partner.NomSociete = dr.GetString(dr.GetOrdinal("Nom_Societe"));
                    Partner.Telephone = dr.GetString(dr.GetOrdinal("Telephone"));
                    Partner.Email = dr.GetString(dr.GetOrdinal("Email"));
                    Partner.NomReferent = dr.GetString(dr.GetOrdinal("Nom_referent"));
                    Partner.PrenomReferent = dr.GetString(dr.GetOrdinal("Prenom_referent"));
                    Partner.MotDePasse = dr.GetString(dr.GetOrdinal("Mot_de_passe"));
                    if (!dr.IsDBNull(dr.GetOrdinal("Date_saisie_inscription"))) Partner.DateSaisieInscription = dr.GetDateTime(dr.GetOrdinal("Date_saisie_inscription"));
                    if (!dr.IsDBNull(dr.GetOrdinal("Date_validation_inscription"))) Partner.DateValidationInscription = dr.GetDateTime(dr.GetOrdinal("Date_validation_inscription"));
                    if (!dr.IsDBNull(dr.GetOrdinal("Date_desinscription"))) Partner.DateDesinscription = dr.GetDateTime(dr.GetOrdinal("Date_desinscription"));
                    if (!dr.IsDBNull(dr.GetOrdinal("Date_reinscription"))) Partner.DateReinscritpion = dr.GetDateTime(dr.GetOrdinal("Date_reinscription"));
                    Partner.NomEtNDeVoie = dr.GetString(dr.GetOrdinal("Nom_et_n_de_voie"));

                    result.Add(Partner);
                }

                cnx.Close();

                return result;
            }
            catch (MySqlException e)
            {
                throw new Exception("Erreur lors de l'insertion d'un Partner. Details : "
               + e.Message);
            }

        }
    }
}    
