﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data.Common;
using Fr.EQL.AI110.DLD_GB.Entities;

namespace Fr.EQL.AI110.DLD_GB.DataAccess
{
    public class EvaluationDAO : DAO
    {
        public void Insert(Evaluation evaluation)
        {
            // créer un objet connection :
            DbConnection cnx = new MySqlConnection();

            // 1 - Paramétrer la connection
            // :
            cnx.ConnectionString = CNX_STR;

            // 2 - Paramétrer la commande :
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"INSERT INTO evaluation
                                (Id, Qualite_Echange_Id, Qualite_Produit_Id, Echange_Id, Note_Evaluation, Commentaire_Evaluation, Date_Evaluation) 
                                VALUES 
                                (@id, @qualiteEchangeId, @qualiteProduitId, @echangeId, @noteEvaluation, @commentaireEvaluation, SYSDATE())";

            cmd.Parameters.Add(new MySqlParameter("id", evaluation.Id));
            cmd.Parameters.Add(new MySqlParameter("qualiteEchangeId", evaluation.QualiteEchangeId));
            cmd.Parameters.Add(new MySqlParameter("qualiteProduitId", evaluation.QualiteProduitId));
            cmd.Parameters.Add(new MySqlParameter("echangeId", evaluation.EchangeId));
            cmd.Parameters.Add(new MySqlParameter("noteEvaluation", evaluation.NoteEvaluation));
            cmd.Parameters.Add(new MySqlParameter("commentaireEvaluation", evaluation.CommentaireEvaluation));
            cmd.Parameters.Add(new MySqlParameter("dateEvaluation", evaluation.DateEvaluation));

            try
            {
                // 3 - ouvrir la connection :
                cnx.Open();

                // 4 - Exécuter la commande :
                int nbLignes = cmd.ExecuteNonQuery();

                // 6 - fermer la conneciton :
                cnx.Close();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de l'insertion d'une évaluation. Details : "
                                        + exc.Message);
            }
        }
    }
}
