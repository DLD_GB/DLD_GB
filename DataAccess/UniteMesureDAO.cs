﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.Entities;
using System.Data.Common;
using MySql.Data.MySqlClient;

namespace Fr.EQL.AI110.DLD_GB.DataAccess
{
    public class UniteMesureDAO : DAO
    {
        public List<UniteMesure> GetAll()
        {

            List<UniteMesure> result = new List<UniteMesure>();
            DbConnection cnx = new MySqlConnection(CNX_STR);

            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = "SELECT * FROM unite_de_mesure";

            try
            {
                cnx.Open();


                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    UniteMesure UniteMesure = new UniteMesure();

                    UniteMesure.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    UniteMesure.Nom = dr.GetString(dr.GetOrdinal("nom"));

                    result.Add(UniteMesure);
                }

                cnx.Close();

                return result;
            }
            catch (MySqlException e)
            {
                throw new Exception("Erreur lors de l'insertion d'un UniteMesuree. Details : "
               + e.Message);
            }

        }

    }
}
