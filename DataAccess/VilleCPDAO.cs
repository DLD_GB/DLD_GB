﻿using Fr.EQL.AI110.DLD_GB.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GB.DataAccess
{
    public class VilleCPDAO : DAO
    {
        public List<VilleCP> GetAll()
        {
            List<VilleCP> villeCPs = new List<VilleCP>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = "SELECT * FROM ville_cp";

            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    VilleCP vCP = new VilleCP();
                    vCP.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    vCP.Ville = dr.GetString(dr.GetOrdinal("Ville"));
                    vCP.Cp = dr.GetInt32(dr.GetOrdinal("Code_postal"));
                    vCP.Latitude = dr.GetDouble(dr.GetOrdinal("Latitude"));
                    vCP.Longitude = dr.GetDouble(dr.GetOrdinal("Longitude"));

                    villeCPs.Add(vCP);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return villeCPs;
        }

        public VilleCP GetByNomDeVille(string Ville)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            
            VilleCP vCP = new VilleCP();
          
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM ville_cp";

            
            cmd.Parameters.Add(new MySqlParameter("Ville", Ville));

            cmd.CommandText = "SELECT * FROM ville_cp WHERE Ville = @Ville";
            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    vCP.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    vCP.Ville = dr.GetString(dr.GetOrdinal("Ville"));
                    vCP.Cp = dr.GetInt32(dr.GetOrdinal("Code_postal"));
                    vCP.Latitude = dr.GetDouble(dr.GetOrdinal("Latitude"));
                    vCP.Longitude = dr.GetDouble(dr.GetOrdinal("Longitude"));
                }
                else
                {
                    vCP.Ville = "erreur";
                    return vCP;
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return vCP;
        }
    
        public VilleCP GetByNomDeVilleEtCP(string Ville, string CP)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);

            VilleCP vCP = new VilleCP();

            DbCommand cmd = cnx.CreateCommand();


            cmd.Parameters.Add(new MySqlParameter("Ville", Ville));
            cmd.Parameters.Add(new MySqlParameter("CP", CP));

            cmd.CommandText = "SELECT * FROM ville_cp WHERE Ville = @Ville AND Code_Postal = @CP";
            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    vCP.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    vCP.Ville = dr.GetString(dr.GetOrdinal("Ville"));
                    vCP.Cp = dr.GetInt32(dr.GetOrdinal("Code_postal"));
                    vCP.Latitude = dr.GetDouble(dr.GetOrdinal("Latitude"));
                    vCP.Longitude = dr.GetDouble(dr.GetOrdinal("Longitude"));
                }
                else
                {
                    vCP.Ville = "erreur";
                    return vCP;
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return vCP;
        }

        public VilleCP GetByIdVille(int id)
        {

            DbConnection cnx = new MySqlConnection(CNX_STR);
            VilleCP result = new VilleCP();
            DbCommand cmd = cnx.CreateCommand();


            cmd.CommandText = "SELECT * FROM ville_cp WHERE ville_cp.Id = @id";
            cmd.Parameters.Add(new MySqlParameter("id", id));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    result.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    result.Ville = dr.GetString(dr.GetOrdinal("Ville"));
                    result.Cp = dr.GetInt32(dr.GetOrdinal("Code_postal"));
                    result.Latitude = dr.GetDouble(dr.GetOrdinal("Latitude"));
                    result.Longitude = dr.GetDouble(dr.GetOrdinal("Longitude"));
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }
    }
}
