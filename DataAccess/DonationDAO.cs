﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;
using System.Data.Common;
using System;
using System.Data.SqlClient;

using Fr.EQL.AI110.DLD_GB.Entities;

namespace Fr.EQL.AI110.DLD_GB.DataAccess
{
    public class DonationDAO : DAO
    {
        public List<DonationDetails> GetByMultiCriteria(int RayonId, int CategorieId, int ProduitId,
            string NomRayon, string NomCategorie, string NomProduit, string UniteMesure, int QuantiteProduit,
        string TemperatureStockage, string Ville, string TypeStockage, bool? Box, bool? Reservable, string ListeVillesProches, string ListeCategories, string ListeRayons, string ListeProduits)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            List<DonationDetails> result = new List<DonationDetails>();
            DbCommand cmd = cnx.CreateCommand();
            PlageHoraireDAO plageHoraireDAO = new PlageHoraireDAO();

            cmd.Parameters.Add(new MySqlParameter("RayonId", RayonId));
            cmd.Parameters.Add(new MySqlParameter("CategorieId", CategorieId));
            cmd.Parameters.Add(new MySqlParameter("ProduitId", ProduitId));

            NomRayon = "%" + NomRayon + "%";
            cmd.Parameters.Add(new MySqlParameter("NomRayon", NomRayon));
            NomCategorie = "%" + NomCategorie + "%";
            cmd.Parameters.Add(new MySqlParameter("NomCategorie", NomCategorie));
            NomProduit = "%" + NomProduit + "%";
            cmd.Parameters.Add(new MySqlParameter("NomProduit", NomProduit));
            UniteMesure = "%" + UniteMesure + "%";
            cmd.Parameters.Add(new MySqlParameter("UniteMesure", UniteMesure));

            cmd.Parameters.Add(new MySqlParameter("QuantiteProduit", QuantiteProduit));
            TemperatureStockage = "%" + TemperatureStockage + "%";
            cmd.Parameters.Add(new MySqlParameter("TemperatureStockage", TemperatureStockage));
            Ville = "%" + Ville + "%";
            cmd.Parameters.Add(new MySqlParameter("Ville", Ville));

            cmd.Parameters.Add(new MySqlParameter("Box", Box));
            cmd.Parameters.Add(new MySqlParameter("Reservable", Reservable));
            cmd.Parameters.Add(new MySqlParameter("ListeVillesProches", ListeVillesProches));
            cmd.Parameters.Add(new MySqlParameter("ListeCategories", ListeCategories));
            cmd.Parameters.Add(new MySqlParameter("ListeRayons", ListeRayons));
            cmd.Parameters.Add(new MySqlParameter("ListeProduits", ListeProduits));

            cmd.CommandText = @"SELECT don.*, 
                                        produit.Nom 'Nom Produit',
                                        categorie.Nom 'Catégorie', 
                                        emplacement_de_stockage.Box_automatique 'Box', 
                                        temperature_stockage.Nom 'Temperature Stockage', 
                                        rayon.Nom 'Rayon',
                                        unite_de_mesure.nom 'Unité de mesure',
                                        (( SELECT don.Nom_et_n_de_voie WHERE don.Ville_CP_Id > 0)
                                        UNION (SELECT adherent.Nom_et_n_de_voie WHERE (don.Emplacement_de_stockage_Id = 0 OR don.Emplacement_de_stockage_Id IS NULL) AND don.Nom_et_n_de_voie IS NULL )
                                        UNION (SELECT emplacement_de_stockage.Nom_et_n_de_voie WHERE don.Emplacement_de_stockage_Id > 0 AND emplacement_de_stockage.Ville_CP_Id > 0 )
                                        UNION (SELECT partenaire.Nom_et_n_de_voie WHERE  emplacement_de_stockage.Ville_CP_Id = 0)) 'Nom et numéro de voie',
                                        villeDon.Code_postal 'CP don', 
                                        villeDon.Ville 'Ville don',
                                        produit.Categorie_Id 'CategorieId',
                                        categorie.Rayon_Id 'RayonId',
                                        adherent.Nom_utilisateur 'Nom Donateur',
                                        produit.picture

                                    FROM don
	                                    LEFT JOIN adherent ON don.Adherent_Id = adherent.Id
	                                    LEFT JOIN produit ON don.Produit_Id = produit.id
                                        LEFT JOIN categorie ON produit.Categorie_Id = categorie.Id
	                                    LEFT JOIN emplacement_de_stockage ON (don.Emplacement_de_stockage_Id = emplacement_de_stockage.Id)
                                        LEFT JOIN partenaire ON partenaire.Id = emplacement_de_stockage.Partenaire_Id
                                        LEFT JOIN ville_cp AS villeDon ON (emplacement_de_stockage.Ville_CP_Id=villeDon.Id) 
		                                    OR (don.Ville_CP_Id=villeDon.Id)
                                            OR ((adherent.Ville_CP_Id=villeDon.Id) AND (don.Ville_CP_Id =0 OR don.Ville_CP_Id IS NULL) AND (don.Emplacement_de_stockage_Id =0 OR don.Emplacement_de_stockage_Id IS NULL))
                                            OR ((partenaire.Ville_CP_Id=villeDon.Id) AND (emplacement_de_stockage.Ville_CP_Id = 0 OR emplacement_de_stockage.Ville_CP_Id IS NULL))
	                                    LEFT JOIN temperature_stockage ON produit.Temperature_Stockage_Id=temperature_stockage.Id
	                                    LEFT JOIN rayon ON categorie.Rayon_Id = rayon.Id
                                        LEFT JOIN unite_de_mesure ON unite_de_mesure.Id = don.Unite_de_mesure_Id

                                 WHERE
                                (emplacement_de_stockage.Box_automatique = @Box OR @Box IS NULL) AND
                                (don.Quantite_produit = @QuantiteProduit OR @QuantiteProduit = 0) AND
                                (don.Produit_Id = @ProduitId OR @ProduitId = 0) AND
                                (categorie.Rayon_Id = @RayonId OR @RayonId = 0) AND
                                (produit.Categorie_Id = @CategorieId OR @CategorieId = 0) AND
                                (produit.Nom LIKE @NomProduit OR @NomProduit IS NULL) AND
                                (categorie.Nom LIKE @NomCategorie OR @NomCategorie IS NULL) AND
                                (rayon.Nom LIKE @NomCategorie OR @NomCategorie IS NULL) AND
                                (temperature_stockage.Nom LIKE @TemperatureStockage OR @TemperatureStockage IS NULL) AND
                                (don.Reservable = @Reservable OR @Reservable IS NULL) AND
                                (unite_de_mesure.nom LIKE @UniteMesure OR @UniteMesure IS NULL) AND
                                (INSTR(@ListeVillesProches, villeDon.Ville ) > 0 OR @listeVillesProches IS NULL) AND
                                (INSTR(@ListeCategories, categorie.Nom ) > 0 OR @listeCategories IS NULL) AND
                                (INSTR(@ListeRayons, rayon.Nom ) > 0 OR @ListeRayons IS NULL) AND
                                (INSTR(@ListeProduits, produit.Nom ) > 0 OR @ListeProduits IS NULL) 
                                ";
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    DonationDetails DonationD = new DonationDetails();

                    DonationD.RayonId = dr.GetInt32(dr.GetOrdinal("RayonId"));
                    DonationD.ProduitId = dr.GetInt32(dr.GetOrdinal("Produit_Id"));
                    DonationD.CategorieId = dr.GetInt32(dr.GetOrdinal("CategorieId"));
                    DonationD.NomEtNDeVoie = dr.GetString(dr.GetOrdinal("Nom et numéro de voie"));
                    DonationD.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    DonationD.NomRayon = dr.GetString(dr.GetOrdinal("Rayon"));
                    DonationD.NomProduit = dr.GetString(dr.GetOrdinal("Nom Produit"));
                    DonationD.NomCategorie = dr.GetString(dr.GetOrdinal("Catégorie"));
                    DonationD.UniteMesure = dr.GetString(dr.GetOrdinal("Unité de mesure"));
                    DonationD.QuantiteProduit = dr.GetInt32(dr.GetOrdinal("Quantite_Produit"));
                    DonationD.TemperatureStockage = dr.GetString(dr.GetOrdinal("Temperature Stockage"));
                    DonationD.Ville = dr.GetString(dr.GetOrdinal("Ville don"));
                    DonationD.CodePostal = dr.GetInt32(dr.GetOrdinal("CP don"));
                    if (!dr.IsDBNull(dr.GetOrdinal("Box"))) DonationD.Box = dr.GetBoolean(dr.GetOrdinal("Box"));
                    DonationD.Plages = plageHoraireDAO.GetAllByIdDon(dr.GetInt32(dr.GetOrdinal("Id")));
                    DonationD.DatePeremption = dr.GetDateTime(dr.GetOrdinal("Date_peremption"));
                    result.Add(DonationD);
                    DonationD.NomDonateur = dr.GetString(dr.GetOrdinal("Nom Donateur"));
                    DonationD.AdherentId = dr.GetInt32(dr.GetOrdinal("Adherent_Id"));
                    DonationD.picture = dr.GetString(dr.GetOrdinal("picture"));

                }
                cnx.Close();
                return result;
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de la recherche du don. Details : "
                                        + exc.Message);

            }
        }

        public int Insert(Donation donation)
        {
            // créer un objet connection :
            MySqlConnection cnx = new MySqlConnection();

            cnx.ConnectionString = CNX_STR;

            string sql = @"INSERT INTO don (Adherent_Id, Emballage_Id, Produit_Id, Unite_de_mesure_Id, Ville_CP_Id, Date_entree_proposition, 
                                                Date_peremption, Nom_et_n_de_voie, Quantite_produit, Reservable, emplacement_de_stockage_id) 
                                       VALUES   (@adherentId, @emballageId, @produitId, @uniteDeMesureId, @villeCpId, SYSDATE(), 
                                                @datePeremption, @nomEtNDeVoie, @quantiteProduit, 1, @emplacementStockageId);
                                UPDATE emplacement_de_stockage SET disponible = 0 WHERE emplacement_de_stockage.id =@emplacementStockageId;
                                SELECT * FROM don ORDER BY ID DESC LIMIT 1";

            MySqlCommand cmd = new MySqlCommand(sql, cnx);
            cmd.Parameters.Add(new MySqlParameter("@adherentId", donation.AdherentId));
            cmd.Parameters.Add(new MySqlParameter("@emballageId", donation.EmballageId));
            cmd.Parameters.Add(new MySqlParameter("@emplacementStockageId", donation.EmplacementDeStockageId));
            cmd.Parameters.Add(new MySqlParameter("@produitId", donation.ProduitId));
            cmd.Parameters.Add(new MySqlParameter("@uniteDeMesureId", donation.UniteDeMesureId));
            cmd.Parameters.Add(new MySqlParameter("@villeCpId", donation.VilleCpId));

            cmd.Parameters.Add(new MySqlParameter("@datePeremption", donation.DatePeremption));
            cmd.Parameters.Add(new MySqlParameter("@nomEtNDeVoie", donation.NomEtNDeVoie));
            cmd.Parameters.Add(new MySqlParameter("@quantiteProduit", donation.QuantiteProduit));

            //try
            //{

            int modified;
            cnx.Open();

            modified = (int)cmd.ExecuteScalar();
            cnx.Close();
            //}
            //catch (MySqlException exc)
            //{
            //    throw new Exception("Erreur lors de l'insertion d'un don. Details : "
            //                            + exc.Message);
            //}

            return modified;
        }

        public void Cancel(int id)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);

            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"UPDATE don 
                                SET Date_annulation_don = Sysdate() 
                                WHERE id = @id";

            cmd.Parameters.Add(new MySqlParameter("id", id));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();

            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de l'annulation d'un don : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }

        public List<DonationDetails> GetByIdAdherent(int id)
        {
            List<DonationDetails> result = new List<DonationDetails>();

            DbConnection cnx = new MySqlConnection(CNX_STR);

            DbCommand cmd = cnx.CreateCommand();
            PlageHoraireDAO plageHoraireDAO = new PlageHoraireDAO();

            cmd.CommandText = @"SELECT don.*, 
                                        produit.Nom 'Nom Produit',
                                        categorie.Nom 'Catégorie', 
                                        emplacement_de_stockage.Box_automatique 'Box', 
                                        temperature_stockage.Nom 'Temperature Stockage', 
                                        rayon.Nom 'Rayon',
                                        unite_de_mesure.nom 'Unité de mesure',
                                        (( SELECT don.Nom_et_n_de_voie WHERE don.Ville_CP_Id > 0)
                                        UNION (SELECT adherent.Nom_et_n_de_voie WHERE (don.Emplacement_de_stockage_Id = 0 OR don.Emplacement_de_stockage_Id IS NULL) AND don.Nom_et_n_de_voie IS NULL )
                                        UNION (SELECT emplacement_de_stockage.Nom_et_n_de_voie WHERE don.Emplacement_de_stockage_Id > 0 AND emplacement_de_stockage.Ville_CP_Id > 0 )
                                        UNION (SELECT partenaire.Nom_et_n_de_voie WHERE  emplacement_de_stockage.Ville_CP_Id = 0)) 'Nom et numéro de voie',
                                        villeDon.Code_postal 'CP don', 
                                        villeDon.Ville 'Ville don',
                                        produit.Categorie_Id 'CategorieId',
                                        categorie.Rayon_Id 'RayonId',
                                        adherent.Nom_utilisateur 'Nom Donateur',
                                        partenaire.Nom_Societe 'Nom Partenaire',
                                        don.Ville_Cp_Id 'Don VilleCpId',
                                        produit.picture

                                    FROM don
	                                    LEFT JOIN adherent ON don.Adherent_Id = adherent.Id
	                                    LEFT JOIN produit ON don.Produit_Id = produit.id
                                        LEFT JOIN categorie ON produit.Categorie_Id = categorie.Id
	                                    LEFT JOIN emplacement_de_stockage ON (don.Emplacement_de_stockage_Id = emplacement_de_stockage.Id)
                                        LEFT JOIN partenaire ON partenaire.Id = emplacement_de_stockage.Partenaire_Id
                                        LEFT JOIN ville_cp AS villeDon ON (emplacement_de_stockage.Ville_CP_Id=villeDon.Id) 
		                                    OR (don.Ville_CP_Id=villeDon.Id)
                                            OR ((adherent.Ville_CP_Id=villeDon.Id) AND (don.Ville_CP_Id =0 OR don.Ville_CP_Id IS NULL) AND (don.Emplacement_de_stockage_Id =0 OR don.Emplacement_de_stockage_Id IS NULL))
                                            OR ((partenaire.Ville_CP_Id=villeDon.Id) AND (emplacement_de_stockage.Ville_CP_Id = 0 OR emplacement_de_stockage.Ville_CP_Id IS NULL))
	                                    LEFT JOIN temperature_stockage ON produit.Temperature_Stockage_Id=temperature_stockage.Id
	                                    LEFT JOIN rayon ON categorie.Rayon_Id = rayon.Id
                                        LEFT JOIN unite_de_mesure ON unite_de_mesure.Id = don.Unite_de_mesure_Id


                                WHERE don.Adherent_Id = @Id AND (don.Date_annulation_don is null OR don.Date_annulation_don = 0) ";


            cmd.Parameters.Add(new MySqlParameter("Id", id));

            //try
            //{
            cnx.Open();
            DbDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                DonationDetails DonationD = new DonationDetails();


                DonationD.RayonId = dr.GetInt32(dr.GetOrdinal("RayonId"));
                DonationD.ProduitId = dr.GetInt32(dr.GetOrdinal("Produit_Id"));
                DonationD.CategorieId = dr.GetInt32(dr.GetOrdinal("CategorieId"));
                DonationD.NomEtNDeVoie = dr.GetString(dr.GetOrdinal("Nom et numéro de voie"));
                DonationD.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                DonationD.NomRayon = dr.GetString(dr.GetOrdinal("Rayon"));
                DonationD.NomProduit = dr.GetString(dr.GetOrdinal("Nom Produit"));
                DonationD.NomCategorie = dr.GetString(dr.GetOrdinal("Catégorie"));
                DonationD.UniteMesure = dr.GetString(dr.GetOrdinal("Unité de mesure"));
                DonationD.QuantiteProduit = dr.GetInt32(dr.GetOrdinal("Quantite_Produit"));
                DonationD.TemperatureStockage = dr.GetString(dr.GetOrdinal("Temperature Stockage"));
                DonationD.Ville = dr.GetString(dr.GetOrdinal("Ville don"));
                DonationD.CodePostal = dr.GetInt32(dr.GetOrdinal("CP don"));
                if (!dr.IsDBNull(dr.GetOrdinal("Box"))) DonationD.Box = dr.GetBoolean(dr.GetOrdinal("Box"));
                DonationD.Plages = plageHoraireDAO.GetAllByIdDon(dr.GetInt32(dr.GetOrdinal("Id")));
                DonationD.DatePeremption = dr.GetDateTime(dr.GetOrdinal("Date_peremption"));
                if (!dr.IsDBNull(dr.GetOrdinal("Date_de_depot"))) DonationD.DateDeDepot = dr.GetDateTime(dr.GetOrdinal("Date_de_depot"));
                if (!dr.IsDBNull(dr.GetOrdinal("Date_entree_proposition"))) DonationD.DateEntreeProposition = dr.GetDateTime(dr.GetOrdinal("Date_entree_proposition"));
                DonationD.NomDonateur = dr.GetString(dr.GetOrdinal("Nom Donateur"));
                DonationD.AdherentId = dr.GetInt32(dr.GetOrdinal("Adherent_Id"));
                if (!dr.IsDBNull(dr.GetOrdinal("Emplacement_de_stockage_Id"))) DonationD.EmplacementDeStockageId = dr.GetInt32(dr.GetOrdinal("Emplacement_de_stockage_Id"));
                if (!dr.IsDBNull(dr.GetOrdinal("Nom Partenaire"))) DonationD.NomPartenaire = dr.GetString(dr.GetOrdinal("Nom Partenaire"));
                if (!dr.IsDBNull(dr.GetOrdinal("Don VilleCpId"))) DonationD.VilleCpId = dr.GetInt32(dr.GetOrdinal("Don VilleCpId"));
                DonationD.picture = dr.GetString(dr.GetOrdinal("picture"));

                result.Add(DonationD);
            }


            //}
            //catch (MySqlException exc)
            //{
            //    throw new Exception("Erreur lors de la sélection des dons proposés: " + exc.Message);
            //}
            //finally
            //{
            cnx.Close();
            //}
            return result;
        }

        public DonationDetails GetById(int id)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DonationDetails DonationD = new DonationDetails();
            PlageHoraireDAO plageHoraireDAO = new PlageHoraireDAO();

            Donation Donation = new Donation();

            DbCommand cmd = cnx.CreateCommand();


            cmd.Parameters.Add(new MySqlParameter("id", id));


            cmd.CommandText = @"SELECT don.*, 
                                        produit.Nom 'Nom Produit',
                                        categorie.Nom 'Catégorie', 
                                        emplacement_de_stockage.Box_automatique 'Box', 
                                        temperature_stockage.Nom 'Temperature Stockage', 
                                        rayon.Nom 'Rayon',
                                        unite_de_mesure.nom 'Unité de mesure',
                                        (( SELECT don.Nom_et_n_de_voie WHERE don.Ville_CP_Id > 0)
                                        UNION (SELECT adherent.Nom_et_n_de_voie WHERE (don.Emplacement_de_stockage_Id = 0 OR don.Emplacement_de_stockage_Id IS NULL) AND don.Nom_et_n_de_voie IS NULL )
                                        UNION (SELECT emplacement_de_stockage.Nom_et_n_de_voie WHERE don.Emplacement_de_stockage_Id > 0 AND emplacement_de_stockage.Ville_CP_Id > 0 )
                                        UNION (SELECT partenaire.Nom_et_n_de_voie WHERE  emplacement_de_stockage.Ville_CP_Id = 0)) 'Nom et numéro de voie',
                                        villeDon.Code_postal 'CP don', 
                                        villeDon.Ville 'Ville don',
                                        produit.Categorie_Id 'CategorieId',
                                        categorie.Rayon_Id 'RayonId',
                                        adherent.Nom_utilisateur 'Nom Donateur',
                                        partenaire.Nom_Societe 'Nom Partenaire',
                                        produit.Temperature_Stockage_Id,
                                        produit.picture

                                    FROM don
	                                    LEFT JOIN adherent ON don.Adherent_Id = adherent.Id
	                                    LEFT JOIN produit ON don.Produit_Id = produit.id
                                        LEFT JOIN categorie ON produit.Categorie_Id = categorie.Id
	                                    LEFT JOIN emplacement_de_stockage ON (don.Emplacement_de_stockage_Id = emplacement_de_stockage.Id)
                                        LEFT JOIN partenaire ON partenaire.Id = emplacement_de_stockage.Partenaire_Id
                                        LEFT JOIN ville_cp AS villeDon ON (emplacement_de_stockage.Ville_CP_Id=villeDon.Id) 
		                                    OR (don.Ville_CP_Id=villeDon.Id)
                                            OR ((adherent.Ville_CP_Id=villeDon.Id) AND (don.Ville_CP_Id =0 OR don.Ville_CP_Id IS NULL) AND (don.Emplacement_de_stockage_Id =0 OR don.Emplacement_de_stockage_Id IS NULL))
                                            OR ((partenaire.Ville_CP_Id=villeDon.Id) AND (emplacement_de_stockage.Ville_CP_Id = 0 OR emplacement_de_stockage.Ville_CP_Id IS NULL))
	                                    LEFT JOIN temperature_stockage ON produit.Temperature_Stockage_Id=temperature_stockage.Id
	                                    LEFT JOIN rayon ON categorie.Rayon_Id = rayon.Id
                                        LEFT JOIN unite_de_mesure ON unite_de_mesure.Id = don.Unite_de_mesure_Id
                                        WHERE don.Id = @Id";
            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();


                if (dr.Read())
                {


                    DonationD.RayonId = dr.GetInt32(dr.GetOrdinal("RayonId"));
                    DonationD.ProduitId = dr.GetInt32(dr.GetOrdinal("Produit_Id"));
                    DonationD.CategorieId = dr.GetInt32(dr.GetOrdinal("CategorieId"));
                    DonationD.NomNumeroVoie = dr.GetString(dr.GetOrdinal("Nom et numéro de voie"));
                    DonationD.CodePostal = dr.GetInt32(dr.GetOrdinal("CP don"));
                    DonationD.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    DonationD.NomRayon = dr.GetString(dr.GetOrdinal("Rayon"));
                    DonationD.NomProduit = dr.GetString(dr.GetOrdinal("Nom Produit"));
                    DonationD.NomCategorie = dr.GetString(dr.GetOrdinal("Catégorie"));
                    DonationD.UniteMesure = dr.GetString(dr.GetOrdinal("Unité de mesure"));
                    DonationD.QuantiteProduit = dr.GetInt32(dr.GetOrdinal("Quantite_Produit"));
                    DonationD.TemperatureStockage = dr.GetString(dr.GetOrdinal("Temperature Stockage"));
                    DonationD.Ville = dr.GetString(dr.GetOrdinal("Ville don"));
                    DonationD.NomDonateur = dr.GetString(dr.GetOrdinal("Nom Donateur"));
                    if (!dr.IsDBNull(dr.GetOrdinal("Box"))) DonationD.Box = dr.GetBoolean(dr.GetOrdinal("Box"));
                    DonationD.Plages = plageHoraireDAO.GetAllByIdDon(dr.GetInt32(dr.GetOrdinal("Id")));
                    DonationD.DatePeremption = dr.GetDateTime(dr.GetOrdinal("Date_peremption"));
                    DonationD.AdherentId = dr.GetInt32(dr.GetOrdinal("Adherent_Id"));
                    if (!dr.IsDBNull(dr.GetOrdinal("Emplacement_de_stockage_Id"))) DonationD.EmplacementDeStockageId = dr.GetInt32(dr.GetOrdinal("Emplacement_de_stockage_Id"));
                    if (!dr.IsDBNull(dr.GetOrdinal("Nom Partenaire"))) DonationD.NomPartenaire = dr.GetString(dr.GetOrdinal("Nom Partenaire"));
                    DonationD.TemperatureStockageId = dr.GetInt32(dr.GetOrdinal("Temperature_Stockage_Id"));
                    DonationD.EmballageId = dr.GetInt32(dr.GetOrdinal("Emballage_Id"));
                    DonationD.UniteDeMesureId = dr.GetInt32(dr.GetOrdinal("Unite_de_mesure_Id"));
                    DonationD.picture = dr.GetString(dr.GetOrdinal("picture"));

                }
                else
                {

                    DonationD.NomProduit = "erreur";
                    return DonationD;
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return DonationD;
        }

        public List<DonationDetails> GetReservedById(int Id)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            List<DonationDetails> result = new List<DonationDetails>();
            DbCommand cmd = cnx.CreateCommand();

            cmd.Parameters.Add(new MySqlParameter("Id", Id));

            cmd.CommandText = @"SELECT Don_Id FROM echange
                                WHERE Adherent_Id = @Id AND
                                Date_annulation_echange IS NULL AND 
                                Date_de_retrait_du_produit IS NULL AND 
                                Date_recuperation_produit IS NULL
                                ";
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    DonationDetails DonationD = GetById(dr.GetInt32(dr.GetOrdinal("Don_Id")));
                    result.Add(DonationD);
                }
                cnx.Close();
                return result;
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de la recherche du don. Details : "
                                        + exc.Message);

            }


        }

        public void ReserveById(int IdDon, int IdAdherent)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.Parameters.Add(new MySqlParameter("IdDon", IdDon));
            cmd.Parameters.Add(new MySqlParameter("IdAdherent", IdAdherent));

            cmd.CommandText = @"INSERT INTO echange
                                (Adherent_Id, Date_reservation,Don_Id) 
                                VALUES 
                                (@IdAdherent, SYSDATE(),@IdDon)";

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
                cnx.Close();
                ChangereservableStatus(IdDon);
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de la reservation. Details : "
                                        + exc.Message);
            }

        }

        public void ChangereservableStatus(int IdDon)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.Parameters.Add(new MySqlParameter("IdDon", IdDon));


            cmd.CommandText = @"UPDATE  don
                                SET     don.reservable = (CASE WHEN don.reservable = 1 THEN 0 ELSE 1 END)
                                WHERE   don.Id = @IdDon";

            try
            {

                cnx.Open();
                cmd.ExecuteNonQuery();
                cnx.Close();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors du changement de statut du don Details : "
                                        + exc.Message);
            }
        }


        public void DepotDon(int IdDon)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.Parameters.Add(new MySqlParameter("IdDon", IdDon));


            cmd.CommandText = @"UPDATE  don
                                SET     don.Date_de_depot = SYSDATE()
                                WHERE   don.Id = @IdDon";

            try
            {

                cnx.Open();
                cmd.ExecuteNonQuery();
                cnx.Close();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors du changement de statut du don Details : "
                                        + exc.Message);
            }
        }

        public void CancelReservation(int IdDon)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.Parameters.Add(new MySqlParameter("IdDon", IdDon));


            cmd.CommandText = @"UPDATE  echange
                                SET     echange.Date_annulation_echange = SYSDATE()
                                WHERE   echange.Don_Id = @IdDon AND
                                        echange.Date_annulation_echange IS NULL";
            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
                cnx.Close();
                ChangereservableStatus(IdDon);
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors du changement de statut du don Details : "
                                        + exc.Message);
            }


        }

        public void ConfirmerReception(int IdDon)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.Parameters.Add(new MySqlParameter("IdDon", IdDon));


            cmd.CommandText = @"UPDATE  echange
                                SET     echange.Date_recuperation_produit = SYSDATE()
                                WHERE   echange.Don_Id = @IdDon AND
                                        echange.Date_annulation_echange IS NULL";
            try
            {

                cnx.Open();
                cmd.ExecuteNonQuery();
                cnx.Close();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de la confirmation : "
                                        + exc.Message);
            }


        }

        public bool IsReserved(int IdDon)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.Parameters.Add(new MySqlParameter("IdDon", IdDon));
            cmd.CommandText = @"SELECT echange
                                        WHERE   echange.Don_Id = @IdDon AND
                                        echange.Date_reservation IS NOT NULL";
            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
                DbDataReader dr = cmd.ExecuteReader();
                if (dr.Read()) return true;
                else return false;
                cnx.Close();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors du changement de statut du don Details : "
                                        + exc.Message);
            }


        }


        public List<DonationDetails> GetByIdPartner(int id)
        {
            List<DonationDetails> result = new List<DonationDetails>();

            DbConnection cnx = new MySqlConnection(CNX_STR);

            DbCommand cmd = cnx.CreateCommand();
            PlageHoraireDAO plageHoraireDAO = new PlageHoraireDAO();

            cmd.CommandText = @"SELECT don.*, 
                                        produit.Nom 'Nom Produit',
                                        categorie.Nom 'Catégorie', 
                                        emplacement_de_stockage.Box_automatique 'Box', 
                                        temperature_stockage.Nom 'Temperature Stockage', 
                                        rayon.Nom 'Rayon',
                                        unite_de_mesure.nom 'Unité de mesure',
                                        (( SELECT don.Nom_et_n_de_voie WHERE don.Ville_CP_Id > 0)
                                        UNION (SELECT adherent.Nom_et_n_de_voie WHERE (don.Emplacement_de_stockage_Id = 0 OR don.Emplacement_de_stockage_Id IS NULL) AND don.Nom_et_n_de_voie IS NULL )
                                        UNION (SELECT emplacement_de_stockage.Nom_et_n_de_voie WHERE don.Emplacement_de_stockage_Id > 0 AND emplacement_de_stockage.Ville_CP_Id > 0 )
                                        UNION (SELECT partenaire.Nom_et_n_de_voie WHERE  emplacement_de_stockage.Ville_CP_Id = 0)) 'Nom et numéro de voie',
                                        villeDon.Code_postal 'CP don', 
                                        villeDon.Ville 'Ville don',
                                        produit.Categorie_Id 'CategorieId',
                                        categorie.Rayon_Id 'RayonId',
                                        adherent.Nom_utilisateur 'Nom Donateur',
                                        partenaire.Nom_Societe 'Nom Partenaire',
                                        don.Ville_Cp_Id 'Don VilleCpId',
                                        produit.picture,
                                        adherent.prenom,
                                        adherent.nom

                                    FROM don
	                                    LEFT JOIN adherent ON don.Adherent_Id = adherent.Id
	                                    LEFT JOIN produit ON don.Produit_Id = produit.id
                                        LEFT JOIN categorie ON produit.Categorie_Id = categorie.Id
	                                    LEFT JOIN emplacement_de_stockage ON (don.Emplacement_de_stockage_Id = emplacement_de_stockage.Id)
                                        LEFT JOIN partenaire ON partenaire.Id = emplacement_de_stockage.Partenaire_Id
                                        LEFT JOIN ville_cp AS villeDon ON (emplacement_de_stockage.Ville_CP_Id=villeDon.Id) 
		                                    OR (don.Ville_CP_Id=villeDon.Id)
                                            OR ((adherent.Ville_CP_Id=villeDon.Id) AND (don.Ville_CP_Id =0 OR don.Ville_CP_Id IS NULL) AND (don.Emplacement_de_stockage_Id =0 OR don.Emplacement_de_stockage_Id IS NULL))
                                            OR ((partenaire.Ville_CP_Id=villeDon.Id) AND (emplacement_de_stockage.Ville_CP_Id = 0 OR emplacement_de_stockage.Ville_CP_Id IS NULL))
	                                    LEFT JOIN temperature_stockage ON produit.Temperature_Stockage_Id=temperature_stockage.Id
	                                    LEFT JOIN rayon ON categorie.Rayon_Id = rayon.Id
                                        LEFT JOIN unite_de_mesure ON unite_de_mesure.Id = don.Unite_de_mesure_Id


                                WHERE partenaire.Id = @Id AND (don.Date_annulation_don is null OR don.Date_annulation_don = 0) ";


            cmd.Parameters.Add(new MySqlParameter("Id", id));

            //try
            //{
            cnx.Open();
            DbDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                DonationDetails DonationD = new DonationDetails();


                DonationD.RayonId = dr.GetInt32(dr.GetOrdinal("RayonId"));
                DonationD.ProduitId = dr.GetInt32(dr.GetOrdinal("Produit_Id"));
                DonationD.CategorieId = dr.GetInt32(dr.GetOrdinal("CategorieId"));
                DonationD.NomEtNDeVoie = dr.GetString(dr.GetOrdinal("Nom et numéro de voie"));
                DonationD.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                DonationD.NomRayon = dr.GetString(dr.GetOrdinal("Rayon"));
                DonationD.NomProduit = dr.GetString(dr.GetOrdinal("Nom Produit"));
                DonationD.NomCategorie = dr.GetString(dr.GetOrdinal("Catégorie"));
                DonationD.UniteMesure = dr.GetString(dr.GetOrdinal("Unité de mesure"));
                DonationD.QuantiteProduit = dr.GetInt32(dr.GetOrdinal("Quantite_Produit"));
                DonationD.TemperatureStockage = dr.GetString(dr.GetOrdinal("Temperature Stockage"));
                DonationD.Ville = dr.GetString(dr.GetOrdinal("Ville don"));
                DonationD.CodePostal = dr.GetInt32(dr.GetOrdinal("CP don"));
                if (!dr.IsDBNull(dr.GetOrdinal("Box"))) DonationD.Box = dr.GetBoolean(dr.GetOrdinal("Box"));
                DonationD.Plages = plageHoraireDAO.GetAllByIdDon(dr.GetInt32(dr.GetOrdinal("Id")));
                DonationD.DatePeremption = dr.GetDateTime(dr.GetOrdinal("Date_peremption"));
                if (!dr.IsDBNull(dr.GetOrdinal("Date_de_depot"))) DonationD.DateDeDepot = dr.GetDateTime(dr.GetOrdinal("Date_de_depot"));
                if (!dr.IsDBNull(dr.GetOrdinal("Date_entree_proposition"))) DonationD.DateEntreeProposition = dr.GetDateTime(dr.GetOrdinal("Date_entree_proposition"));
                DonationD.NomDonateur = dr.GetString(dr.GetOrdinal("Nom Donateur"));
                DonationD.AdherentId = dr.GetInt32(dr.GetOrdinal("Adherent_Id"));
                if (!dr.IsDBNull(dr.GetOrdinal("Emplacement_de_stockage_Id"))) DonationD.EmplacementDeStockageId = dr.GetInt32(dr.GetOrdinal("Emplacement_de_stockage_Id"));
                if (!dr.IsDBNull(dr.GetOrdinal("Nom Partenaire"))) DonationD.NomPartenaire = dr.GetString(dr.GetOrdinal("Nom Partenaire"));
                if (!dr.IsDBNull(dr.GetOrdinal("Don VilleCpId"))) DonationD.VilleCpId = dr.GetInt32(dr.GetOrdinal("Don VilleCpId"));
                DonationD.picture = dr.GetString(dr.GetOrdinal("picture"));
                DonationD.NomDonateur = dr.GetString(dr.GetOrdinal("prenom"));
                DonationD.PrenomDonateur = dr.GetString(dr.GetOrdinal("nom"));

                result.Add(DonationD);
            }
            cnx.Close();
            //}
            return result;
        }
    }
    }