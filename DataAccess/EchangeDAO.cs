﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.Entities;
using System.Data.Common;
using MySql.Data.MySqlClient;

namespace Fr.EQL.AI110.DLD_GB.DataAccess
{
    public class EchangeDAO : DAO
    {
        public List<Echange> getAll()
        {
            List<Echange> result = new List<Echange>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT * from echange";

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Echange echange = new Echange();

                    echange.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    echange.DonId = dr.GetInt32(dr.GetOrdinal("Don_Id"));
                    result.Add(echange);
                }


            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de l'annulation d'un don : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }



    }
}
