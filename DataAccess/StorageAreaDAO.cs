﻿using Fr.EQL.AI110.DLD_GB.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GB.DataAccess
{
	public class StorageAreaDAO : DAO
	{
		public void Update (StorageArea storageArea)
		{
			DbConnection cnx = new MySqlConnection(CNX_STR);
			DbCommand cmd = cnx.CreateCommand();

			cmd.CommandText = @"UPDATE emplacement_de_stockage
                                SET Taille_Id = @tailleId, Temperature_Stockage_Id = @temperatureStockageId, Ville_CP_Id = @villeCpId, 
									Nom_et_n_de_voie = @adresse, box_automatique = @box_automatique, disponible = @dispo
                                WHERE id = @id";

			cmd.Parameters.Add(new MySqlParameter("tailleId", storageArea.TailleId));
			cmd.Parameters.Add(new MySqlParameter("temperatureStockageId", storageArea.TemperatureStockageId));
			cmd.Parameters.Add(new MySqlParameter("villeCpId", storageArea.VilleCpId));
			cmd.Parameters.Add(new MySqlParameter("adresse", storageArea.NomEtNDeVoie));
			cmd.Parameters.Add(new MySqlParameter("box_automatique", storageArea.BoxAutomatique));
			cmd.Parameters.Add(new MySqlParameter("dispo", storageArea.Disponible));
			cmd.Parameters.Add(new MySqlParameter("id", storageArea.Id));

			try
			{
				cnx.Open();
				cmd.ExecuteNonQuery();

			}
			catch (MySqlException exc)
			{
				throw new Exception("Erreur lors de l'annulation d'un don : " + exc.Message);
			}
			finally
			{
				cnx.Close();
			}

		}

		public StorageArea GetById(int id)
		{
			StorageArea result = new StorageArea();
			DbConnection cnx = new MySqlConnection(CNX_STR);
			DbCommand cmd = cnx.CreateCommand();

			cmd.CommandText = @"SELECT *
								FROM emplacement_de_stockage eds
								WHERE Id = @id;
								";

			cmd.Parameters.Add(new MySqlParameter("id", id));

			try
			{
				cnx.Open();
				DbDataReader dr = cmd.ExecuteReader();

				while (dr.Read())
				{
                    
					result.Id = dr.GetInt32(dr.GetOrdinal("id"));
					result.TailleId = dr.GetInt32(dr.GetOrdinal("taille_id"));
					result.TemperatureStockageId = dr.GetInt32(dr.GetOrdinal("temperature_stockage_id"));
                    result.VilleCpId = dr.GetInt32(dr.GetOrdinal("ville_cp_id"));
                    result.PartenaireId = dr.GetInt32(dr.GetOrdinal("partenaire_id"));
                    result.NomEtNDeVoie = dr.GetString(dr.GetOrdinal("nom_et_n_de_voie"));
                    if (!dr.IsDBNull(dr.GetOrdinal("box_automatique"))){
                        result.BoxAutomatique = dr.GetBoolean(dr.GetOrdinal("box_automatique"));
                    }
                    if (!dr.IsDBNull(dr.GetOrdinal("disponible")))
                    {
                        result.Disponible = dr.GetBoolean(dr.GetOrdinal("disponible"));
                    }
				}

            }
			catch (MySqlException exc)
			{
				throw new Exception("Erreur lors de la sélection des dons proposés: " + exc.Message);
			}
			finally
			{
				cnx.Close();
			}
			return result;


		}

		public List<StorageArea> GetStorageAreaDispoByMulticritera(int partnerId, int temperatureStockageId)
        {
			List<StorageArea> result = new List<StorageArea>();
			DbConnection cnx = new MySqlConnection(CNX_STR);
			DbCommand cmd = cnx.CreateCommand();

			cmd.CommandText = @"SELECT *
								FROM emplacement_de_stockage 
								WHERE Partenaire_Id = @partnerId 
								AND (Temperature_Stockage_Id = @temperatureStockageId)
								AND (disponible = @disponibilite)
								";

			cmd.Parameters.Add(new MySqlParameter("partnerId", partnerId));
			cmd.Parameters.Add(new MySqlParameter("disponibilite", 1));
			cmd.Parameters.Add(new MySqlParameter("temperatureStockageId", temperatureStockageId));

			try
			{
				cnx.Open();
				DbDataReader dr = cmd.ExecuteReader();

				while (dr.Read())
				{
					StorageArea storageArea = new StorageArea();

					storageArea.Id = dr.GetInt32(dr.GetOrdinal("Id"));

					result.Add(storageArea);
				}


			}
			catch (MySqlException exc)
			{
				throw new Exception("Erreur lors de la sélection des dons proposés: " + exc.Message);
			}
			finally
			{
				cnx.Close();
			}
			return result;

		}

        public void Insert(StorageArea storageArea)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);

            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"INSERT INTO emplacement_de_stockage
                                        (Taille_Id, Temperature_Stockage_Id, 
                                        Ville_CP_Id, Partenaire_Id, 
                                        Nom_et_n_de_voie, Date_ajout_stockage, 
                                        Box_automatique, Disponible)
                                VALUES (@TailleId, @TemperatureStockageId, 
                                        @VilleCpId, @PartenaireId, @NomEtNDeVoie, 
                                        sysdate(), @BoxAutomatique, @Disponible)";

            cmd.Parameters.Add(new MySqlParameter
                ("TailleId", storageArea.TailleId));
            cmd.Parameters.Add(new MySqlParameter
                ("TemperatureStockageId", storageArea.TemperatureStockageId));
            cmd.Parameters.Add(new MySqlParameter
                ("VilleCpId", storageArea.VilleCpId));
            cmd.Parameters.Add(new MySqlParameter
                ("PartenaireId", storageArea.PartenaireId));
            cmd.Parameters.Add(new MySqlParameter
                ("NomEtNDeVoie", storageArea.NomEtNDeVoie));
            cmd.Parameters.Add(new MySqlParameter
                ("DateAjoutStockage", storageArea.DateAjoutStockage));
            cmd.Parameters.Add(new MySqlParameter
                ("BoxAutomatique", storageArea.BoxAutomatique));
            cmd.Parameters.Add(new MySqlParameter
                ("Disponible", storageArea.Disponible));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cnx.Close();
            }
        }

        public void Cancel(int id)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);

            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"UPDATE emplacement_de_stockage 
                                SET Date_retrait_Stockage = Sysdate() 
                                WHERE id = @id";

            cmd.Parameters.Add(new MySqlParameter("id", id));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();

            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors du retrait " +
                    "de l'emplacement de stockage : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }

        public List<StorageAreaDetails> GetAllWithDetailsByIdPartner(int id)
        {
            List<StorageAreaDetails> result = new List<StorageAreaDetails>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT eds.Id 'id', taille.nom_taille 'taille', 
                                        temperature_stockage.Nom 'temperature', partenaire.Id 'partenaire_id',
                                        partenaire.Nom_Societe 'partenaire', ville_cp.Ville 'ville',
                               CASE WHEN eds.Nom_et_n_de_voie IS NULL THEN partenaire.Nom_et_n_de_voie
									WHEN eds.Nom_et_n_de_voie IS NOT NULL THEN eds.Nom_et_n_de_voie
                                     END 'adresse',
                                        eds.Box_automatique 'box automatique', eds.Disponible 'disponible'
                                FROM emplacement_de_stockage eds
                                    LEFT JOIN temperature_stockage 
                                           ON eds.Temperature_Stockage_Id = temperature_stockage.Id
                                    LEFT JOIN taille ON eds.Taille_Id = taille.Id
                                    LEFT JOIN partenaire ON eds.Partenaire_Id = partenaire.Id
                                    LEFT JOIN ville_cp ON (eds.Ville_CP_Id = ville_cp.Id) 
													   OR ((partenaire.Ville_CP_Id = ville_cp.Id)
                                                       AND (eds.Ville_CP_Id = 0))
                                WHERE eds.Partenaire_Id = @PartenaireId AND Date_retrait_Stockage IS NULL";

            cmd.Parameters.Add(new MySqlParameter("PartenaireId", id));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                { 
                    StorageAreaDetails sAD = new StorageAreaDetails();

                    sAD.TailleNom = dr.GetString(dr.GetOrdinal("taille"));
                    sAD.TemperatureStockageNom = dr.GetString(dr.GetOrdinal("temperature"));
                    sAD.PartenaireId = dr.GetInt32(dr.GetOrdinal("partenaire_id"));
                    sAD.PartenaireNomSociete = dr.GetString(dr.GetOrdinal("partenaire"));
                    sAD.VilleCPNom = dr.GetString(dr.GetOrdinal("ville"));
                    sAD.Id = dr.GetInt32(dr.GetOrdinal("id"));
                    sAD.NomEtNDeVoie = dr.GetString(dr.GetOrdinal("adresse"));
                    if (!dr.IsDBNull(dr.GetOrdinal("box automatique")))
                    {
                        sAD.BoxAutomatique = dr.GetBoolean(dr.GetOrdinal("box automatique"));
                    }
                    if (!dr.IsDBNull(dr.GetOrdinal("disponible")))
                    {
                        sAD.Disponible = dr.GetBoolean(dr.GetOrdinal("disponible"));
                    }

                    result.Add(sAD);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de la récupération " +
                    "des détails d'un espace de stockage : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }
    }
}
