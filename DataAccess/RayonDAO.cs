﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.Entities;
using System.Data.Common;
using MySql.Data.MySqlClient;

namespace Fr.EQL.AI110.DLD_GB.DataAccess
{
    public class RayonDAO : DAO
    {
        public List<Rayon> GetAll()
        {

            List<Rayon> result = new List<Rayon>();
            DbConnection cnx = new MySqlConnection(CNX_STR);

            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = "SELECT * FROM Rayon";

            try
            {
                cnx.Open();


                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Rayon Rayon = new Rayon();

                    Rayon.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    Rayon.Nom = dr.GetString(dr.GetOrdinal("Nom"));

                    result.Add(Rayon);
                }

                cnx.Close();

                return result;
            }
            catch (MySqlException e)
            {
                throw new Exception("Erreur lors de l'insertion d'un Rayone. Details : "
               + e.Message);
            }

        }

        public Rayon GetByIdRayon(int id)
        {
            Rayon result = new Rayon();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = "SELECT * FROM Rayon WHERE id=@id";

            cmd.Parameters.Add(new MySqlParameter("Id", id));

            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Rayon Rayon = new Rayon();
                    result.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    result.Nom = dr.GetString(dr.GetOrdinal("Nom"));
                }

                cnx.Close();

                return result;
            }
            catch (MySqlException e)
            {
                throw new Exception("Erreur lors de l'insertion d'un Rayone. Details : "
               + e.Message);
            }

        }
        public Rayon GetById(int id)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);

            Rayon Rayon = new Rayon();

            DbCommand cmd = cnx.CreateCommand();


            cmd.Parameters.Add(new MySqlParameter("id", id));


            cmd.CommandText = "SELECT * FROM Rayon WHERE Id = @Id";
            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    Rayon.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    Rayon.Nom = dr.GetString(dr.GetOrdinal("Nom"));
                }
                else
                {
                    Rayon.Nom = "erreur";
                    return Rayon;
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return Rayon;
        }
    }
}
