﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.Entities;
using System.Data.Common;
using MySql.Data.MySqlClient;

namespace Fr.EQL.AI110.DLD_GB.DataAccess
{
    public class TemperatureStockageDAO : DAO
    {
        public List<TemperatureStockage> GetAll()
        {

            List<TemperatureStockage> result = new List<TemperatureStockage>();
            DbConnection cnx = new MySqlConnection(CNX_STR);

            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = "SELECT * FROM temperature_stockage";

            try
            {
                cnx.Open();


                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    TemperatureStockage TemperatureStockage = new TemperatureStockage();

                    TemperatureStockage.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    TemperatureStockage.Nom = dr.GetString(dr.GetOrdinal("Nom"));

                    result.Add(TemperatureStockage);
                }

                cnx.Close();

                return result;
            }
            catch (MySqlException e)
            {
                throw new Exception("Erreur lors de l'insertion d'un TemperatureStockagee. Details : "
               + e.Message);
            }

        }

    }
}
