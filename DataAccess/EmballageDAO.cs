﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.Entities;
using MySql.Data.MySqlClient;

namespace Fr.EQL.AI110.DLD_GB.DataAccess
{
    public class EmballageDAO : DAO
    {
        public List<Emballage> GetAll()
        {

            List<Emballage> result = new List<Emballage>();
            DbConnection cnx = new MySqlConnection(CNX_STR);

            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = "SELECT * FROM Emballage";

            try
            {
                cnx.Open();


                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Emballage Emballage = new Emballage();

                    Emballage.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    Emballage.Nom = dr.GetString(dr.GetOrdinal("Nom"));
                    
                    result.Add(Emballage);
                }

                cnx.Close();

                return result;
            }
            catch (MySqlException e)
            {
                throw new Exception("Erreur lors de l'insertion d'un Emballagee. Details : "
               + e.Message);
            }

        }

    }
}
