﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.Entities;
using System.Data.Common;
using MySql.Data.MySqlClient;

namespace Fr.EQL.AI110.DLD_GB.DataAccess
{
    public class CategorieDAO : DAO
    {
        public List<Categorie> GetAll()
        {

            List<Categorie> result = new List<Categorie>();
            DbConnection cnx = new MySqlConnection(CNX_STR);

            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = "SELECT * FROM Categorie";

            try
            {
                cnx.Open();


                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Categorie Categorie = new Categorie();

                    Categorie.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    Categorie.Nom = dr.GetString(dr.GetOrdinal("Nom"));
                    Categorie.RayonId = dr.GetInt32(dr.GetOrdinal("Rayon_Id"));

                    result.Add(Categorie);
                }

                cnx.Close();

                return result;
            }
            catch (MySqlException e)
            {
                throw new Exception("Erreur lors de l'insertion d'un Categoriee. Details : "
               + e.Message);
            }

        }

        public List<Categorie> GetAllByIdRayon(int id)
        {
            List<Categorie> result = new List<Categorie>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT Categorie.Id, Categorie.Rayon_Id, Categorie.Nom
                                FROM Categorie 
                                WHERE rayon_id= @id";

            cmd.Parameters.Add(new MySqlParameter("id", id));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Categorie Categorie = new Categorie();

                    Categorie.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    Categorie.Nom = dr.GetString(dr.GetOrdinal("Nom"));
                    Categorie.RayonId = dr.GetInt32(dr.GetOrdinal("Rayon_Id"));

                    result.Add(Categorie);
                }

                return result;
            }
            catch (MySqlException e)
            {
                throw new Exception("Erreur lors de l'insertion d'un Categoriee. Details : "
               + e.Message);
            }
            finally
            {

                cnx.Close();
            }
        }

        public Categorie GetByIdCategorie(int id)
        {
            Categorie result = new Categorie();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = "SELECT * FROM Categorie WHERE id=@id";

            cmd.Parameters.Add(new MySqlParameter("Id", id));

            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Rayon Rayon = new Rayon();
                    result.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    result.Nom = dr.GetString(dr.GetOrdinal("Nom"));
                    result.RayonId = dr.GetInt32(dr.GetOrdinal("Rayon_Id"));
                }

                cnx.Close();

                return result;
            }
            catch (MySqlException e)
            {
                throw new Exception("Erreur lors de l'insertion d'un Rayone. Details : "
               + e.Message);
            }

        }
        public List<Categorie> GetByRayonId(int rayonId)
        {

            List<Categorie> result = new List<Categorie>();
            DbConnection cnx = new MySqlConnection(CNX_STR);

            DbCommand cmd = cnx.CreateCommand();

            cmd.Parameters.Add(new MySqlParameter("rayonId", rayonId));

            cmd.CommandText = "SELECT * FROM Categorie WHERE Rayon_Id = @rayonId ";

            try
            {
                cnx.Open();


                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Categorie Categorie = new Categorie();

                    Categorie.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    Categorie.Nom = dr.GetString(dr.GetOrdinal("Nom"));
                    Categorie.RayonId = dr.GetInt32(dr.GetOrdinal("Rayon_Id"));

                    result.Add(Categorie);
                }

                cnx.Close();

                return result;
            }
            catch (MySqlException e)
            {
                throw new Exception("Erreur lors de l'insertion d'un Categoriee. Details : "
               + e.Message);
            }

        }

        public Categorie GetById(int id)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);

            Categorie categorie = new Categorie();

            DbCommand cmd = cnx.CreateCommand();


            cmd.Parameters.Add(new MySqlParameter("id", id));


            cmd.CommandText = "SELECT * FROM Categorie WHERE Id = @Id";
            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    categorie.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    categorie.Nom = dr.GetString(dr.GetOrdinal("Nom"));
                    categorie.RayonId = dr.GetInt32(dr.GetOrdinal("Rayon_Id"));

                }
                else
                {
                    categorie.Nom = "erreur";
                    return categorie;
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return categorie;
        }

        public Rayon GetRayon(int idCategorie)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);

            Rayon rayon = new Rayon();

            DbCommand cmd = cnx.CreateCommand();


            cmd.Parameters.Add(new MySqlParameter("id", idCategorie));


            cmd.CommandText = "SELECT Categorie.Rayon_Id, Rayon.Nom FROM categorie LEFT JOIN  Rayon ON Categorie.Rayon_Id = Rayon.Id WHERE Categorie.Id = @id";

            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    rayon.Id = dr.GetInt32(dr.GetOrdinal("Rayon_Id"));
                    rayon.Nom = dr.GetString(dr.GetOrdinal("Nom"));

                }
                else
                {
                    rayon.Nom = "erreur";
                    return rayon;
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return rayon;
        }
    }

}
