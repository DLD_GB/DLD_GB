﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.Entities;
using MySql.Data.MySqlClient;

namespace Fr.EQL.AI110.DLD_GB.DataAccess
{
    public class AdherentDAO : DAO
    {
       public List<Adherent> getAll()
        {
            List<Adherent> result = new List<Adherent>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT * from adherent";

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Adherent adherent = new Adherent();

                    adherent.Nom = dr.GetString(dr.GetOrdinal("Nom"));
                    adherent.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    adherent.Prenom = dr.GetString(dr.GetOrdinal("Prenom"));
                    adherent.NomUtilisateur = dr.GetString(dr.GetOrdinal("Nom_utilisateur"));
                    result.Add(adherent);
                }


            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de l'annulation d'un don : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }
        public Adherent GetById(int id)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);

            Adherent adherent = new Adherent();

            DbCommand cmd = cnx.CreateCommand();


            cmd.Parameters.Add(new MySqlParameter("id", id));


            cmd.CommandText = "SELECT * FROM adherent WHERE Id = @Id";
            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    adherent.Nom = dr.GetString(dr.GetOrdinal("Nom"));
                    adherent.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    adherent.Prenom = dr.GetString(dr.GetOrdinal("Prenom"));
                    adherent.NomUtilisateur = dr.GetString(dr.GetOrdinal("Nom_utilisateur"));

                }
                else
                {
                    adherent.Nom = "erreur";
                    return adherent;
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return adherent;
        }

        public Adherent getByAdherentId(int id)
        {
            Adherent result = new Adherent();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT * from adherent WHERE adherent.Id = @id";

            cmd.Parameters.Add(new MySqlParameter("id", id));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    result.Nom = dr.GetString(dr.GetOrdinal("Nom"));
                    result.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    result.Prenom = dr.GetString(dr.GetOrdinal("Prenom"));
                    result.VilleCpId = dr.GetInt32(dr.GetOrdinal("Ville_CP_Id"));
                 
                }


            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de l'annulation d'un don : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        public List<Adherent> GetByMulticriteria(string nom, string prenom)
        {
            List<Adherent> result = new List<Adherent>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT *
                                FROM adherent
                                WHERE nom LIKE @nom
                                    AND prenom  LIKE @prenom";

            nom = "%" + nom + "%";
            prenom = "%" + prenom + "%";
            cmd.Parameters.Add(new MySqlParameter("nom", nom));
            cmd.Parameters.Add(new MySqlParameter("prenom", prenom));

            try
            {
                cnx.Open();


                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Adherent adherent = new Adherent();

                    adherent.Id = dr.GetInt32(dr.GetOrdinal("Id"));
                    adherent.Nom = dr.GetString(dr.GetOrdinal("Nom"));
                    adherent.Prenom = dr.GetString(dr.GetOrdinal("Prenom"));

                    result.Add(adherent);
                }
                return result;
            }
            catch (MySqlException e)
            {
                throw new Exception("Erreur lors de la récupération des partenaires : " + e.Message);
            }
            finally
            {
                cnx.Close();
            }
        }

    }
}
