﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Fr.EQL.AI110.DLD_GB.Entities
{
    public class PlageHoraire
    {

        public int AdherentId { get; set; }
        public DateTime DebutPlage1 { get; set; }
        public DateTime DebutPlage2 { get; set; }
        public int DonId { get; set; }
        [Required(ErrorMessage = "Le champ Emplacement de stockage_Id est obligatoire")] public int EmplacementDeStockageId { get; set; }
        public DateTime FinPlage1 { get; set; }
        public DateTime FinPlage2 { get; set; }
        public int Id { get; set; }
        [Required(ErrorMessage = "Le champ Jours_Id est obligatoire")] public int JoursId { get; set; }
        public int PartenaireId { get; set; }
        public string NomJour { get; set; }

        public PlageHoraire(int adherentId, DateTime debutPlage1, DateTime debutPlage2, int donId, int emplacementDeStockageId, DateTime finPlage1, DateTime finPlage2, int id, int joursId, int partenaireId)
        {
            AdherentId = adherentId;
            DebutPlage1 = debutPlage1;
            DebutPlage2 = debutPlage2;
            DonId = donId;
            EmplacementDeStockageId = emplacementDeStockageId;
            FinPlage1 = finPlage1;
            FinPlage2 = finPlage2;
            Id = id;
            JoursId = joursId;
            PartenaireId = partenaireId;
        }

        public PlageHoraire()
        {
        }
    }
}
