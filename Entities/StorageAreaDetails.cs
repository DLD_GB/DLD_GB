﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GB.Entities
{
    public class StorageAreaDetails : StorageArea
    {
        public string TailleNom { get; set; }
        public string TemperatureStockageNom { get; set; }
        public string VilleCPNom { get; set; }
        public string VilleCPCp { get; set; }
        public string PartenaireNomSociete { get; set; }
        
    }
}