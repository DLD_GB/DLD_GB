﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GB.Entities
{
    public class Evaluation
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Le champ Qualité_Échange_Id est obligatoire")] public int QualiteEchangeId { get; set; }
        [Required(ErrorMessage = "Le champ Qualité_Produit_Id est obligatoire")] public int QualiteProduitId { get; set; }
        [Required(ErrorMessage = "Le champ Échange_Id est obligatoire")] public int EchangeId { get; set; }
        [Required(ErrorMessage = "Le champ Note_Évaluation est obligatoire")] public int NoteEvaluation { get; set; }
        public string CommentaireEvaluation { get; set; }
        [Required(ErrorMessage = "Le champ Date_Évaluation est obligatoire")] public DateTime DateEvaluation { get; set; }
        public Evaluation(int id, int qualiteEchangeId, int qualiteProduitId, int echangeId, int noteEvaluation, string commentaireEvaluation, DateTime dateEvaluation)
        {
            Id = id;
            QualiteEchangeId = qualiteEchangeId;
            QualiteProduitId = qualiteProduitId;
            EchangeId = echangeId;
            NoteEvaluation = noteEvaluation;
            CommentaireEvaluation = commentaireEvaluation;
            DateEvaluation = dateEvaluation;
        }

        public Evaluation()
        {
        }
    }
}
