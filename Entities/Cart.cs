﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Fr.EQL.AI110.DLD_GB.Entities
{
    public class Cart
    {
        [Required(ErrorMessage = "Le champ Adhérent_Id est obligatoire")] public int AdherentId { get; set; }
        [Required(ErrorMessage = "Le champ Don_Id est obligatoire")] public int DonId { get; set; }
        public int Id { get; set; }
        public string Picture { get; set; }



        public Cart()
        {
        }
    }
}
