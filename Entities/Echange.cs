﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Fr.EQL.AI110.DLD_GB.Entities
{
    public class Echange
    {
        public int Id { get; set; }
        public int DonId { get; set; }

        public Echange(int id, int donId)
        {
            Id = id;
            DonId = donId;
        }

        public Echange()
        {
        }

    }
}
