﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Fr.EQL.AI110.DLD_GB.Entities
{

    public class Categorie
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        [Required(ErrorMessage = "Le champ Rayon_Id est obligatoire")] public int RayonId { get; set; }

        public Categorie(int id, string nom, int rayonId)
        {
            Id = id;
            Nom = nom;
            RayonId = rayonId;
        }

        public Categorie()
        {
        }
    }
}
