﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GB.Entities
{
    public class Taille
    {
        public int HauteurMoyenne { get; set; }
        public int Id { get; set; }
        public int LargeurMoyenne { get; set; }
        public int LongueurMoyenne { get; set; }
        public string NomTaille { get; set; }

        public Taille(int hauteurMoyenne, int id, int largeurMoyenne, int longueurMoyenne, string nomTaille)
        {
            HauteurMoyenne = hauteurMoyenne;
            Id = id;
            LargeurMoyenne = largeurMoyenne;
            LongueurMoyenne = longueurMoyenne;
            NomTaille = nomTaille;
        }

        public Taille()
        {
        }
    }
}
