﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Fr.EQL.AI110.DLD_GB.Entities
{
    public class Donation
    {
        [Required(ErrorMessage = "Le champ Adhérent_Id est obligatoire")] public int AdherentId { get; set; }
        public DateTime DateAnnulationDon { get; set; }
        public DateTime DateDeDepot { get; set; }
        public DateTime DateDeMiseAuRebut { get; set; }
        public DateTime DateEntreeProposition { get; set; }
        public DateTime DatePeremption { get; set; }
        [Required(ErrorMessage = "Le champ Emballage_Id est obligatoire")] public int EmballageId { get; set; }
        public int? EmplacementDeStockageId { get; set; }
        public int Id { get; set; }
        public string NomEtNDeVoie { get; set; }
        [Required(ErrorMessage = "Le champ Produit_Id est obligatoire")] public int ProduitId { get; set; }
        public float QuantiteProduit { get; set; }
        [Required(ErrorMessage = "Le champ Reservable est obligatoire")] public bool Reservable { get; set; }
        [Required(ErrorMessage = "Le champ Unité de mesure_Id est obligatoire")] public int UniteDeMesureId { get; set; }
        public int? VilleCpId { get; set; }

        public List<PlageHoraire> Plages { get; set; }

        public PlageHoraire Lundi { get; set; }
        public PlageHoraire Mardi { get; set; }
        public PlageHoraire Mercredi { get; set; }
        public PlageHoraire Jeudi { get; set; }
        public PlageHoraire Vendredi { get; set; }
        public PlageHoraire Samedi { get; set; }
        public PlageHoraire Dimanche { get; set; }
        public int CP { get; set; }
        public bool ADomicile { get; set; }
        public VilleCP VilleCP { get; set; }
        public string PrenomDonateur { get; set; }
        public string NomDonateur { get; set; }


        public Donation(int adherentId, DateTime dateAnnulationDon, DateTime dateDeDepot, DateTime dateDeMiseAuRebut, DateTime dateEntreeProposition, DateTime datePeremption,
            int emballageId, int emplacementDeStockageId, int id, string nomEtNDeVoie, int produitId, float quantiteProduit, bool reservable, int uniteDeMesureId, int villeCpId)
        {
            AdherentId = adherentId;
            DateAnnulationDon = dateAnnulationDon;
            DateDeDepot = dateDeDepot;
            DateDeMiseAuRebut = dateDeMiseAuRebut;
            DateEntreeProposition = dateEntreeProposition;
            DatePeremption = datePeremption;
            EmballageId = emballageId;
            EmplacementDeStockageId = emplacementDeStockageId;
            Id = id;
            NomEtNDeVoie = nomEtNDeVoie;
            ProduitId = produitId;
            QuantiteProduit = quantiteProduit;
            Reservable = reservable;
            UniteDeMesureId = uniteDeMesureId;
            VilleCpId = villeCpId;
        }

        public Donation()
        {
        }
    }
}
