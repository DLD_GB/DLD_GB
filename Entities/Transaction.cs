﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Fr.EQL.AI110.DLD_GB.Entities
{

    public class Transaction
    {
        [Required(ErrorMessage = "Le champ Adhérent_Id est obligatoire")] public int AdherentId { get; set; }
        public int AugmentationCredit { get; set; }
        public DateTime DateTransaction { get; set; }
        public int DiminutionCredit { get; set; }
        public int Id { get; set; }
        public int ModePaiementId { get; set; }
        public int MontantPaiement { get; set; }

        public Transaction()
        {
        }
    }
}
