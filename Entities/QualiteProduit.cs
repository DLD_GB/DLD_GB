﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Fr.EQL.AI110.DLD_GB.Entities
{
    public class QualiteProduit
    {
        public int Id { get; set; }
        public string Nom { get; set; }

        public QualiteProduit(int id, string nom)
        {
            Id = id;
            Nom = nom;
        }

        public QualiteProduit()
        {
        }
    }
}
