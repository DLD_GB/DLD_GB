﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Fr.EQL.AI110.DLD_GB.Entities
{
    public class QualiteEchange
    {
        public int Id { get; set; }
        public string Nom { get; set; }

        public QualiteEchange(int id, string nom)
        {
            Id = id;
            Nom = nom;
        }

        public QualiteEchange()
        {
        }
    }
}
