﻿
using System.ComponentModel.DataAnnotations;


namespace Fr.EQL.AI110.DLD_GB.Entities
{
    public class Adherent
    {
        public DateTime DateDeNaissance { get; set; }
        public DateTime DateDesinscription { get; set; }
        public DateTime DateReinscription { get; set; }
        public DateTime DateSaisieInscription { get; set; }
        public DateTime DateValidationInscription { get; set; }
        public bool DroitAdministrateur { get; set; }
        public string Email { get; set; }
        public int Id { get; set; }
        public string MotDePasse { get; set; }
        public int MotifDesinscriptionId { get; set; }
        public string Nom { get; set; }
        public string NomEtNDeVoie { get; set; }
        public string NomUtilisateur { get; set; }
        public string Prenom { get; set; }
        [Required(ErrorMessage = "Le champ Ville-CP_Id est obligatoire")] public int VilleCpId { get; set; }

        public Adherent(DateTime dateDeNaissance, DateTime dateDesinscription, DateTime dateReinscription, DateTime dateSaisieInscription, 
            DateTime dateValidationInscription, bool droitAdministrateur, string email, int id, string motDePasse, int motifDesinscriptionId, 
            string nom, string nomEtNDeVoie, string nomUtilisateur, string prenom, int villeCpId)
        {
            DateDeNaissance = dateDeNaissance;
            DateDesinscription = dateDesinscription;
            DateReinscription = dateReinscription;
            DateSaisieInscription = dateSaisieInscription;
            DateValidationInscription = dateValidationInscription;
            DroitAdministrateur = droitAdministrateur;
            Email = email;
            Id = id;
            MotDePasse = motDePasse;
            MotifDesinscriptionId = motifDesinscriptionId;
            Nom = nom;
            NomEtNDeVoie = nomEtNDeVoie;
            NomUtilisateur = nomUtilisateur;
            Prenom = prenom;
            VilleCpId = villeCpId;
        }
        public Adherent()
        {
        }


    }
}
