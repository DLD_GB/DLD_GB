﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Fr.EQL.AI110.DLD_GB.Entities
{
    public class DonationDetails : Donation
    {
        public int? IdDonation { get; set; }
        public string? NomPartenaire { get; set; }
        public string? AdresseDon { get; set; }
        public string NomProduit { get; set; }
        public string NomCategorie { get; set; }
        public string NomRayon { get; set; }
        public string UniteMesure { get; set; }
        public string Emballage { get; set; }
        public int CodePostal { get; set; }
        public string Ville { get; set; }
        public string TypeStockage { get; set; }
        public string TemperatureStockage { get; set; }
        public int TemperatureStockageId { get; set; }
        public bool? Box { get; set; }
        public string NomNumeroVoie { get; set; }
        public int PartnerId { get; set; }
        public int DispoEmplacementStockage { get; set; }
        public string Adherent { get; set; }
        public string Produit { get; set; }


        public int RayonId { get; set; }
        public int CategorieId { get; set; }
        public string NomDonateur { get; set; }

        public List<VilleCP> Villes { get; set;}
        public List<Partner> Partners { get; set; }
        public List<StorageArea> StorageAreas { get; set; }

        public string picture { get; set; }


    }
}
