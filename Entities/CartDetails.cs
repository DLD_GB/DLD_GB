﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GB.Entities
{
    public class CartDetails : Cart
    {
        public string ProduitNom { get; set; }
        public string UniteDeMesure { get; set;}
        public int Quantite { get; set; }
        public string Categorie { get; set; }

        public string AdherentNom { get; set; }
    }
}
