﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GB.Entities
{
    public class Emballage
    {
        public int Id { get; set; }
        public string Nom { get; set; }

        public Emballage(int id, string nom)
        {
            Id = id;
            Nom = nom;
        }

        public Emballage()
        {
        }
    }
}
