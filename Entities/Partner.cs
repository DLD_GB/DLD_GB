﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GB.Entities
{
    public class Partner
    {
        public int Id { get; set; }
        public double Siret { get; set; }
        public string NomSociete { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string NomReferent { get; set; }
        public string PrenomReferent { get; set; }
        public string MotDePasse { get; set; }
        public DateTime DateSaisieInscription { get; set; }
        public DateTime DateValidationInscription { get; set; }
        public string NomEtNDeVoie { get; set; }
        public DateTime DateDesinscription { get; set; }
        public DateTime DateReinscritpion { get; set; }
        public int VilleCPId { get; set; }
        public int MotifDesinscriptionId { get; set; }

        public Partner(int id, double siret, string nomSociete, 
            string telephone, string email, string nomReferent, 
            string prenomReferent, string motDePasse, 
            DateTime dateSaisieInscription, 
            DateTime dateValidationInscription, 
            string nomEtNDeVoie, DateTime dateDesinscription, 
            DateTime dateReinscritpion, int villeCPId, 
            int motifDesinscriptionId)
        {
            Id = id;
            Siret = siret;
            NomSociete = nomSociete;
            Telephone = telephone;
            Email = email;
            NomReferent = nomReferent;
            PrenomReferent = prenomReferent;
            MotDePasse = motDePasse;
            DateSaisieInscription = dateSaisieInscription;
            DateValidationInscription = dateValidationInscription;
            NomEtNDeVoie = nomEtNDeVoie;
            DateDesinscription = dateDesinscription;
            DateReinscritpion = dateReinscritpion;
            VilleCPId = villeCPId;
            MotifDesinscriptionId = motifDesinscriptionId;
        }

        public Partner()
        {
        }
    }
}
