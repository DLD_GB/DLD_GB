﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GB.Entities
{
    public class TemperatureStockage
    {
        public int Id { get; set; }
        public string Nom { get; set; }

        public TemperatureStockage(int id, string nom)
        {
            Id = id;
            Nom = nom;
        }

        public TemperatureStockage()
        {
        }
    }
}
