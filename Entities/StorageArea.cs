﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Fr.EQL.AI110.DLD_GB.Entities
{
    public class StorageArea
    {

        [Required(ErrorMessage = "Le champ Box_automatique est obligatoire")]
        public bool BoxAutomatique { get; set; }
        public DateTime DateAjoutStockage { get; set; }
        public DateTime DateRetraitStockage { get; set; }

        [Required(ErrorMessage = "Le champ Disponible est obligatoire")]
        public bool Disponible { get; set; }
        public int Id { get; set; }
        public string NomEtNDeVoie { get; set; }

        [Required(ErrorMessage = "Le champ Partenaire_Id est obligatoire")]
        public int PartenaireId { get; set; }

        [Required(ErrorMessage = "Le champ Taille_Id est obligatoire")]
        public int TailleId { get; set; }

        [Required(ErrorMessage = "Le champ Temperature_Stockage_Id est obligatoire")]
        public int TemperatureStockageId { get; set; }
        public int VilleCpId { get; set; }

        //public List<PlageHoraire> PlageHoraire { get; set; }

        public StorageArea(bool boxAutomatique, DateTime dateAjoutStockage, DateTime dateRetraitStockage,
            bool disponible, int id, string nomEtNDeVoie, int partenaireId, int tailleId, int temperatureStockageId, int villeCpId)
        {
            BoxAutomatique = boxAutomatique;
            DateAjoutStockage = dateAjoutStockage;
            DateRetraitStockage = dateRetraitStockage;
            Disponible = disponible;
            Id = id;
            NomEtNDeVoie = nomEtNDeVoie;
            PartenaireId = partenaireId;
            TailleId = tailleId;
            TemperatureStockageId = temperatureStockageId;
            VilleCpId = villeCpId;
        }

        public StorageArea()
        {
        }
    }
}
