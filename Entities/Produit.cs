﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Fr.EQL.AI110.DLD_GB.Entities
{
    public class Produit
    {
        [Required(ErrorMessage = "Le champ Catégorie_Id est obligatoire")] public int CategorieId { get; set; }
        public int Id { get; set; }
        public string Nom { get; set; }
        public int TemperatureStockageId { get; set; }

        public Produit(int categorieId, int id, string nom, int temperatureStockageId)
        {
            CategorieId = categorieId;
            Id = id;
            Nom = nom;
            TemperatureStockageId = temperatureStockageId;
        }

        public Produit()
        {
        }
    }
}
