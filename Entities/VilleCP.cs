﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GB.Entities
{
    public class VilleCP
    {
        public int Cp { get; set; }
        public int Id { get; set; }
        public double Latitude  { get; set; }
        public double Longitude  { get; set; }
        public string Ville { get; set; }

        public VilleCP(int id, string ville, int cp, double latitude, double longitude)
        {
            Id = id;
            Ville = ville;
            Cp = cp;
            Latitude = latitude;
            Longitude = longitude;
        }

        public VilleCP()
        {
        }
    }
}
