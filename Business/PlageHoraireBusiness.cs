﻿using Fr.EQL.AI110.DLD_GB.DataAccess;
using Fr.EQL.AI110.DLD_GB.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.Entities;
using Fr.EQL.AI110.DLD_GB.DataAccess;

namespace Fr.EQL.AI110.DLD_GB.Business
{
    public class PlageHoraireBusiness
    {
        //public void SavePlageHoraire(PlageHoraire plageHoraire, StorageArea storageArea)
        //{
        //    PlageHoraireDAO plageHoraireDAO = new PlageHoraireDAO();
        //    plageHoraireDAO.Insert(plageHoraire, storageArea);
        //}

        //public List<PlageHoraire> GetPlageHoraireByIdEspaceDeStockage(int idStorageArea)
        //{
        //    PlageHoraireDAO plageHoraireDAO = new PlageHoraireDAO();
        //    return plageHoraireDAO.GetAllByIdEspaceDeStockage(idStorageArea);
        //}

        public List<PlageHoraire> GetAllPlageHoraireByIdDonateur(int id)
        {
            PlageHoraireDAO PlageHoraireDAO = new PlageHoraireDAO();
            return PlageHoraireDAO.GetAllByIdDon(id);
        }

        public void InsertPlageHoraireForDon(int IdDon, PlageHoraire plageHoraire, int jour)
        {
            PlageHoraireDAO plageHoraireDAO = new PlageHoraireDAO();
            plageHoraireDAO.InsertForDon(IdDon,plageHoraire, jour);
        }
    }
}
