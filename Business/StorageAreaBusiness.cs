﻿using Fr.EQL.AI110.DLD_GB.DataAccess;
using Fr.EQL.AI110.DLD_GB.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GB.Business
{
	public class StorageAreaBusiness
	{
		public void UpdateStorageArea(StorageArea storageArea)
		{
			StorageAreaDAO dao = new StorageAreaDAO();
			dao.Update(storageArea);
		}

		public StorageArea GetStorageAreaById(int id)
		{
			StorageAreaDAO dao = new StorageAreaDAO();
			return dao.GetById(id);
		}
		public List<StorageArea> GetStorageAreaDispoByMulticritera(int partnerId, int temperatureStockageId)
        {
			StorageAreaDAO storageAreaDAO = new StorageAreaDAO();
			return storageAreaDAO.GetStorageAreaDispoByMulticritera(partnerId, temperatureStockageId);
        }

		public void SaveStorageArea(StorageArea storageArea)
		{
			StorageAreaDAO storageAreaDAO = new StorageAreaDAO();
			storageAreaDAO.Insert(storageArea);
		}

		public void CancelStorageArea(int id)
		{
			StorageAreaDAO storageAreaDAO = new StorageAreaDAO();
			storageAreaDAO.Cancel(id);
		}

		public List<StorageAreaDetails> GetStorageAreasByIdPartner(int IdPartner)
		{
			StorageAreaDAO storageAreaDAO = new StorageAreaDAO();
			return storageAreaDAO.GetAllWithDetailsByIdPartner(IdPartner);
		}

	}
}
