﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.DataAccess;
using Fr.EQL.AI110.DLD_GB.Entities;

namespace Fr.EQL.AI110.DLD_GB.Business
{
    public class AdherentBusiness
    {
        public List<Adherent> GetAllAdherents()
        {
            AdherentDAO adherentDAO = new AdherentDAO(); 
            return adherentDAO.getAll();
        }
        public Adherent GetAdherentByAdherentId(int id)
        {
            AdherentDAO adherentDAO = new AdherentDAO();
            return adherentDAO.getByAdherentId(id);

        }

        public List<Adherent> SearchAdherent(string nom, string prenom)
        {
            AdherentDAO adherentDAO = new AdherentDAO();
            return  adherentDAO.GetByMulticriteria(nom, prenom);
        }
        public Adherent GetAdherentById(int id)
        {
            AdherentDAO AdherentDAO = new AdherentDAO();
            return AdherentDAO.GetById(id);

        }
    }
}
