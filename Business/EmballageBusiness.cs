﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.DataAccess;
using Fr.EQL.AI110.DLD_GB.Entities;

namespace Fr.EQL.AI110.DLD_GB.Business
{
    public class EmballageBusiness
    {
        public List<Emballage> GetAllEmballage()
        {
            EmballageDAO EmballageDAO = new EmballageDAO();
            return EmballageDAO.GetAll();
        }

        public string GetAllEmballageInString()
        {
            string result = "";
            EmballageDAO EmballageDAO = new EmballageDAO();
            List<Emballage> ListeEmballages = EmballageDAO.GetAll();
            foreach (Emballage r in ListeEmballages) {
                result = result + r.Nom;
            }
            return result;
        }
    }
}
