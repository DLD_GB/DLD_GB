﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.DataAccess;
using Fr.EQL.AI110.DLD_GB.Entities;

namespace Fr.EQL.AI110.DLD_GB.Business
{
    public class ProduitBusiness
    {

        public List<Produit> GetAllProduit()
        {
            ProduitDAO ProduitDAO = new ProduitDAO();
            return ProduitDAO.GetAll();
        }

        public string GetAllProduitInString()
        {
            string result = "";
            ProduitDAO ProduitDAO = new ProduitDAO();
            List<Produit> ListeProduits = ProduitDAO.GetAll();
            foreach (Produit r in ListeProduits)
            {
                result = result + r.Nom;
            }
            return result;
        }
        public List<Produit> GetProduitByIdCategorieId(int categorieId)
        {
            ProduitDAO ProduitDAO = new ProduitDAO();
            List<Produit> ListeProduits = ProduitDAO.GetByCategorieId(categorieId);
            return ListeProduits;

        }

        public Produit GetProduitById(int id)
        {
            ProduitDAO ProduitDAO = new ProduitDAO();
            return ProduitDAO.GetById(id);
        }

        public Categorie GetCategorieParentByIdProduit(int id)
        {
            ProduitDAO ProduitDAO = new ProduitDAO();
            return ProduitDAO.GetCategorieParent(id);
        }

        public Produit GetProduitByIdProduit(int id)
        {
            ProduitDAO produitDAO = new ProduitDAO();
            return produitDAO.GetByIdProduit(id);
        }

        public List<Produit> GetAllProduitsByIdCategorie(int id)
        {
            ProduitDAO produitDAO = new ProduitDAO();
            return produitDAO.GetAllByIdCategorie(id);
        }
    }



}
