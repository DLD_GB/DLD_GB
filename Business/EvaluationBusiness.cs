﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.Entities;
using Fr.EQL.AI110.DLD_GB.DataAccess;

namespace Fr.EQL.AI110.DLD_GB.Business
{
    public class EvaluationBusiness
    {
        public void InsertEvaluation(Evaluation evaluation)
        {
            EvaluationDAO dao = new EvaluationDAO();
            dao.Insert(evaluation);
        }
    }
}
