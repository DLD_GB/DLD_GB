﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.Entities;
using Fr.EQL.AI110.DLD_GB.DataAccess;

namespace Fr.EQL.AI110.DLD_GB.Business
{
    public class RayonBusiness
    {

        public List<Rayon> GetAllRayon()
        {
            RayonDAO RayonDAO = new RayonDAO();
            return RayonDAO.GetAll();
        }

        public string GetAllRayonInString()
        {
            string result = "";
            RayonDAO RayonDAO = new RayonDAO();
            List<Rayon> ListeRayons = RayonDAO.GetAll();
            foreach (Rayon r in ListeRayons) {
                result = result + r.Nom;
            }
            return result;
        }

        public Rayon GetRayonById(int id)
        {
            RayonDAO RayonDAO = new RayonDAO();
            return RayonDAO.GetById(id);
        }

        public Rayon GetRayonByIdRayon(int id)
        {
            RayonDAO rayonDAO = new RayonDAO();
            return rayonDAO.GetByIdRayon(id);
        }


    }
}
