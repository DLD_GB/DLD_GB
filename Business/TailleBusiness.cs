﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.Entities;
using Fr.EQL.AI110.DLD_GB.DataAccess;

namespace Fr.EQL.AI110.DLD_GB.Business
{
    public class TailleBusiness
    {
        public List<Taille> GetAllTaille()
        {
            TailleDAO tailleDAO = new TailleDAO();
            return tailleDAO.GetAll();
        }
    }
}
