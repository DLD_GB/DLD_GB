﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.Entities;
using Fr.EQL.AI110.DLD_GB.DataAccess;

namespace Fr.EQL.AI110.DLD_GB.Business
{
    public class PartnerBusiness
    {

        public List<Partner> GetAllPartner()
        {
            PartnerDAO PartnerDAO = new PartnerDAO();
            return PartnerDAO.GetAll();
        }

        public List<Partner> SearchPartner(string name, double siret, int villeId)
		{
            PartnerDAO partnerDAO = new PartnerDAO();
            return partnerDAO.GetByMulticriteria(name, siret, villeId);
		}

        public List<Partner> SearchPartnerDispo(int villeId, int temperatureStockageId)
        {
            PartnerDAO partnerDAO = new PartnerDAO();
            return partnerDAO.GetAllPartnersDispoByMulticriteria(villeId, temperatureStockageId);
        }

        public List<Partner> DonationToStock(int id)
        {
            PartnerDAO PartnerDAO = new PartnerDAO();
            return PartnerDAO.GetDonationStockByMultiCriteria(id);
        }


    }
}
