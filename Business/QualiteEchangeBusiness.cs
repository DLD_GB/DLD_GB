﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.Entities;
using Fr.EQL.AI110.DLD_GB.DataAccess;

namespace Fr.EQL.AI110.DLD_GB.Business
{
    public class QualiteEchangeBusiness
    {

        public List<QualiteEchange> GetAllQualiteEchange()
        {
            QualiteEchangeDAO qualiteEchangeDAO = new QualiteEchangeDAO();
            return qualiteEchangeDAO.getAll();
        }
    }
}
