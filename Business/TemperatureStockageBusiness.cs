﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.Entities;
using Fr.EQL.AI110.DLD_GB.DataAccess;

namespace Fr.EQL.AI110.DLD_GB.Business
{
    public class TemperatureStockageBusiness
    {

        public List<TemperatureStockage> GetAllTemperatureStockage()
        {
            TemperatureStockageDAO TemperatureStockageDAO = new TemperatureStockageDAO();
            return TemperatureStockageDAO.GetAll();
        }

    }
}
