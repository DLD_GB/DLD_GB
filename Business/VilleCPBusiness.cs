﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.Entities;
using Fr.EQL.AI110.DLD_GB.DataAccess;

namespace Fr.EQL.AI110.DLD_GB.Business
{
    public class VilleCPBusiness
    {

        public List<VilleCP> GetAllVilleCP()
        {
            VilleCPDAO VilleCPDAO = new VilleCPDAO();
            return VilleCPDAO.GetAll();
        }

        public string listeVillesProches (VilleCP VilleReference, int distance)
        {
  
            List<VilleCP> villeCPs = new VilleCPDAO().GetAll();
            string listeVillesProches = "";
                    
            double rad = Math.PI / 180;

            foreach (VilleCP villeCP in villeCPs) {

                double dist = (Math.Acos(Math.Sin(VilleReference.Latitude * rad) * Math.Sin(villeCP.Latitude * rad) 
                    + Math.Cos(VilleReference.Latitude * rad) * Math.Cos(villeCP.Latitude * rad) * Math.Cos(villeCP.Longitude * rad - VilleReference.Longitude * rad)) 
                    * 6378.137);

                if (dist <= distance) listeVillesProches = listeVillesProches + villeCP.Ville;
            }

            return listeVillesProches;
        }

        public List<VilleCP> listeVillesProximite(VilleCP VilleReference, int distance)
        {
            List<VilleCP> result = new List<VilleCP>();

            List<VilleCP> villeCPs = new VilleCPDAO().GetAll();

            double rad = Math.PI / 180;

            foreach (VilleCP villeCP in villeCPs)
            {

                double dist = (Math.Acos(Math.Sin(VilleReference.Latitude * rad) * Math.Sin(villeCP.Latitude * rad)
                    + Math.Cos(VilleReference.Latitude * rad) * Math.Cos(villeCP.Latitude * rad) * Math.Cos(villeCP.Longitude * rad - VilleReference.Longitude * rad))
                    * 6378.137);

                if (dist <= distance) result.Add(villeCP);
            }

            return result;
        }

        public VilleCP GetVilleCPByNomDeVille(string Ville)
        {
            VilleCPDAO VilleCPDAO = new VilleCPDAO();
            return VilleCPDAO.GetByNomDeVille(Ville);
        }

        public VilleCP GetVilleCPByVilleEtCP(string Ville,string cp)
        {
            VilleCPDAO VilleCPDAO = new VilleCPDAO();
            return VilleCPDAO.GetByNomDeVilleEtCP(Ville,cp);    
        }

        public VilleCP GetVilleCPByIdVille(int id)
        {
            VilleCPDAO villeCPDAO = new VilleCPDAO();
            return villeCPDAO.GetByIdVille(id);
        }
    }
}
