﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.Entities;
using Fr.EQL.AI110.DLD_GB.DataAccess;

namespace Fr.EQL.AI110.DLD_GB.Business
{
    public class CategorieBusiness
    {

        public List<Categorie> GetAllCategorie()
        {
            CategorieDAO CategorieDAO = new CategorieDAO();
            return CategorieDAO.GetAll();
        }

        public string GetAllCategorieInString()
        {
            string result = "";
            CategorieDAO CategorieDAO = new CategorieDAO();
            List<Categorie> ListeCategories = CategorieDAO.GetAll();
            foreach (Categorie r in ListeCategories)
            {
                result = result + r.Nom;
            }
            return result;
        }

        public List<Categorie> GetCategorieByRayonId(int rayonID)
        {
            CategorieDAO CategorieDAO = new CategorieDAO();
            List<Categorie> ListeCategories = CategorieDAO.GetByRayonId(rayonID);
            return ListeCategories;
                
        }

        public Categorie GetCategorieById(int id)
        {
            CategorieDAO CategorieDAO = new CategorieDAO();
            return CategorieDAO.GetById(id);
        }


        public Rayon GetRayonParentByIdCategorie(int id)
        {
            CategorieDAO CategorieDAO = new CategorieDAO();
            return CategorieDAO.GetRayon(id);
        }
        public List<Categorie> GetAllCategorieByIdRayon(int id)
        {
            CategorieDAO categorieDAO = new CategorieDAO();
            return categorieDAO.GetAllByIdRayon(id);
        }

        public Categorie GetCategorieByIdCategorie(int id)
        {
            CategorieDAO categorieDAO = new CategorieDAO();
            return categorieDAO.GetByIdCategorie(id);
        }

    }
}
