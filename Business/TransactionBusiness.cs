﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.Entities;
using Fr.EQL.AI110.DLD_GB.DataAccess;

namespace Fr.EQL.AI110.DLD_GB.Business
{
    public class TransactionBusiness
    {

        public List<Transaction> GetAllTransaction()
        {
            TransactionDAO TransactionDAO = new TransactionDAO();
            return TransactionDAO.GetAll();
        }

        public List<Transaction> GetTransactionByIdAdherent(int AdherentId)
        {
            TransactionDAO TransactionDAO = new TransactionDAO();
            List<Transaction> ListeTransactions = TransactionDAO.GetByIdAdherent(AdherentId);
            return ListeTransactions;
                
        }

        public Transaction GetTransactionById(int id)
        {
            TransactionDAO TransactionDAO = new TransactionDAO();
            return TransactionDAO.GetById(id);
        }

        public int CagnotteByAdherentId(int AdherentId)
        {
            int result = 0;

            TransactionDAO TransactionDAO = new TransactionDAO();
            List<Transaction> ListeTransactions = TransactionDAO.GetByIdAdherent(AdherentId);
            foreach(Transaction transaction in ListeTransactions)
            {
                result = result + transaction.AugmentationCredit - transaction.DiminutionCredit;
            }

            return result;
        }
    }
}
