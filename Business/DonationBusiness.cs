﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.DataAccess;
using Fr.EQL.AI110.DLD_GB.Entities;

namespace Fr.EQL.AI110.DLD_GB.Business
{
    public class DonationBusiness
    {

        public List<DonationDetails> GetDonationByMultiCriteria(int RayonId, int CategorieId, int ProduitId,
            string NomRayon, string NomCategorie, string NomProduit, string UniteMesure, int QuantiteProduit,
        string TemperatureStockage, string Ville, string TypeStockage, bool? Box, bool? Reservable, string ListeVillesProches, string ListeCategories, string ListeRayons, string ListeProduits)
        {
            DonationDAO donationDAO = new DonationDAO();
            return donationDAO.GetByMultiCriteria(RayonId,CategorieId,ProduitId, NomRayon, NomCategorie, NomProduit, UniteMesure, QuantiteProduit,
                TemperatureStockage, Ville, TypeStockage, Box, Reservable, ListeVillesProches, ListeCategories, ListeRayons,ListeProduits);
        }

        public int CreateDonation(Donation donation)
        {
            DonationDAO dao = new DonationDAO();
           return dao.Insert(donation);

        }

        

        

        //public List<Donation> GetDonations()
        //{
        //    DonationDAO dao = new DonationDAO();
        //    return dao.GetAll();
        //}


        public void CancelDonation(int id)
        {
            DonationDAO donationDAO = new DonationDAO();
            donationDAO.Cancel(id);
        }

        public List<DonationDetails> GetDonationByIdAdherent(int id)
        {
            DonationDAO dao = new DonationDAO();
            return dao.GetByIdAdherent(id);
        }

        public List<DonationDetails> GetDonationByIdPartner(int id)
        {
            DonationDAO dao = new DonationDAO();
            return dao.GetByIdPartner(id);
        }

        public DonationDetails GetDonationById(int id)
        {
            DonationDAO DonationDAO = new DonationDAO();
            return DonationDAO.GetById(id);

        }

        public List<DonationDetails> GetReservedDonationsByIdAdherent(int id)
        {
            DonationDAO dao = new DonationDAO();
            return dao.GetReservedById(id);
        }

        public void ReserveDonation(int idDon, int IdAdherent)
        {
            DonationDAO donationDAO = new DonationDAO();
            donationDAO.ReserveById(idDon, IdAdherent);
        }

        public void CancelReservation(int idDon)
        {
            DonationDAO donationDAO = new DonationDAO();
            donationDAO.CancelReservation(idDon);
        }

        public void ConfirmerReceptionDeDon(int idDon)
        {
            DonationDAO donationDAO = new DonationDAO();
            donationDAO.ConfirmerReception(idDon);
        }
        public void ConfirmerDepotDon(int idDon)
        {
            DonationDAO donationDAO = new DonationDAO();
            donationDAO.DepotDon(idDon);
        }
    }


}
