﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GB.DataAccess;
using Fr.EQL.AI110.DLD_GB.Entities;

namespace Fr.EQL.AI110.DLD_GB.Business
{
    public class CartBusiness
    {
        public void addDonation(int idDonation, int idAdherent)
        {
            CartDAO cartDAO = new CartDAO(); 
            cartDAO.InsertDonation(idDonation, idAdherent);
        }

        public List<Cart> GetByIdAdherent(int idAdherent)
        {
            CartDAO cartDAO = new CartDAO();
           return  cartDAO.GetByIdAdherent(idAdherent);
        }

        public int TotalPanierByIdAdherent(int idAdherent)
        {
            CartDAO cartDAO = new CartDAO();
            return cartDAO.GetByIdAdherent(idAdherent).Count();
        }

        public List<CartDetails> GetAllCartDetailsByIdAdherent(int idAdherent)
        {
            CartDAO cartDAO = new CartDAO();
            return cartDAO.GetAllCartDetailsByIdAdherent(idAdherent);
        }

        public void DeleteCartByIdCart(int idCart)
        {
            CartDAO cartDAO = new CartDAO();
            cartDAO.DeleteByIdCart(idCart);
        }

    }
}
