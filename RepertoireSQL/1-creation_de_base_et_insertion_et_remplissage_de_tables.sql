DROP DATABASE IF EXISTS dld_gb_bdd;
CREATE SCHEMA dld_gb_bdd DEFAULT CHARACTER SET utf8mb4 ;/*==============================================================*/
/* Nom de SGBD :  MySQL 5.0                                     */
/* Date de création :  13/12/2021 16:33:09                      */
/*==============================================================*/
USE dld_gb_bdd;

drop table if exists Adherent;

drop table if exists Categorie;

drop table if exists Categorie_reclamation;

drop table if exists Don;

drop table if exists Echange;

drop table if exists Emballage;

drop table if exists Emplacement_de_stockage;

drop table if exists Evaluation;

drop table if exists Jours;

drop table if exists Mode_paiement;

drop table if exists Motif_annulation_echange;

drop table if exists Motif_desinscription;

drop table if exists Motif_non_recuperation;

drop table if exists Panier;

drop table if exists Partenaire;

drop table if exists Periode_indisponibilite;

drop table if exists Plage_horaire;

drop table if exists Produit;

drop table if exists Qualite_echange;

drop table if exists Qualite_produit;

drop table if exists Rayon;

drop table if exists Reclamation;

drop table if exists Taille;

drop table if exists Temperature_Stockage;

drop table if exists Transaction;

drop table if exists Unite_de_mesure;

drop table if exists Ville_CP;

drop table if exists mesurer;

/*==============================================================*/
/* Table : Adherent                                             */
/*==============================================================*/
create table Adherent
(
   Id                   int not null auto_increment,
   Ville_CP_Id          int not null,
   Motif_desinscription_Id int,
   Nom                  varchar(254),
   Prenom               varchar(254),
   Nom_utilisateur      varchar(254),
   Date_de_naissance    datetime,
   Email                varchar(254),
   Mot_de_passe         varchar(254),
   Date_saisie_Inscription datetime,
   Date_validation_inscription datetime,
   Nom_et_n_de_voie     varchar(254),
   droit_Administrateur bool,
   Date_desinscription  datetime,
   Date_reinscription   datetime,
   primary key (Id)
);

/*==============================================================*/
/* Table : Categorie                                            */
/*==============================================================*/
create table Categorie
(
   Id                   int not null auto_increment,
   Rayon_Id             int not null,
   Nom                  varchar(254),
   primary key (Id)
);

/*==============================================================*/
/* Table : Categorie_reclamation                                */
/*==============================================================*/
create table Categorie_reclamation
(
   Id                   int not null auto_increment,
   nom                  varchar(254),
   primary key (Id)
);

/*==============================================================*/
/* Table : Don                                                  */
/*==============================================================*/
create table Don
(
   Id                   int not null auto_increment,
   Produit_Id           int not null,
   Ville_CP_Id          int,
   Adherent_Id          int not null,
   Emballage_Id         int not null,
   Emplacement_de_stockage_Id int,
   Unite_de_mesure_Id   int not null,
   Quantite_produit     float,
   Date_peremption      datetime,
   Date_de_depot        datetime,
   Date_annulation_don  datetime,
   Nom_et_n_de_voie     varchar(254),
   Date_entree_proposition datetime,
   Date_de_mise_au_rebut_ datetime,
   Reservable           bool not null,
   primary key (Id)
);

/*==============================================================*/
/* Table : Echange                                              */
/*==============================================================*/
create table Echange
(
   Id                   int not null auto_increment,
   Don_Id               int not null,
   Motif_annulation_echange_Id int not null,
   Adherent_Id          int not null,
   Date_reservation     datetime,
   Date_annulation_echange datetime,
   Date_validation      datetime,
   Date_de_retrait_du_produit datetime,
   Date_recuperation_produit datetime,
   primary key (Id)
);

/*==============================================================*/
/* Table : Emballage                                            */
/*==============================================================*/
create table Emballage
(
   Id                   int not null auto_increment,
   nom                  varchar(254),
   primary key (Id)
);

/*==============================================================*/
/* Table : Emplacement_de_stockage                              */
/*==============================================================*/
create table Emplacement_de_stockage
(
   Id                   int not null auto_increment,
   Taille_Id            int not null,
   Temperature_Stockage_Id int not null,
   Ville_CP_Id          int,
   Partenaire_Id        int not null,
   Nom_et_n_de_voie     varchar(254),
   Date_ajout_stockage  datetime,
   Date_retrait_Stockage datetime,
   Box_automatique      bool,
   Disponible           bool not null,
   primary key (Id)
);

/*==============================================================*/
/* Table : Evaluation                                           */
/*==============================================================*/
create table Evaluation
(
   Id                   int not null auto_increment,
   Qualite_echange_Id   int not null,
   Qualite_produit_Id   int not null,
   Echange_Id           int not null,
   Note_evaluation      int,
   Commentaire_evaluation varchar(254),
   Date_evaluation      datetime,
   primary key (Id)
);

/*==============================================================*/
/* Table : Jours                                                */
/*==============================================================*/
create table Jours
(
   Id                   int not null auto_increment,
   jours_semaine        varchar(254),
   primary key (Id)
);

/*==============================================================*/
/* Table : Mode_paiement                                        */
/*==============================================================*/
create table Mode_paiement
(
   Id                   int not null auto_increment,
   nom_                 varchar(254),
   primary key (Id)
);

/*==============================================================*/
/* Table : Motif_annulation_echange                             */
/*==============================================================*/
create table Motif_annulation_echange
(
   Id                   int not null auto_increment,
   Raison               varchar(254),
   primary key (Id)
);

/*==============================================================*/
/* Table : Motif_desinscription                                 */
/*==============================================================*/
create table Motif_desinscription
(
   Id                   int not null auto_increment,
   Raison_              varchar(254),
   primary key (Id)
);

/*==============================================================*/
/* Table : Motif_non_recuperation                               */
/*==============================================================*/
create table Motif_non_recuperation
(
   Id                   int not null auto_increment,
   Echange_Id           int not null,
   raison               varchar(254),
   primary key (Id)
);

/*==============================================================*/
/* Table : Panier                                               */
/*==============================================================*/
create table Panier
(
   Don_Id               int not null,
   Adherent_Id          int not null,
   Id                   int not null auto_increment,
   primary key (Id)
);

/*==============================================================*/
/* Table : Partenaire                                           */
/*==============================================================*/
create table Partenaire
(
   Id                   int not null auto_increment,
   Motif_desinscription_Id int,
   Ville_CP_Id          int not null,
   SIRET                int,
   Nom_Societe          varchar(254),
   Telephone            varchar(254),
   Email                varchar(254),
   Nom_referent         varchar(254),
   Prenom_referent      varchar(254),
   Mot_de_passe         varchar(254),
   Date_saisie_inscription datetime,
   Date_validation_inscription datetime,
   Nom_et_n_de_voie     varchar(254),
   Date_desinscription  datetime,
   Date_reinscription   datetime,
   primary key (Id)
);

/*==============================================================*/
/* Table : Periode_indisponibilite                              */
/*==============================================================*/
create table Periode_indisponibilite
(
   Id                   int not null auto_increment,
   Partenaire_Id        int not null,
   Adherent_Id          int not null,
   Date_debut           datetime,
   Fin_Jour_            datetime,
   primary key (Id)
);

/*==============================================================*/
/* Table : Plage_horaire                                        */
/*==============================================================*/
create table Plage_horaire
(
   Id                   int not null auto_increment,
   Adherent_Id          int,
   Emplacement_de_stockage_Id int not null,
   Jours_Id             int not null,
   Partenaire_Id        int,
   Don_Id               int,
   Debut_plage_1        datetime,
   Fin_plage_1          datetime,
   Debut_plage_2        datetime,
   Fin_plage_2          datetime,
   primary key (Id)
);

/*==============================================================*/
/* Table : Produit                                              */
/*==============================================================*/
create table Produit
(
   Id                   int not null auto_increment,
   Categorie_Id         int not null,
   Temperature_Stockage_Id int,
   Nom                  varchar(254),
   primary key (Id)
);

/*==============================================================*/
/* Table : Qualite_echange                                      */
/*==============================================================*/
create table Qualite_echange
(
   Id                   int not null auto_increment,
   Libelle_quali_ech    varchar(254),
   primary key (Id)
);

/*==============================================================*/
/* Table : Qualite_produit                                      */
/*==============================================================*/
create table Qualite_produit
(
   Id                   int not null auto_increment,
   libelle_quali_prod   varchar(254),
   primary key (Id)
);

/*==============================================================*/
/* Table : Rayon                                                */
/*==============================================================*/
create table Rayon
(
   Id                   int not null auto_increment,
   Nom                  varchar(254),
   primary key (Id)
);

/*==============================================================*/
/* Table : Reclamation                                          */
/*==============================================================*/
create table Reclamation
(
   Id                   int not null auto_increment,
   Adherent_Id          int not null,
   Categorie_reclamation_Id int not null,
   Echange_Id           int not null,
   Date_reclamation     datetime,
   Commentaire_         varchar(254),
   Date_traitement      datetime,
   Note_evaluation_traitement int,
   Date_cloture_reclamation datetime,
   primary key (Id)
);

/*==============================================================*/
/* Table : Taille                                               */
/*==============================================================*/
create table Taille
(
   Id                   int not null auto_increment,
   nom_taille           varchar(254),
   Hauteur_moyenne      int,
   Largeur_moyenne      int,
   Longeur_moyenne      int,
   primary key (Id)
);

/*==============================================================*/
/* Table : Temperature_Stockage                                 */
/*==============================================================*/
create table Temperature_Stockage
(
   Id                   int not null auto_increment,
   Nom                  varchar(254),
   primary key (Id)
);

/*==============================================================*/
/* Table : Transaction                                          */
/*==============================================================*/
create table Transaction
(
   Id                   int not null auto_increment,
   Adherent_Id          int not null,
   Mode_paiement_Id     int,
   Augmentation_credit  int,
   Montant_paiement     int,
   credit_de_recuperation_ int,
   Date_transaction     datetime,
   primary key (Id)
);

/*==============================================================*/
/* Table : Unite_de_mesure                                      */
/*==============================================================*/
create table Unite_de_mesure
(
   Id                   int not null auto_increment,
   nom                  varchar(254),
   primary key (Id)
);

/*==============================================================*/
/* Table : Ville_CP                                             */
/*==============================================================*/
create table Ville_CP
(
   Id                   int not null auto_increment,
   Ville                varchar(254),
   Code_postal          int,
   Latitude             decimal(11,8),
   Longitude            decimal(11,8),
   primary key (Id)
);

/*==============================================================*/
/* Table : mesurer                                              */
/*==============================================================*/
create table mesurer
(
   Produit_Id           int not null auto_increment,
   Id                   int not null,
   primary key (Produit_Id, Id)
);

alter table Adherent add constraint FK_Association_1 foreign key (Ville_CP_Id)
      references Ville_CP (Id) on delete restrict on update restrict;

alter table Adherent add constraint FK_Association_34 foreign key (Motif_desinscription_Id)
      references Motif_desinscription (Id) on delete restrict on update restrict;

alter table Categorie add constraint FK_Association_26 foreign key (Rayon_Id)
      references Rayon (Id) on delete restrict on update restrict;

alter table Don add constraint FK_Association_13 foreign key (Emplacement_de_stockage_Id)
      references Emplacement_de_stockage (Id) on delete restrict on update restrict;

alter table Don add constraint FK_Association_14 foreign key (Ville_CP_Id)
      references Ville_CP (Id) on delete restrict on update restrict;

alter table Don add constraint FK_Association_18 foreign key (Produit_Id)
      references Produit (Id) on delete restrict on update restrict;

alter table Don add constraint FK_Association_2 foreign key (Adherent_Id)
      references Adherent (Id) on delete restrict on update restrict;

alter table Don add constraint FK_Association_24 foreign key (Unite_de_mesure_Id)
      references Unite_de_mesure (Id) on delete restrict on update restrict;

alter table Don add constraint FK_Association_25 foreign key (Emballage_Id)
      references Emballage (Id) on delete restrict on update restrict;

alter table Echange add constraint FK_Association_3 foreign key (Adherent_Id)
      references Adherent (Id) on delete restrict on update restrict;

alter table Echange add constraint FK_Association_38 foreign key (Motif_annulation_echange_Id)
      references Motif_annulation_echange (Id) on delete restrict on update restrict;

alter table Echange add constraint FK_Association_4 foreign key (Don_Id)
      references Don (Id) on delete restrict on update restrict;

alter table Emplacement_de_stockage add constraint FK_Association_16 foreign key (Partenaire_Id)
      references Partenaire (Id) on delete restrict on update restrict;

alter table Emplacement_de_stockage add constraint FK_Association_20 foreign key (Temperature_Stockage_Id)
      references Temperature_Stockage (Id) on delete restrict on update restrict;

alter table Emplacement_de_stockage add constraint FK_Association_21 foreign key (Taille_Id)
      references Taille (Id) on delete restrict on update restrict;

alter table Emplacement_de_stockage add constraint FK_Association_27 foreign key (Ville_CP_Id)
      references Ville_CP (Id) on delete restrict on update restrict;

alter table Evaluation add constraint FK_Association_17 foreign key (Echange_Id)
      references Echange (Id) on delete restrict on update restrict;

alter table Evaluation add constraint FK_Association_29 foreign key (Qualite_echange_Id)
      references Qualite_echange (Id) on delete restrict on update restrict;

alter table Evaluation add constraint FK_Association_30 foreign key (Qualite_produit_Id)
      references Qualite_produit (Id) on delete restrict on update restrict;

alter table Motif_non_recuperation add constraint FK_Association_40 foreign key (Echange_Id)
      references Echange (Id) on delete restrict on update restrict;

alter table Panier add constraint FK_Association_45 foreign key (Adherent_Id)
      references Adherent (Id) on delete restrict on update restrict;

alter table Panier add constraint FK_Association_46 foreign key (Don_Id)
      references Don (Id) on delete restrict on update restrict;

alter table Partenaire add constraint FK_Association_19 foreign key (Ville_CP_Id)
      references Ville_CP (Id) on delete restrict on update restrict;

alter table Partenaire add constraint FK_Association_35 foreign key (Motif_desinscription_Id)
      references Motif_desinscription (Id) on delete restrict on update restrict;

alter table Periode_indisponibilite add constraint FK_Association_36 foreign key (Adherent_Id)
      references Adherent (Id) on delete restrict on update restrict;

alter table Periode_indisponibilite add constraint FK_Association_37 foreign key (Partenaire_Id)
      references Partenaire (Id) on delete restrict on update restrict;

alter table Plage_horaire add constraint FK_Association_12 foreign key (Emplacement_de_stockage_Id)
      references Emplacement_de_stockage (Id) on delete restrict on update restrict;

alter table Plage_horaire add constraint FK_Association_31 foreign key (Jours_Id)
      references Jours (Id) on delete restrict on update restrict;

alter table Plage_horaire add constraint FK_Association_39 foreign key (Don_Id)
      references Don (Id) on delete restrict on update restrict;

alter table Plage_horaire add constraint FK_Association_43 foreign key (Partenaire_Id)
      references Partenaire (Id) on delete restrict on update restrict;

alter table Plage_horaire add constraint FK_Association_44 foreign key (Adherent_Id)
      references Adherent (Id) on delete restrict on update restrict;

alter table Produit add constraint FK_Association_23 foreign key (Temperature_Stockage_Id)
      references Temperature_Stockage (Id) on delete restrict on update restrict;

alter table Produit add constraint FK_Association_6 foreign key (Categorie_Id)
      references Categorie (Id) on delete restrict on update restrict;

alter table Reclamation add constraint FK_Association_15 foreign key (Echange_Id)
      references Echange (Id) on delete restrict on update restrict;

alter table Reclamation add constraint FK_Association_22 foreign key (Categorie_reclamation_Id)
      references Categorie_reclamation (Id) on delete restrict on update restrict;

alter table Reclamation add constraint FK_Association_28 foreign key (Adherent_Id)
      references Adherent (Id) on delete restrict on update restrict;

alter table Transaction add constraint FK_Association_41 foreign key (Adherent_Id)
      references Adherent (Id) on delete restrict on update restrict;

alter table Transaction add constraint FK_Association_42 foreign key (Mode_paiement_Id)
      references Mode_paiement (Id) on delete restrict on update restrict;

alter table mesurer add constraint FK_mesurerProduit foreign key (Produit_Id)
      references Produit (Id) on delete restrict on update restrict;

alter table mesurer add constraint FK_mesurerUnitéMesure foreign key (Id)
      references Unite_de_mesure (Id) on delete restrict on update restrict;

-- MySQL dump 10.13  Distrib 8.0.27, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: dld_gb_bdd
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `adherent`
--

LOCK TABLES `adherent` WRITE;
/*!40000 ALTER TABLE `adherent` DISABLE KEYS */;
INSERT INTO `adherent` VALUES (1,1,NULL,'Dupont','Emilie','EDupont','0000-00-00 00:00:00','dupontemile@gmail.com','toto','2021-12-10 00:00:00','2021-12-10 00:00:00','1 rue de la clairière',NULL,NULL,NULL),(2,5,NULL,'Henry','Georges','GHenry','1985-02-17 00:00:00','georgeshenry@gmail.com','toto','2021-12-10 00:00:00','2021-12-10 00:00:00','11 rue des champs',NULL,NULL,NULL),(3,6,NULL,'Lee','Xavier','XLee','1980-01-10 00:00:00','xavierlee@gmail.com','toto','2021-12-11 00:00:00','2021-12-11 00:00:00','15 avenue grivou',NULL,NULL,NULL),(4,7,NULL,'Brown','Sugar','SBrown','1995-10-05 00:00:00','sugarbrown@gmail.com','toto','2021-12-11 00:00:00','2021-12-11 00:00:00','5 rue de la clairière',NULL,NULL,NULL),(5,1,NULL,'Grivon','Janette','JGrivon','1990-11-11 00:00:00','janettegrivon@gmail.com','toto','2021-12-11 00:00:00','2021-12-11 00:00:00','70 rue des amandiers',NULL,NULL,NULL),(6,5,NULL,'Hubert','Simon','SHubert','1981-10-10 00:00:00','simonhubert@gmail.com','toto','2021-12-11 00:00:00','2021-12-11 00:00:00','5 rue des pieds mouillés',NULL,NULL,NULL);
/*!40000 ALTER TABLE `adherent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `categorie`
--

LOCK TABLES `categorie` WRITE;
/*!40000 ALTER TABLE `categorie` DISABLE KEYS */;
INSERT INTO `categorie` VALUES (1,1,'Boucherie'),(2,1,'Volaille et Gibier'),(3,1,'Poissonerie'),(4,1,'Traiteur de la mer'),(5,2,'Legumes'),(6,2,'Jus de fruit et legumes frais'),(7,2,'Fruits'),(8,3,'Pain et pâtisserie'),(9,3,'Pain de mie et pain grillé'),(10,3,'Cake et gâteau'),(11,4,'Lait et oeuf'),(12,4,'Beurre et crème'),(13,4,'Fromages'),(14,4,'Charcuterie');
/*!40000 ALTER TABLE `categorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `categorie_reclamation`
--

LOCK TABLES `categorie_reclamation` WRITE;
/*!40000 ALTER TABLE `categorie_reclamation` DISABLE KEYS */;
INSERT INTO `categorie_reclamation` VALUES (1,'Bénéficiaire indisponible'),(2,'Donateur indisponible'),(3,'Produit indisponible'),(4,'Mauvais produit');
/*!40000 ALTER TABLE `categorie_reclamation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `don`
--

LOCK TABLES `don` WRITE;
/*!40000 ALTER TABLE `don` DISABLE KEYS */;
INSERT INTO `don` VALUES (1,1,0,1,2,2,5,1,'2022-01-15 00:00:00','2021-12-13 00:00:00',NULL,NULL,'2021-12-13 00:00:00',NULL,0),(2,2,6,1,2,0,5,1,'2022-01-05 00:00:00','2021-12-13 00:00:00',NULL,'2 rue du don','2021-12-13 00:00:00',NULL,0),(3,6,0,4,1,0,12,1,'2021-01-08 00:00:00','2021-12-13 00:00:00',NULL,NULL,'2021-12-13 00:00:00',NULL,1),(4,7,0,3,1,7,8,1,'2021-01-06 00:00:00','2021-12-13 00:00:00',NULL,NULL,'2021-12-13 00:00:00',NULL,1),(5,10,0,4,1,4,5,1,'2022-01-07 00:00:00','2021-12-13 00:00:00',NULL,NULL,'2021-12-13 00:00:00',NULL,1),(6,5,0,4,2,5,5,1,'2022-01-08 00:00:00','2021-12-11 00:00:00',NULL,NULL,'2021-12-11 00:00:00',NULL,0),(7,6,0,2,1,0,3,3,'2022-01-08 00:00:00','2021-12-11 00:00:00',NULL,NULL,'2021-12-11 00:00:00',NULL,1);
/*!40000 ALTER TABLE `don` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `echange`
--

LOCK TABLES `echange` WRITE;
/*!40000 ALTER TABLE `echange` DISABLE KEYS */;
INSERT INTO `echange` VALUES (1,1,1,5,'2021-11-06 10:00:00','2021-11-06 14:00:00','2021-11-06 10:00:00','2021-11-10 14:14:14',NULL),(2,2,2,4,'2021-11-06 10:50:00',NULL,'2021-11-06 20:00:00','0000-00-00 00:00:00','2021-11-10 14:14:14'),(3,3,3,3,'2020-11-06 10:00:00','2020-11-06 14:00:00','2020-11-06 10:00:00','2020-11-10 14:14:14',NULL),(4,4,2,2,'2020-11-06 10:50:00',NULL,'2020-11-06 20:00:00',NULL,'2020-11-10 14:14:14'),(5,3,3,1,'2022-11-06 10:00:00','2022-11-06 14:00:00','2022-11-06 10:00:00','2022-11-10 14:14:14',NULL);
/*!40000 ALTER TABLE `echange` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `emballage`
--

LOCK TABLES `emballage` WRITE;
/*!40000 ALTER TABLE `emballage` DISABLE KEYS */;
INSERT INTO `emballage` VALUES (1,'Sans Emballage'),(2,'Neuf'),(3,'Ouvert'),(4,'Endommagé');
/*!40000 ALTER TABLE `emballage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `emplacement_de_stockage`
--

LOCK TABLES `emplacement_de_stockage` WRITE;
/*!40000 ALTER TABLE `emplacement_de_stockage` DISABLE KEYS */;
INSERT INTO `emplacement_de_stockage` VALUES (1,1,1,1,1,'1 rue de l\'emplacement','2021-12-10 00:00:00',NULL,NULL,0),(2,2,2,0,2,NULL,'2021-12-10 00:00:00',NULL,NULL,0),(3,3,3,5,1,'3 rue de l\'emplacement','2021-12-10 00:00:00',NULL,NULL,0),(4,1,2,6,2,'4 rue de l\'emplacement','0000-00-00 00:00:00',NULL,NULL,0),(5,2,2,7,2,'5 rue de l\'emplacement','0000-00-00 00:00:00',NULL,NULL,0),(6,1,1,1,2,'6 rue de l\'emplacement','0000-00-00 00:00:00',NULL,0,0),(7,1,1,5,3,'7 rue de l\'emplacement','0000-00-00 00:00:00',NULL,1,0),(8,3,3,6,3,'8 rue de l\'emplacement','0000-00-00 00:00:00',NULL,NULL,0),(9,2,2,7,3,'9 rue de l\'emplacement','0000-00-00 00:00:00',NULL,NULL,0);
/*!40000 ALTER TABLE `emplacement_de_stockage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `evaluation`
--

LOCK TABLES `evaluation` WRITE;
/*!40000 ALTER TABLE `evaluation` DISABLE KEYS */;
INSERT INTO `evaluation` VALUES (1,1,1,4,1,'Ok','2020-10-10 20:20:20'),(2,2,2,5,2,'Top','2019-11-20 11:11:11'),(3,3,3,6,3,'Super','2019-10-13 13:13:13'),(4,1,1,4,4,'Moyen','2019-10-13 13:13:13');
/*!40000 ALTER TABLE `evaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `jours`
--

LOCK TABLES `jours` WRITE;
/*!40000 ALTER TABLE `jours` DISABLE KEYS */;
INSERT INTO `jours` VALUES (1,'Lundi'),(2,'Mardi'),(3,'Mercredi'),(4,'Jeudi'),(5,'Vendredi'),(6,'Samedi'),(7,'Dimanche');
/*!40000 ALTER TABLE `jours` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mesurer`
--

LOCK TABLES `mesurer` WRITE;
/*!40000 ALTER TABLE `mesurer` DISABLE KEYS */;
INSERT INTO `mesurer` VALUES (2,1),(1,2),(3,2),(14,4),(4,5),(5,5),(10,5),(11,5),(12,6),(7,7),(8,8),(13,9),(9,11),(6,12);
/*!40000 ALTER TABLE `mesurer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mode_paiement`
--

LOCK TABLES `mode_paiement` WRITE;
/*!40000 ALTER TABLE `mode_paiement` DISABLE KEYS */;
INSERT INTO `mode_paiement` VALUES (1,'Paypal'),(2,'CB');
/*!40000 ALTER TABLE `mode_paiement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `motif_annulation_echange`
--

LOCK TABLES `motif_annulation_echange` WRITE;
/*!40000 ALTER TABLE `motif_annulation_echange` DISABLE KEYS */;
INSERT INTO `motif_annulation_echange` VALUES (1,'Plus besoin du produit'),(2,'Pas le temps de récupérer le produit'),(3,'Horaires de récupération incompatibles');
/*!40000 ALTER TABLE `motif_annulation_echange` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `motif_desinscription`
--

LOCK TABLES `motif_desinscription` WRITE;
/*!40000 ALTER TABLE `motif_desinscription` DISABLE KEYS */;
INSERT INTO `motif_desinscription` VALUES (1,'Je n’utilise pas l’application'),(2,'Je ne trouve pas de don qui m’intéresse'),(3,'Mes dons ne sont pas récupérés à temps'),(4,'Je ne suis plus disponible à l’avenir'),(5,'Difficulté d’utilisation de l’application'),(6,'Trop cher, devrait être gratuit'),(7,'Les produits sont de mauvaise qualité'),(8,'Le système est trop compliqué.'),(9,'Le système ne me convient pas'),(10,'Les bénéficiaires ne récupèrent pas leur colis'),(11,'Les donateurs laissent leurs produits sans les récupérer s’ils ne sont pas donnés'),(12,'Plus le temps');
/*!40000 ALTER TABLE `motif_desinscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `motif_non_recuperation`
--

LOCK TABLES `motif_non_recuperation` WRITE;
/*!40000 ALTER TABLE `motif_non_recuperation` DISABLE KEYS */;
INSERT INTO `motif_non_recuperation` VALUES (1,4,'Empêchement'),(2,5,'indisponible'),(3,6,'Lieu de récupération fermé'),(4,7,'Don en mauvais éta');
/*!40000 ALTER TABLE `motif_non_recuperation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `panier`
--

LOCK TABLES `panier` WRITE;
/*!40000 ALTER TABLE `panier` DISABLE KEYS */;
/*!40000 ALTER TABLE `panier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `partenaire`
--

LOCK TABLES `partenaire` WRITE;
/*!40000 ALTER TABLE `partenaire` DISABLE KEYS */;
INSERT INTO `partenaire` VALUES (1,NULL,1,125789498,'G20','125123615','abcdef@gmail.com','blanc','arthur','toto','2021-12-10 00:00:00','2021-12-10 00:00:00','5 rue des Champs',NULL,NULL),(2,NULL,5,153489637,'Franprix','160782698','abcd@gmail.com','lopez','juan','toto','2021-11-09 00:00:00','2021-11-11 00:00:00','1 boulevard Haussman',NULL,NULL),(3,NULL,6,548779916,'SuperAlimentation','157889658','ldeeg@gmail.com','lambic','susie','toto','2021-10-25 00:00:00','2021-10-25 00:00:00','Quai Francois Mauriac',NULL,NULL);
/*!40000 ALTER TABLE `partenaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `periode_indisponibilite`
--

LOCK TABLES `periode_indisponibilite` WRITE;
/*!40000 ALTER TABLE `periode_indisponibilite` DISABLE KEYS */;
INSERT INTO `periode_indisponibilite` VALUES (1,1,2,'2008-01-01 00:00:00','2008-02-01 00:00:00'),(2,2,3,'2008-03-01 00:00:00','2008-04-01 00:00:00'),(3,3,1,'2008-05-01 00:00:00','2008-06-01 00:00:00'),(4,4,1,'2008-07-01 00:00:00','2008-08-01 00:00:00');
/*!40000 ALTER TABLE `periode_indisponibilite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `plage_horaire`
--

LOCK TABLES `plage_horaire` WRITE;
/*!40000 ALTER TABLE `plage_horaire` DISABLE KEYS */;
INSERT INTO `plage_horaire` VALUES (1,1,5,1,1,6,'2021-02-10 07:00:00','2021-02-10 12:00:00','2021-02-10 14:00:00','2021-02-10 18:00:00'),(2,2,6,2,2,4,'2021-02-10 08:15:00','2021-02-10 12:00:00','2021-02-10 14:00:00','2021-02-10 16:15:00'),(3,3,2,3,3,2,'2021-02-10 09:30:00','2021-02-10 12:00:00','2021-02-10 14:00:00','2021-02-10 17:30:00'),(4,4,8,4,2,1,'2021-02-10 10:45:00','2021-02-10 18:00:00',NULL,NULL),(5,5,1,6,1,3,'2021-02-10 11:00:00','2021-02-10 20:00:00',NULL,NULL);
/*!40000 ALTER TABLE `plage_horaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `produit`
--

LOCK TABLES `produit` WRITE;
/*!40000 ALTER TABLE `produit` DISABLE KEYS */;
INSERT INTO `produit` VALUES (1,1,3,'Steak haché'),(2,1,3,'Saucisse'),(3,1,3,'Merguez'),(4,3,3,'Filet de Merlan'),(5,3,3,'Saumon fumé'),(6,5,2,'Carotte'),(7,5,2,'Tomate'),(8,7,2,'Cerise'),(9,7,2,'Banane'),(10,8,2,'Baguette'),(11,8,2,'Pain de campagne'),(12,10,3,'Tiramisu'),(13,10,3,'Fondant chocolat'),(14,11,3,'Lait');
/*!40000 ALTER TABLE `produit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `qualite_echange`
--

LOCK TABLES `qualite_echange` WRITE;
/*!40000 ALTER TABLE `qualite_echange` DISABLE KEYS */;
INSERT INTO `qualite_echange` VALUES (1,'Très bonne'),(2,'Moyenne'),(3,'Mauvaise');
/*!40000 ALTER TABLE `qualite_echange` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `qualite_produit`
--

LOCK TABLES `qualite_produit` WRITE;
/*!40000 ALTER TABLE `qualite_produit` DISABLE KEYS */;
INSERT INTO `qualite_produit` VALUES (1,'Très bonne'),(2,'Moyenne'),(3,'Mauvaise');
/*!40000 ALTER TABLE `qualite_produit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `rayon`
--

LOCK TABLES `rayon` WRITE;
/*!40000 ALTER TABLE `rayon` DISABLE KEYS */;
INSERT INTO `rayon` VALUES (1,'Viandes Poissons'),(2,'Fruits Legumes'),(3,'Pains Pâtisseries'),(4,'Frais'),(5,'Surgelés'),(6,'Epicerie salée'),(7,'Epicerie sucrée'),(8,'Boissons'),(9,'');
/*!40000 ALTER TABLE `rayon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `reclamation`
--

LOCK TABLES `reclamation` WRITE;
/*!40000 ALTER TABLE `reclamation` DISABLE KEYS */;
INSERT INTO `reclamation` VALUES (1,1,1,5,'2020-11-02 15:15:15','bla','2020-11-03 15:15:15',1,'2020-12-02 15:15:15'),(2,2,2,4,'2021-12-12 14:14:14','meuh','2021-12-13 14:14:14',2,'2022-01-12 14:14:14'),(3,3,3,3,'2019-01-01 09:09:09','zut','2019-01-01 10:09:09',0,'2019-01-01 16:09:09'),(4,4,4,2,'2021-05-05 16:16:16','flûte','2021-05-06 16:16:16',4,'2021-05-07 16:16:16'),(5,5,3,1,'2021-06-06 17:17:17','mouais','2021-06-07 17:17:17',5,'2021-06-06 17:20:17');
/*!40000 ALTER TABLE `reclamation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `taille`
--

LOCK TABLES `taille` WRITE;
/*!40000 ALTER TABLE `taille` DISABLE KEYS */;
INSERT INTO `taille` VALUES (1,'Grand',35,60,80),(2,'Moyen',20,40,60),(3,'Petit',16,20,30);
/*!40000 ALTER TABLE `taille` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `temperature_stockage`
--

LOCK TABLES `temperature_stockage` WRITE;
/*!40000 ALTER TABLE `temperature_stockage` DISABLE KEYS */;
INSERT INTO `temperature_stockage` VALUES (1,'ambiante'),(2,'froid positif'),(3,'froid negatif');
/*!40000 ALTER TABLE `temperature_stockage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `transaction`
--

LOCK TABLES `transaction` WRITE;
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
INSERT INTO `transaction` VALUES (1,1,1,1,1,1,'2019-10-13 13:13:13'),(2,2,2,5,4,5,'2020-10-10 20:20:20'),(3,3,1,10,7,10,'2019-10-13 13:13:13'),(4,4,2,1,1,1,'2019-11-20 11:11:11');
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `unite_de_mesure`
--

LOCK TABLES `unite_de_mesure` WRITE;
/*!40000 ALTER TABLE `unite_de_mesure` DISABLE KEYS */;
INSERT INTO `unite_de_mesure` VALUES (1,'gramme'),(2,'kilogramme'),(3,'centilitre'),(4,'litre'),(5,'unite'),(6,'boite'),(7,'cagette'),(8,'panier'),(9,'paquet'),(10,'sachet'),(11,'filet'),(12,'botte');
/*!40000 ALTER TABLE `unite_de_mesure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ville_cp`
--

LOCK TABLES `ville_cp` WRITE;
/*!40000 ALTER TABLE `ville_cp` DISABLE KEYS */;
INSERT INTO `ville_cp` VALUES (1,'Paris',75001,48.86176997,2.34703338),(2,'Bordeaux',33000,0.99990000,-0.99990000),(3,'Marseille',13000,0.99990000,0.99990000),(5,'Sèvres',92310,48.82066549,2.20755425),(6,'Meudon',92190,48.80700422,2.22847973),(7,'Melun',77000,48.54162114,2.65558867);
/*!40000 ALTER TABLE `ville_cp` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-16 11:27:06
