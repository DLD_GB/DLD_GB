/*==============================================================*/
/* Nom de SGBD :  MySQL 5.0                                     */
/* Date de création :  20/12/2021 14:54:16                      */
/*==============================================================*/


drop table if exists Adherent;

drop table if exists Categorie;

drop table if exists Categorie_reclamation;

drop table if exists Don;

drop table if exists Echange;

drop table if exists Emballage;

drop table if exists Emplacement_de_stockage;

drop table if exists Evaluation;

drop table if exists Jours;

drop table if exists Mode_paiement;

drop table if exists Motif_annulation_echange;

drop table if exists Motif_desinscription;

drop table if exists Motif_non_recuperation;

drop table if exists Panier;

drop table if exists Partenaire;

drop table if exists Periode_indisponibilite;

drop table if exists Plage_horaire;

drop table if exists Produit;

drop table if exists Qualite_echange;

drop table if exists Qualite_produit;

drop table if exists Rayon;

drop table if exists Reclamation;

drop table if exists Taille;

drop table if exists Temperature_Stockage;

drop table if exists Transaction;

drop table if exists Unite_de_mesure;

drop table if exists Ville_CP;

drop table if exists mesurer;

/*==============================================================*/
/* Table : Adherent                                             */
/*==============================================================*/
create table Adherent
(
   Id                   int not null auto_increment,
   Ville_CP_Id          int not null,
   Motif_desinscription_Id int,
   Nom                  varchar(254),
   Prenom               varchar(254),
   Nom_utilisateur      varchar(254),
   Date_de_naissance    datetime,
   Email                varchar(254),
   Mot_de_passe         varchar(254),
   Date_saisie_Inscription datetime,
   Date_validation_inscription datetime,
   Nom_et_n_de_voie     varchar(254),
   droit_Administrateur bool,
   Date_desinscription  datetime,
   Date_reinscription   datetime,
   primary key (Id)
);

/*==============================================================*/
/* Table : Categorie                                            */
/*==============================================================*/
create table Categorie
(
   Id                   int not null auto_increment,
   Rayon_Id             int not null,
   Nom                  varchar(254),
   primary key (Id)
);

/*==============================================================*/
/* Table : Categorie_reclamation                                */
/*==============================================================*/
create table Categorie_reclamation
(
   Id                   int not null auto_increment,
   nom                  varchar(254),
   primary key (Id)
);

/*==============================================================*/
/* Table : Don                                                  */
/*==============================================================*/
create table Don
(
   Id                   int not null auto_increment,
   Produit_Id           int not null,
   Ville_CP_Id          int,
   Adherent_Id          int not null,
   Emballage_Id         int not null,
   Emplacement_de_stockage_Id int,
   Unite_de_mesure_Id   int not null,
   Quantite_produit     float,
   Date_peremption      datetime,
   Date_de_depot        datetime,
   Date_annulation_don  datetime,
   Nom_et_n_de_voie     varchar(254),
   Date_entree_proposition datetime,
   Date_de_mise_au_rebut_ datetime,
   Reservable           bool not null,
   primary key (Id)
);

/*==============================================================*/
/* Table : Echange                                              */
/*==============================================================*/
create table Echange
(
   Id                   int not null auto_increment,
   Don_Id               int not null,
   Motif_annulation_echange_Id int not null,
   Adherent_Id          int not null,
   Date_reservation     datetime,
   Date_annulation_echange datetime,
   Date_validation      datetime,
   Date_de_retrait_du_produit datetime,
   Date_recuperation_produit datetime,
   primary key (Id)
);

/*==============================================================*/
/* Table : Emballage                                            */
/*==============================================================*/
create table Emballage
(
   Id                   int not null auto_increment,
   nom                  varchar(254),
   primary key (Id)
);

/*==============================================================*/
/* Table : Emplacement_de_stockage                              */
/*==============================================================*/
create table Emplacement_de_stockage
(
   Id                   int not null auto_increment,
   Taille_Id            int not null,
   Temperature_Stockage_Id int not null,
   Ville_CP_Id          int,
   Partenaire_Id        int not null,
   Nom_et_n_de_voie     varchar(254),
   Date_ajout_stockage  datetime,
   Date_retrait_Stockage datetime,
   Box_automatique      bool not null,
   Disponible           bool not null,
   primary key (Id)
);

/*==============================================================*/
/* Table : Evaluation                                           */
/*==============================================================*/
create table Evaluation
(
   Id                   int not null auto_increment,
   Qualite_echange_Id   int not null,
   Qualite_produit_Id   int not null,
   Echange_Id           int not null,
   Note_evaluation      int,
   Commentaire_evaluation varchar(254),
   Date_evaluation      datetime,
   primary key (Id)
);

/*==============================================================*/
/* Table : Jours                                                */
/*==============================================================*/
create table Jours
(
   Id                   int not null auto_increment,
   jours_semaine        varchar(254),
   primary key (Id)
);

/*==============================================================*/
/* Table : Mode_paiement                                        */
/*==============================================================*/
create table Mode_paiement
(
   Id                   int not null auto_increment,
   nom_                 varchar(254),
   primary key (Id)
);

/*==============================================================*/
/* Table : Motif_annulation_echange                             */
/*==============================================================*/
create table Motif_annulation_echange
(
   Id                   int not null auto_increment,
   Raison               varchar(254),
   primary key (Id)
);

/*==============================================================*/
/* Table : Motif_desinscription                                 */
/*==============================================================*/
create table Motif_desinscription
(
   Id                   int not null auto_increment,
   Raison_              varchar(254),
   primary key (Id)
);

/*==============================================================*/
/* Table : Motif_non_recuperation                               */
/*==============================================================*/
create table Motif_non_recuperation
(
   Id                   int not null auto_increment,
   Echange_Id           int not null,
   raison               varchar(254),
   primary key (Id)
);

/*==============================================================*/
/* Table : Panier                                               */
/*==============================================================*/
create table Panier
(
   Don_Id               int not null,
   Adherent_Id          int not null,
   Id                   int not null auto_increment,
   primary key (Id)
);

/*==============================================================*/
/* Table : Partenaire                                           */
/*==============================================================*/
create table Partenaire
(
   Id                   int not null auto_increment,
   Motif_desinscription_Id int,
   Ville_CP_Id          int not null,
   SIRET                int,
   Nom_Societe          varchar(254),
   Telephone            int,
   Email                varchar(254),
   Nom_referent         varchar(254),
   Prenom_referent      varchar(254),
   Mot_de_passe         varchar(254),
   Date_saisie_inscription datetime,
   Date_validation_inscription datetime,
   Nom_et_n_de_voie     varchar(254),
   Date_desinscription  datetime,
   Date_reinscription   datetime,
   primary key (Id)
);

/*==============================================================*/
/* Table : Periode_indisponibilite                              */
/*==============================================================*/
create table Periode_indisponibilite
(
   Id                   int not null auto_increment,
   Partenaire_Id        int not null,
   Adherent_Id          int not null,
   Date_debut           datetime,
   Fin_Jour_            datetime,
   primary key (Id)
);

/*==============================================================*/
/* Table : Plage_horaire                                        */
/*==============================================================*/
create table Plage_horaire
(
   Id                   int not null auto_increment,
   Adherent_Id          int,
   Emplacement_de_stockage_Id int not null,
   Jours_Id             int not null,
   Partenaire_Id        int,
   Don_Id               int,
   Debut_plage_1        datetime,
   Fin_plage_1          datetime,
   Debut_plage_2        datetime,
   Fin_plage_2          datetime,
   primary key (Id)
);

/*==============================================================*/
/* Table : Produit                                              */
/*==============================================================*/
create table Produit
(
   Id                   int not null auto_increment,
   Categorie_Id         int not null,
   Temperature_Stockage_Id int,
   Nom                  varchar(254),
   primary key (Id)
);

/*==============================================================*/
/* Table : Qualite_echange                                      */
/*==============================================================*/
create table Qualite_echange
(
   Id                   int not null auto_increment,
   Libelle_quali_ech    varchar(254),
   primary key (Id)
);

/*==============================================================*/
/* Table : Qualite_produit                                      */
/*==============================================================*/
create table Qualite_produit
(
   Id                   int not null auto_increment,
   libelle_quali_prod   varchar(254),
   primary key (Id)
);

/*==============================================================*/
/* Table : Rayon                                                */
/*==============================================================*/
create table Rayon
(
   Id                   int not null auto_increment,
   Nom                  varchar(254),
   primary key (Id)
);

/*==============================================================*/
/* Table : Reclamation                                          */
/*==============================================================*/
create table Reclamation
(
   Id                   int not null auto_increment,
   Adherent_Id          int not null,
   Categorie_reclamation_Id int not null,
   Echange_Id           int not null,
   Date_reclamation     datetime,
   Commentaire_         varchar(254),
   Date_traitement      datetime,
   Note_evaluation_traitement int,
   Date_cloture_reclamation datetime,
   primary key (Id)
);

/*==============================================================*/
/* Table : Taille                                               */
/*==============================================================*/
create table Taille
(
   Id                   int not null auto_increment,
   nom_taille           varchar(254),
   Hauteur_moyenne      int,
   Largeur_moyenne      int,
   Longeur_moyenne      int,
   primary key (Id)
);

/*==============================================================*/
/* Table : Temperature_Stockage                                 */
/*==============================================================*/
create table Temperature_Stockage
(
   Id                   int not null auto_increment,
   Nom                  varchar(254),
   primary key (Id)
);

/*==============================================================*/
/* Table : Transaction                                          */
/*==============================================================*/
create table Transaction
(
   Id                   int not null auto_increment,
   Adherent_Id          int not null,
   Mode_paiement_Id     int,
   Augmentation_credit  int,
   Montant_paiement     int,
   credit_de_recuperation_ int,
   Date_transaction     datetime,
   primary key (Id)
);

/*==============================================================*/
/* Table : Unite_de_mesure                                      */
/*==============================================================*/
create table Unite_de_mesure
(
   Id                   int not null auto_increment,
   nom                  varchar(254),
   primary key (Id)
);

/*==============================================================*/
/* Table : Ville_CP                                             */
/*==============================================================*/
create table Ville_CP
(
   Id                   int not null auto_increment,
   Ville                varchar(254),
   Code_postal          int,
   Latitude             numeric(8,0),
   Longitude            numeric(8,0),
   primary key (Id)
);

/*==============================================================*/
/* Table : mesurer                                              */
/*==============================================================*/
create table mesurer
(
   Produit_Id           int not null auto_increment,
   Id                   int not null,
   primary key (Produit_Id, Id)
);

alter table Adherent add constraint FK_Association_1 foreign key (Ville_CP_Id)
      references Ville_CP (Id) on delete restrict on update restrict;

alter table Adherent add constraint FK_Association_34 foreign key (Motif_desinscription_Id)
      references Motif_desinscription (Id) on delete restrict on update restrict;

alter table Categorie add constraint FK_Association_26 foreign key (Rayon_Id)
      references Rayon (Id) on delete restrict on update restrict;

alter table Don add constraint FK_Association_13 foreign key (Emplacement_de_stockage_Id)
      references Emplacement_de_stockage (Id) on delete restrict on update restrict;

alter table Don add constraint FK_Association_14 foreign key (Ville_CP_Id)
      references Ville_CP (Id) on delete restrict on update restrict;

alter table Don add constraint FK_Association_18 foreign key (Produit_Id)
      references Produit (Id) on delete restrict on update restrict;

alter table Don add constraint FK_Association_2 foreign key (Adherent_Id)
      references Adherent (Id) on delete restrict on update restrict;

alter table Don add constraint FK_Association_24 foreign key (Unite_de_mesure_Id)
      references Unite_de_mesure (Id) on delete restrict on update restrict;

alter table Don add constraint FK_Association_25 foreign key (Emballage_Id)
      references Emballage (Id) on delete restrict on update restrict;

alter table Echange add constraint FK_Association_3 foreign key (Adherent_Id)
      references Adherent (Id) on delete restrict on update restrict;

alter table Echange add constraint FK_Association_38 foreign key (Motif_annulation_echange_Id)
      references Motif_annulation_echange (Id) on delete restrict on update restrict;

alter table Echange add constraint FK_Association_4 foreign key (Don_Id)
      references Don (Id) on delete restrict on update restrict;

alter table Emplacement_de_stockage add constraint FK_Association_16 foreign key (Partenaire_Id)
      references Partenaire (Id) on delete restrict on update restrict;

alter table Emplacement_de_stockage add constraint FK_Association_20 foreign key (Temperature_Stockage_Id)
      references Temperature_Stockage (Id) on delete restrict on update restrict;

alter table Emplacement_de_stockage add constraint FK_Association_21 foreign key (Taille_Id)
      references Taille (Id) on delete restrict on update restrict;

alter table Emplacement_de_stockage add constraint FK_Association_27 foreign key (Ville_CP_Id)
      references Ville_CP (Id) on delete restrict on update restrict;

alter table Evaluation add constraint FK_Association_17 foreign key (Echange_Id)
      references Echange (Id) on delete restrict on update restrict;

alter table Evaluation add constraint FK_Association_29 foreign key (Qualite_echange_Id)
      references Qualite_echange (Id) on delete restrict on update restrict;

alter table Evaluation add constraint FK_Association_30 foreign key (Qualite_produit_Id)
      references Qualite_produit (Id) on delete restrict on update restrict;

alter table Motif_non_recuperation add constraint FK_Association_40 foreign key (Echange_Id)
      references Echange (Id) on delete restrict on update restrict;

alter table Panier add constraint FK_Association_45 foreign key (Adherent_Id)
      references Adherent (Id) on delete restrict on update restrict;

alter table Panier add constraint FK_Association_46 foreign key (Don_Id)
      references Don (Id) on delete restrict on update restrict;

alter table Partenaire add constraint FK_Association_19 foreign key (Ville_CP_Id)
      references Ville_CP (Id) on delete restrict on update restrict;

alter table Partenaire add constraint FK_Association_35 foreign key (Motif_desinscription_Id)
      references Motif_desinscription (Id) on delete restrict on update restrict;

alter table Periode_indisponibilite add constraint FK_Association_36 foreign key (Adherent_Id)
      references Adherent (Id) on delete restrict on update restrict;

alter table Periode_indisponibilite add constraint FK_Association_37 foreign key (Partenaire_Id)
      references Partenaire (Id) on delete restrict on update restrict;

alter table Plage_horaire add constraint FK_Association_12 foreign key (Emplacement_de_stockage_Id)
      references Emplacement_de_stockage (Id) on delete restrict on update restrict;

alter table Plage_horaire add constraint FK_Association_31 foreign key (Jours_Id)
      references Jours (Id) on delete restrict on update restrict;

alter table Plage_horaire add constraint FK_Association_39 foreign key (Don_Id)
      references Don (Id) on delete restrict on update restrict;

alter table Plage_horaire add constraint FK_Association_43 foreign key (Partenaire_Id)
      references Partenaire (Id) on delete restrict on update restrict;

alter table Plage_horaire add constraint FK_Association_44 foreign key (Adherent_Id)
      references Adherent (Id) on delete restrict on update restrict;

alter table Produit add constraint FK_Association_23 foreign key (Temperature_Stockage_Id)
      references Temperature_Stockage (Id) on delete restrict on update restrict;

alter table Produit add constraint FK_Association_6 foreign key (Categorie_Id)
      references Categorie (Id) on delete restrict on update restrict;

alter table Reclamation add constraint FK_Association_15 foreign key (Echange_Id)
      references Echange (Id) on delete restrict on update restrict;

alter table Reclamation add constraint FK_Association_22 foreign key (Categorie_reclamation_Id)
      references Categorie_reclamation (Id) on delete restrict on update restrict;

alter table Reclamation add constraint FK_Association_28 foreign key (Adherent_Id)
      references Adherent (Id) on delete restrict on update restrict;

alter table Transaction add constraint FK_Association_41 foreign key (Adherent_Id)
      references Adherent (Id) on delete restrict on update restrict;

alter table Transaction add constraint FK_Association_42 foreign key (Mode_paiement_Id)
      references Mode_paiement (Id) on delete restrict on update restrict;

alter table mesurer add constraint FK_mesurerProduit foreign key (Produit_Id)
      references Produit (Id) on delete restrict on update restrict;

alter table mesurer add constraint FK_mesurerUnitéMesure foreign key (Id)
      references Unite_de_mesure (Id) on delete restrict on update restrict;

